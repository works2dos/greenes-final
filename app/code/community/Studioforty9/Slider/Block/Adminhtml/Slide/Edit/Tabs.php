<?php

class Studioforty9_Slider_Block_Adminhtml_Slide_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('slider_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->_getHelper()->__('Slides'));
    }

    /**
     * _beforeToHtml()
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'   => $this->_getHelper()->__('Slide Content'),
            'title'   => $this->_getHelper()->__('Slide Content'),
            'content' => $this->getLayout()->createBlock(
                'studioforty9_slider_adminhtml/slide_edit_tab_form'
            )->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

    /**
     * _getHelper()
     *
     * @return Studioforty9_Slider_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('studioforty9_slider');
    }
}
