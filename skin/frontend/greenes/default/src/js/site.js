var $j = jQuery.noConflict();
$j(document).ready(function ($) {
    $('#select-currency').CurrencySwitcher({});

    if ($('body.cms-index-index').length) {
        $('#banner-carousel').slider();
    }

    if ($('body.contacts-index-index').length) {
        $('.opening-hours-activator').on('click', function(evt) {
            evt.preventDefault();
            console.log($(this).parent());
            $(this).parent().parent().find('.store-hours').slideToggle();
        });
    }
});
