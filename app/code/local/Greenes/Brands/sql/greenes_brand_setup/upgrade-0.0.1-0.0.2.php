<?php
/** @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
/** @var Magento_Db_Adapter_Pdo_Mysql $conn */


$brands = Mage::getModel('studioforty9_brands/brand')
    ->getCollection();

/** @var Studioforty9_Brands_Model_Brand $brand */
foreach ($brands as $brand) {

    $productIds = array();
    $name = $brand->getData('name');

    /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
    $collection = Mage::getModel('catalog/product')->getCollection();
    $collection->addAttributeToSelect(array('id'));
    $collection->addAttributeToFilter(
        array(
            array('attribute'=>'name', 'like'=> "%$name%")
        )
    );

    if (! $collection->count()) {
        Mage::log("Could not find any products with the name $name", null, 'brand_failed.log', true);
        continue;
    }

    foreach ($collection as $product) {
        $productIds[] = $product->getId();
    }

    /** @var Mage_Catalog_Model_Product_Action $action */
    $action = Mage::getSingleton('catalog/product_action');

    try {
        $action->updateAttributes(
            $productIds,
            array('brand_id' => $brand->getId()),
            0
        );
    } catch (Exception $e) {
        Mage::log("Could not update products for the brand $name", null, 'brand_failed.log', true);
        Mage::log($e->getMessage(), null, 'brand_failed.log', true);
        Mage::log($productIds, null, 'brand_failed.log', true);
    }
}
$this->endSetup();