<?php

/**
 * Class Greenes_Epos_Model_Source_Abstract
 *
 * @method string   getColumnValue()
 * @method array    getRowData()
 * @method string   getRowColumn()
 *
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
abstract class Greenes_Epos_Model_Source_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * @param string $value
     *
     * @return false|mixed
     */
    abstract public function getValue();


    /**
     * Sets the row data and column
     *
     * It also sets the default value if exists with the data
     *
     * @param array  $data
     * @param string $column
     *
     * @return $this
     */
    public function setRowData(array $data, $column = '')
    {
        $this->setData('row_data', $data);
        if (!$column) {
            return $this;
        }
        $this->setRowColumn($column);

        if (array_key_exists($column, $data)) {
            $this->setColumnValue($data[$column]);
        }
        return $this;

    }

    /**
     * @return Greenes_Epos_Helper_Attributes
     */
    public function getAttributeHelper()
    {
        return Mage::helper('greenes_epos/attributes');
    }

    /**
     * @return Greenes_Epos_Helper_Attributeset
     */
    public function getAttributeSetHelper()
    {
        return Mage::helper('greenes_epos/attributeset');
    }

    /**
     * @return Greenes_Epos_Helper_Option
     */
    public function getOptionHelper()
    {
        return Mage::helper('greenes_epos/option');
    }

    /**
     * Get another row value by attribute code
     *
     * @param string $code
     *
     * @return bool|false|mixed
     */
    public function getRowValueByAttributeCode($code)
    {
        $sourceModel = $this->getSourceModelByCode($code);
        if (!$sourceModel) {
            return false;
        }

        $column = $this->getAttributeHelper()->getColumnByCode($code);
        $sourceModel->setRowData($this->getRowData(), $column);
        return $sourceModel->getValue();
    }

    /**
     * @param string $code
     *
     * @return false|Greenes_Epos_Model_Source_Abstract
     */
    public function getSourceModelByCode($code)
    {
        $model = $this->getAttributeHelper()->getSourceModelByCode($code);
        if (!$model) {
            return false;
        }
        return Mage::getModel($model);
    }

    /**
     * @param $code
     *
     * @return bool
     */
    public function getRawAttributeValue($code)
    {
        $column = $this->getAttributeHelper()->getColumnByCode($code);
        if (!$column) {
            return false;
        }

        $data = $this->getRowData();
        if (!array_key_exists($column, $data)) {
            return false;
        }

        return $data[$column];
    }
}