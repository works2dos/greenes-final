<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Stock
 */
class Greenes_Epos_Model_Source_StockSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Stock');
    }

    function it_should_be_valid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("stock");
        $data = array($column => 1);
        $this->setRowData($data, $column);
        $this->getValue()->shouldBeArray();

        $data = array($column => 0);
        $this->setRowData($data, $column);
        $this->getValue()->shouldBeArray();

        $data = array($column => 5);
        $this->setRowData($data, $column);
        $this->getValue()->shouldBeArray();




    }
}
