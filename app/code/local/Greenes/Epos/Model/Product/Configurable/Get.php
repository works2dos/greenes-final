<?php

/**
 * Class Greenes_Epos_Model_Product_Configurable_Get
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Configurable_Get extends Greenes_Epos_Model_Base
{

    /**
     * Gets configurable products and grouped by sku
     *
     * @return array|bool
     */
    public function getProducts()
    {
        $collection = Mage::getSingleton('greenes_epos/configurable')
            ->getCollection()
            ->addFieldToSelect("*")
            ->setOrder("id", "ASC");

        if (!$collection->count()) {
            return false;
        }

        $data = array();
        /** @var Greenes_Epos_Model_Configurable$item */
        foreach ($collection as $item) {
            $sku = $item->getData("configurable_sku");
            $productId = $item->getData("id");
            $eposId = $item->getData("epos_id");
            $data[$sku][] = array(
                'product_id' => $productId,
                'epos_id' => $eposId
            );
        }

        return $data;
    }
}