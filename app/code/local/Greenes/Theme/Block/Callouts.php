<?php

/**
 * Class Greenes_Theme_Block_Callouts
 *
 * @extends     Mage_Core_Block_Template
 * @category    Greenes
 * @package     Greenes_Theme
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Theme_Block_Callouts extends Mage_Core_Block_Template
{
    protected function _beforeToHtml()
    {
        $callouts = $this->getCallouts();
        if (! $callouts) {
            $this->setTemplate("");
        }
        parent::_beforeToHtml();
    }

    /**
     * @return array|false
     */
    public function getCallouts()
    {
        if ($this->getData("callouts")) {
            return $this->getData("callouts");
        }

        $callouts = array();
        for ($i = 1; $i <= 3; $i++) {
            $image = Mage::getStoreConfig("greenes_homepage/callouts/callout_" . $i . "_image");
            if (! $image) {
                continue;
            }
            $callout = new Varien_Object();
            $callout->setData("image", $image);
            $callout->setData("url", Mage::getStoreConfig("greenes_homepage/callouts/callout_" . $i . "_url"));
            $callout->setData("description", Mage::getStoreConfig("greenes_homepage/callouts/callout_" . $i . "_description"));
            $callout->setData("button", Mage::getStoreConfig("greenes_homepage/callouts/callout_" . $i . "_button"));
            $callouts[] = $callout;
        }

        if (empty($callouts)) {
            return false;
        }

        $this->setData("callouts", $callouts);
        return $callouts;
    }


}