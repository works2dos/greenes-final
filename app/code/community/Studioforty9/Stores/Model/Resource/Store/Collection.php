<?php
/**
 * StudioForty9 Stores Resource Collection Model
 *
 * @category   Studioforty9
 * @package    Studioforty9_Stores
 * @author     Copyright (c) 2016 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Model_Resource_Store_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * _construct()
     *
     * @access protected
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('studioforty9_stores/store');
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();

        foreach ($this as $store) {
            $store->afterLoad();
        }

        return $this;
    }
}
