<?php

class Greenes_Promotions_Block_Products_Group extends Mage_Core_Block_Text_List
{
    /** @var string */
    protected $_blockType = 'greenes_promotions/products';

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        if (!$this->hasTemplate()) {
            $this->setTemplate('greenes/promotions/product.phtml');
        }

        $_collection = $this->_getCollection();

        if ($_collection->count() > 0) {
            foreach ($_collection as $_model) {
                $_blockName = $this->getFormattedBlockName($_model->getId());
                $_block = $this->getLayout()->createBlock($this->_blockType, $_blockName, array(
                    'template' => $this->getTemplate()
                ));
                $_block->setCategory($_model);
                $this->append($_block);
            }
        }

        return $this;
    }

    /**
     * Get a formatted block name based on a given iteration of child blocks.
     *
     * @param int $key
     *
     * @return string
     */
    protected function getFormattedBlockName($key)
    {
        $prefix = str_replace(array('/', '_', '-'), '.', $this->_blockType);
        return $prefix . '.' . $key;
    }

    /**
     * Get the collection. If 're-writing' this class, you can safely
     * override this method by returning a Category Collection.
     *
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    protected function _getCollection()
    {
        $_collection = Mage::helper('greenes_promotions')->getProductPromotionsCollection();
        $_collection->setPageSize($this->getPageSize());

        return $_collection;
    }
}
