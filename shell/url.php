<?php

require_once 'abstract.php';

class Mage_Shell_Url extends Mage_Shell_Abstract
{
    /**
     * Apply PHP settings to shell script
     *
     * @return void
     */
    protected function _applyPhpVariables()
    {
        parent::_applyPhpVariables();

        set_time_limit(0);
        error_reporting(E_ALL);
        ini_set( 'memory_limit', '2G' );
        ini_set( 'display_errors', 1 );
    }

    public function run()
    {
        Mage::getModel('studioforty9_url/observer')->removeDuplicateUrls();
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php url.php
USAGE;
    }
}

$shell = new Mage_Shell_Url();
$shell->run();
