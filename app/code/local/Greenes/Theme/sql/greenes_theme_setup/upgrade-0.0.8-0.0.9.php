<?php
/**
 * Captializes Colour Values
 */
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $this->getConnection();

$attributeId = Mage::getSingleton('eav/config')
    ->getAttribute(Mage_Catalog_Model_Product::ENTITY, "colour")
    ->getId();

$optionTable = $this->getConnection()->getTableName("eav_attribute_option");
$valueTable = $this->getConnection()->getTableName("eav_attribute_option_value");

$query
    = "SELECT v.value_id, v.value
FROM $optionTable as o
INNER JOIN $valueTable as v ON o.option_id = v.option_id
WHERE o.attribute_id = $attributeId
ORDER BY value ASC";


/** @var Varien_Db_Statement_Pdo_Mysql $options */
$options = $this->getConnection()->query($query);
$data = array();
foreach ($options as $option) {
    $value = strtolower($option['value']);
    $value = str_replace("/", "/ ", $value);
    $value = ucwords($value);
    $value = str_replace("/ ", "/", $value);
    $value = Mage::helper('core')->escapeHtml($value);
    $id = (int)$option['value_id'];

    $query = "UPDATE $valueTable SET value = '$value' WHERE value_id = $id";
    $connection->query($query);
}

$this->endSetup();