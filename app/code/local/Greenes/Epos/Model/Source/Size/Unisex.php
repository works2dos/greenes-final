<?php

/**
 * Mens Size Source Model
 *
 * Class Greenes_Epos_Model_Source_Size_Unisex
 *
 * @extends     Greenes_Epos_Model_Source_Size
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Size_Unisex
    extends Greenes_Epos_Model_Source_Size
{
    use Greenes_Epos_Model_Source_Csv_Trait;

    /**
     * Only Set the size if its in the ladies attribute set id
     *
     * @return bool|false|string
     */
    public function getValue()
    {
        $value = $this->getColumnValue();
        if (!$value) {
            return false;
        }

        $attributeSetId = $this->getRowValueByAttributeCode("attribute_set_id");
        if ($attributeSetId !== $this->getAttributeSetId()) {
            return false;
        }

        $value = parent::getValue();
        if (! $value) {
            return false;
        }

        return $this->getOptionHelper()->getOptionId($value, $this->getAttributeSize());
    }

    /**
     * Config path of the CSV file
     *
     * @return bool
     */
    public function getFileConfigPath()
    {
        return $this->getData("file_config_path");
    }

    /**
     * Mens Attribute Set Id
     *
     * @return int
     */
    public function getAttributeSetId()
    {
        return $this->getAttributeSetHelper()->getSetId("UNI");
    }


    /**
     * @return string
     */
    public function getAttributeSize()
    {
        return $this->getAttributeSetHelper()->getSize("UNI");
    }

}