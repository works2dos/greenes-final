<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

$entityCode = Mage_Catalog_Model_Product::ENTITY;

$this->addAttribute(
    $entityCode,
    "imported_sku",
    array(
        'position'                 => 999,
        'user_defined'             => 1,
        "apply_to"                 => "simple",
        'type'                     => 'text',
        'input'                    => 'text',
        'label'                    => "Imported Prestashop SKU",
        'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'                  => 0,
        'required'                 => 0,
        'visible_on_front'         => 0,
        'frontend_class'           => '',
        'is_html_allowed_on_front' => 0,
        'is_configurable'          => 0,
        'searchable'               => 0,
        'filterable'               => 0,
        'comparable'               => 0,
        'unique'                   => 0,
        'used_in_product_listing'  => 0,
    )
);

$this->endSetup();