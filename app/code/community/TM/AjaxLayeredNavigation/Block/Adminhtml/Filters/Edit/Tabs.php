<?php

class TM_AjaxLayeredNavigation_Block_Adminhtml_Filters_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('ajaxlayerednavigation_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ajaxlayerednavigation')->__('Attribute Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('name_section', array(
            'label'     => Mage::helper('ajaxlayerednavigation')->__('Generel'),
            'title'     => Mage::helper('ajaxlayerednavigation')->__('Generel'),
            'content'   => $this->getLayout()->createBlock('ajaxlayerednavigation/adminhtml_filters_edit_tab_filter')->toHtml(),
        ));

        $this->addTab('options_section', array(
            'label'     => Mage::helper('ajaxlayerednavigation')->__('Options'),
            'title'     => Mage::helper('ajaxlayerednavigation')->__('Options'),
            'content'   => $this->getLayout()->createBlock('ajaxlayerednavigation/adminhtml_filters_edit_tab_options')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
