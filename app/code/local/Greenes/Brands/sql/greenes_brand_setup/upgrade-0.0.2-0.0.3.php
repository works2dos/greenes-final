<?php
/** @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
/** @var Magento_Db_Adapter_Pdo_Mysql $conn */

/** @var Studioforty9_Brands_Model_Resource_Brand_Collection $brands */
$brands = Mage::getModel('studioforty9_brands/brand')
    ->getCollection();

$brands->addFieldToFilter(
    'name',
    array('in', array(
        'A&B',
        'Anne Michelle',
        'Apple Computer, Inc',
        'Baerchi',
        'BKB',
        'CAA',
        'Casandra',
        'Don Uorri',
        'Dudes',
        'Gimor',
        'KED',
        'Kraseva',
        'LJR Footwear',
        'Ruby Rocks',
        'SBT',
        'South Sole Ltd',
        'TUK',
        'UTN',
        'Vt range'
    )
    )
);


/** @var Studioforty9_Brands_Model_Brand $brand */
foreach ($brands as $brand) {
    $brand->setVisibility(0);
    $brand->save();
}
$this->endSetup();