<?php

class SF9_Realex_Model_Observer extends Varien_Event_Observer
{
    public function adminhtmlBlockHtmlBefore($observer)
    {
        /** @var Mage_Adminhtml_Block_Sales_Order_Grid $block */
        $block = $observer->getEvent()->getBlock();
        if (false === ($block instanceof Mage_Adminhtml_Block_Sales_Order_Grid)) {
            return;
        }

        $block->getMassactionBlock()->addItem('change_state',
            array(
                'label' => Mage::helper('sales')->__('Change State'),
                'url' => $block->getUrl('realex/adminhtml_realex/setState'),
                'additional' => array(
                    'state' => array(
                        'name' => 'state',
                        'class' => 'required-entry',
                        'label' => Mage::helper('sales')->__('State'),
                        'type' => 'select',
                        'options' => array(
                            //Mage_Sales_Model_Order::STATE_NEW => 'New',
                            Mage_Sales_Model_Order::STATE_PENDING_PAYMENT => 'Pending Payment',
                            Mage_Sales_Model_Order::STATE_PROCESSING => 'Processing',
                            //Mage_Sales_Model_Order::STATE_COMPLETE => 'Complete',
                            //Mage_Sales_Model_Order::STATE_CLOSED => 'Closed',
                            //Mage_Sales_Model_Order::STATE_CANCELED => 'Canceled',
                            //Mage_Sales_Model_Order::STATE_HOLDED => "On Hold"
                        ),
                    ),
                ),
            )
        );
    }
}