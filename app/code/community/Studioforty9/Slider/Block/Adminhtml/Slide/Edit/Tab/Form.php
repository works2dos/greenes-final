<?php

class Studioforty9_Slider_Block_Adminhtml_Slide_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * _prepareForm()
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('slide_form', array(
            'legend' => $this->_getHelper()->__('Slide Content')
        ));

        $fieldset->addType('image', 'Studioforty9_Slider_Block_Adminhtml_Slide_Helper_Image');

        $this->_addFieldsToFieldset($fieldset, array(
            'title'           => $this->_getTitleField(),
            'image'           => $this->_getImageField(),
            'image_tablet'    => $this->_getImageTabletField(),
            'image_mobile'    => $this->_getImageMobileField(),
            'theme'           => $this->_getThemeField(),
            'color'           => $this->_getColorField(),
            'summary'         => $this->_getSummaryField(),
            //'store_view'      => $this->_getStoreViewField(),
            'link'            => $this->_getLinkField(),
            'sequence'        => $this->_getSequenceField(),
            'status'          => $this->_getStatusField()
        ));

        return parent::_prepareForm();
    }

    /**
     * Retrieve the existing banner for pre-populating the form fields.
     * For a new slide entry this will return an empty Slide object.
     *
     * @return Studioforty9_Slider_Model_Slide
     */
    protected function _getSlide()
    {
        if (! $this->hasData('slide')) {
            $slide = Mage::registry('current_slide');
            if (!$slide instanceof Studioforty9_Slider_Model_Slide) {
                $slide = Mage::getModel('studioforty9_slider/slide');
            }
            $this->setData('slide', $slide);
        }

        return $this->getData('slide');
    }

    /**
     * This method makes life a little easier for us by pre-populating
     * fields with $_POST data where applicable and wraps our post data in
     * 'slideData' so we can easily separate all relevant information in
     * the controller.
     *
     * @param Varien_Data_Form_Element_Fieldset $fieldset
     * @param array                             $fields
     * @return $this
     */
    protected function _addFieldsToFieldset(Varien_Data_Form_Element_Fieldset $fieldset, $fields)
    {
        $requestData = new Varien_Object($this->getRequest()->getPost());

        foreach ($fields as $name => $_data) {
            if ($requestValue = $requestData->getData($name)) {
                $_data['value'] = $requestValue;
            }
            // wrap all fields with slide group
            $_data['name'] = $name;
            // generally label and title always the same
            $_data['title'] = $_data['label'];
            // if no new value exists, use existing banner data
            $existingValue = $this->_getSlide()->getData($name);
            if (!array_key_exists('value', $_data) || array_key_exists('value', $_data) && $_data['value'] !== $existingValue) {
                $_data['value'] = $existingValue;
            }
            // finally call vanilla functionality to add field
            $fieldset->addField($name, $_data['input'], $_data);
        }

        return $this;
    }

    /**
     * _getHelper()
     *
     * @return Studioforty9_Slider_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('studioforty9_slider');
    }

    /**
     * _getSession()
     *
     * @return Mage_Adminhtml_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * _getTitleField()
     *
     * @return array
     */
    protected function _getTitleField()
    {
        return array(
            'input'    => 'text',
            'label'    => $this->_getHelper()->__('Title'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'title'
        );
    }

    /**
     * _getImageField()
     *
     * @return array
     */
    protected function _getImageField()
    {
        return array(
            'input'  => 'image',
            'name'   => 'image',
            'label'  => $this->_getHelper()->__('Slide Image'),
            'title'  => $this->_getHelper()->__('Slide Image'),
            'hint'   => '1200 x 480 pixels',
            'tooltip' => '770 x 480 pixels',
        );
    }

    /**
     * _getImageTabletField()
     *
     * @return array
     */
    protected function _getImageTabletField()
    {
        return array(
            'input'  => 'image',
            'name'   => 'image_tablet',
            'label'  => $this->_getHelper()->__('Tablet Slide Image'),
            'title'  => $this->_getHelper()->__('Tablet Slide Image'),
            'hint'   => '980 x 480 pixels',
            'tooltip' => '770 x 480 pixels',
        );
    }

    /**
     * _getImageMobileField()
     *
     * @return array
     */
    protected function _getImageMobileField()
    {
        return array(
            'input'   => 'image',
            'name'    => 'image_mobile',
            'label'   => $this->_getHelper()->__('Mobile Slide Image'),
            'title'   => $this->_getHelper()->__('Mobile Slide Image'),
            'hint'    => '770 x 480 pixels',
            'tooltip' => '770 x 480 pixels',
        );
    }

    /**
     * _getColorField()
     *
     * @return array
     */
    protected function _getColorField()
    {
        return array(
            'input'    => 'text',
            'label'    => $this->_getHelper()->__('Background Color'),
            'class'    => '',
            'required' => false,
            'name'     => 'color',
            'value'    => '#000000',
            'hint'    => 'A hexadecimal colour code e.g. #333333',
        );
    }

    /**
     * _getThemeField()
     *
     * @return array
     */
    protected function _getThemeField()
    {
        return array(
            'input'  => 'select',
            'label'  => $this->_getHelper()->__('Theme'),
            'name'   => 'theme',
            'values' => $this->_getHelper()->getThemeOptions()
        );
    }

    /**
     * _getSummaryField()
     *
     * @return array
     */
    protected function _getSummaryField()
    {
        return array(
            'input'    => 'editor',
            'name'     => 'summary',
            'label'    => $this->_getHelper()->__('Slide Text'),
            'title'    => $this->_getHelper()->__('Slide Text'),
            'style'    => 'width:260px; height:100px;',
            'wysiwyg'  => false,
            'required' => false
        );
    }

    /**
     * _getProductsLinkedField()
     *
     * @return array
     */
    protected function _getProductsLinkedField()
    {
        return array(
            'input'      => 'checkbox',
            'label'      => $this->_getHelper()->__('Link Products?'),
            //'class'    => 'required-entry',
            //'required' => true,
            'name'       => 'products_linked'
        );
    }

    /**
     * _getProductsField()
     *
     * @return array
     */
    protected function _getProductsField()
    {
        return array(
            'input'      => 'text',
            'label'      => $this->_getHelper()->__('Product SKUs'),
            //'class'    => 'required-entry',
            //'required' => true,
            'name'       => 'products'
        );
    }

    /**
     * _getLinkField()
     *
     * @return array
     */
    protected function _getLinkField()
    {
        return array(
            'input'    => 'text',
            'label'    => $this->_getHelper()->__('Click-through Link'),
            'class'    => '',
            'required' => false,
            'name'     => 'link'
        );
    }

    /**
     * _getOrderField()
     *
     * @return array
     */
    protected function _getSequenceField()
    {
        return array(
            'input'     => 'text',
            'label'     => $this->_getHelper()->__('Sequence'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'sequence',
            'style'     => 'width: 60px;'
        );
    }

    /**
     * _getStatusField()
     *
     * @return array
     */
    protected function _getStatusField()
    {
        return array(
            'input'  => 'select',
            'label'  => $this->_getHelper()->__('Status'),
            'name'   => 'status',
            'values' => $this->_getHelper()->getStatusOptions()
        );
    }

    /**
     * _getStoreViewField()
     *
     * @return array
     */
    protected function _getStoreViewField()
    {
        $field = array(
            'name' => 'store_id',
            'required' => true,
            'label' => $this->__('Store View'),
            'title' => $this->__('Store View'),
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $field['input'] = 'select';
            $field['store_id']['values'] = Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true);
        } else {
            $field['input'] = 'hidden';
            $field['value'] = Mage::app()->getStore(true)->getId();
        }

        return $field;
    }
}
