<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();
/**
 * Size Updates
 *
 * Updates configurable options to kids, mens or ladies size
 * Updates configurable product to kids, mens or ladies attribute set
 * Update sizes of simple products to use kids, mens or ladies
 * Updates simple product to kids, mens or ladies attribute set
 *
 */

// Connection for updating attribute ids and size
/** @var Mage_Core_Model_Resource $resource */
$resource = Mage::getModel("core/resource");
/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $resource->getConnection("core_write");

// Helpers
$helper = Mage::helper('greenes_theme/data');
$mapperHelper = Mage::helper('greenes_theme/attribute_mapper');

// Category Id's
$kidsCatId = 573;
$mensCatId = 596;
$ladiesCatIdCatId = 556;

// Attribute Set Id's
$ladiesAttrId = $helper->getAttributeSetId("Ladies");
$mensAttrId = $helper->getAttributeSetId("Mens");
$kidsAttrId = $helper->getAttributeSetId("Kids");

// Size Ids
$eav = Mage::getSingleton('eav/config');
$sizeAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "sizes")->getId();
$kidSizeAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "kids_size")->getId();
$ladiesSizeAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "ladies_size")->getId();
$mensSizeAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "mens_size")->getId();


/** @var Mage_Catalog_Model_Resource_Product_Collection $configurables */
$configurables = Mage::getModel('catalog/product')->getCollection();
$configurables->addAttributeToSelect(array("sku", "name", "sizes", "status"))
    ->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE);


/** @var Mage_Catalog_Model_Product $configurable */
foreach ($configurables as $configurable) {

    $configurableId = $configurable->getId();
    $sku = $configurable->getSku();
    $status = $configurable->getStatus();
    $status = ((int)$status === 1) ? 'enabled' : "disabled";

    $isKids = $helper->hasCategory($configurable, $kidsCatId);
    $isLadies = false;
    $isMens = false;
    if (!$isKids) {
        $isLadies = $helper->hasCategory($configurable, $ladiesCatIdCatId);
        if (!$isLadies) {
            $isMens = $helper->hasCategory($configurable, $mensCatId);
        }
    }

    // Not found
    if (!$isKids && !$isLadies && !$isMens) {
        Mage::log($sku, null, "excluded_" . $status, true);
        continue;
    }

    // Set id, attribute size code, attribute set id,
    $ids = array($configurable->getId());
    $sizeCode = ($isKids) ? "kids_size" : ($isLadies ? "ladies_size" : "mens_size");
    $attrSetId = ($isKids) ? $kidsAttrId : ($isLadies ? $ladiesAttrId : $mensAttrId);
    $configurableSizeId = ($isKids) ? $kidSizeAttrId : ($isLadies ? $ladiesSizeAttrId : $mensSizeAttrId);

    // Set filenames for logging
    $filename = ($isKids) ? "kids_" : ($isLadies ? "ladies_" : "mens_");
    $failedFileName = $filename . "failed_" . $status . ".log";
    $savedFileName = $filename . "saved.log";

    // Update sizes and attribute sets for simple products
    $collection = $configurable->getTypeInstance()->getUsedProducts(array($sizeAttrId), $configurable);

    /** @var Mage_Catalog_Model_Product $product */
    foreach ($collection as $product) {

        $ids[] = $product->getId();
        $size = $product->getAttributeText("sizes");
        if (!$size) {
            Mage::log("No Size found for $sku", null, $failedFileName, true);
            continue;
        }

        // Get New Size
        if ($isKids) {
            $newSize = $mapperHelper->getKidsSize($size);
        } else if ($isLadies) {
            $newSize = $mapperHelper->getLadiesSize($size);
        } else {
            $newSize = $mapperHelper->getMensSize($size);
        }

        if (! $newSize) {
            Mage::log("New Size not found for $sku for size $size", null, $failedFileName, true);
            continue;
        }
        $attribute = Mage::getModel('catalog/resource_product')->getAttribute($sizeCode);
        $optionId = $attribute->getSource()->getOptionId($newSize);
        if (! $optionId) {
            Mage::log("Option Id not found for $sku and option $newSize", null, $failedFileName, true);
            continue;
        }

        # Set the size
        $product->setData($sizeCode, $optionId);
        try {
            $product->getResource()->saveAttribute($product, $sizeCode);
            Mage::log("$sku - size: $newSize - attribute id $attrSetId", null, $savedFileName, true);
        } catch (Exception $e) {
            Mage::log("$sku issues with saving", null, "save_size_error.log", true);
            Mage::log($e->getMessage(), null, "save_size_error.log", true);
        }
    }

    # Update the attribute set id
    $ids = implode(",", $ids);
    $table = $resource->getTableName("catalog/product");
    $query = "UPDATE $table SET attribute_set_id = $attrSetId WHERE entity_id IN ($ids)";
    try {
        $connection->query($query);
    } catch (Exception $e) {
        Mage::log($e->getMessage(), null, "save_attribute_error.log", true);
    }

    # Update the configurable option
    $table = $resource->getTableName("catalog/product");
    $table = $resource->getTableName("catalog/product_super_attribute");
    $query = "UPDATE catalog_product_super_attribute
    SET attribute_id = $configurableSizeId
    WHERE product_id = $configurableId
    AND attribute_id = $sizeAttrId";

    try {
        $connection->query($query);
    } catch (Exception $e) {
        Mage::log($sku, null, "save_id_error.log", true);
        Mage::log($e->getMessage(), null, "save_id_error.log", true);
    }

}




$this->endSetup();