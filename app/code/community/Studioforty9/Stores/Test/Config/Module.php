<?php

class Studioforty9_Stores_Test_Config_Module extends EcomDev_PHPUnit_Test_Case_Config
{
    public function testModuleRegistration()
    {
        $this->assertModuleCodePool('community');
        $this->assertModuleVersion('0.0.1');
    }

    public function testControllerFrontName()
    {
        $this->assertRouteFrontName('stores');
    }

    public function testModuleBlocksRegistered()
    {
        $this->assertConfigNodeHasChild('global/blocks', 'stores');
        $this->assertConfigNodeHasChild('global/blocks', 'stores_adminhtml');
    }
}
