<?php

/**
 * Special Price Source Model
 *
 * Class Greenes_Epos_Model_Source_Specialprice
 *
 * @extends     Greenes_Epos_Model_Source_Price
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Specialprice
    extends Greenes_Epos_Model_Source_Price
{
    /**
     * Gets the special price only if its not equal to the current price
     *
     * @return false|string
     */
    public function getValue()
    {
        $value = $this->getColumnValue();
        if (!$value) {
            return false;
        }

        if (! is_numeric($value)) {
            return false;
        }

        $price = $this->getRowValueByAttributeCode("price");
        $specialPrice = $this->formatPrice($value);
        if ($price === $specialPrice) {
            return false;
        }
        return $specialPrice;
    }

    /**
     * @param float $price
     *
     * @return string
     */
    public function formatPrice($price)
    {
        return number_format($price, 2, ".", "");
    }

    /**
     * @return Greenes_Epos_Helper_Configurable
     */
    public function getAttributeHelper()
    {
        return Mage::helper('greenes_epos/configurable');
    }

}