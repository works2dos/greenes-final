<?php

/**
 * Stock Source Model
 *
 * Class Greenes_Epos_Model_Source_Stock
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Stock
    extends Greenes_Epos_Model_Source_Abstract
{
    /**
     * @return false|array
     */
    public function getValue()
    {
        $stock = (int)$this->getColumnValue();
        if (!$stock && $stock !== 0) {
            return false;
        }

        return array(
            'qty'                     => $stock,
            'use_config_manage_stock' => 0,
            'manage_stock'            => 1,
            'is_in_stock'             => $this->isInStock($stock),
        );

    }

    /**
     * @param integer $stock
     *
     * @return int
     */
    public function isInStock($stock)
    {
        return ((int)$stock === 0) ? 0 : 1;
    }

}