<?php

Mage::getConfig()->saveConfig('design/package/name', 'greenes', 'default', 0);
Mage::getConfig()->saveConfig('store_locator/map_frontend/zoom', '6',  'default', 0);
Mage::getConfig()->saveConfig('store_locator/map_frontend/latitude', '53.4249', 'default', 0);
Mage::getConfig()->saveConfig('store_locator/map_frontend/longitude', '-7.9449', 'default', 0);
Mage::getConfig()->saveConfig('store_locator/map_frontend/icon', '', 'default', 0);
Mage::getConfig()->saveConfig('store_locator/map_backend/zoom', '20', 'default', 0);
Mage::getConfig()->saveConfig('store_locator/map_backend/latitude', '53.4249', 'default', 0);
Mage::getConfig()->saveConfig('store_locator/map_backend/longitude', '-7.9449', 'default', 0);
Mage::getConfig()->saveConfig('store_locator/map_backend/icon', '', 'default', 0);

// Update the company links block
$block = Mage::getModel('cms/block')->load('footer_links_company', 'identifier');
$block->setContent('<div class="links">
<div class="block-title"><strong>Navigate Our Site</strong><span class="fa fa-angle-up">&nbsp;</span></div>
<div class="block-content">
<ul class="popular-links">
<li><a href="{{store url=""}}ladies">Ladies</a></li>
<li><a href="{{store url=""}}mens">Mens</a></li>
<li><a href="{{store url=""}}kids">Kids</a></li>
<li><a href="{{store url=""}}new-arrivals">New Arrival</a></li>
<li><a href="{{store url=""}}sale">Sale</a></li>
</ul>
<ul class="block-links">
<li><a href="{{store url=""}}about-us.html">About Us</a></li>
<li><a href="{{store url=""}}contacts/">Contact Us</a></li>
<!--li><a href="{{store url=""}}terms.html">Terms of Service</a></li-->
<li><a href="{{store url=""}}delivery.html">Delivery</a></li>
<li><a href="{{store url=""}}returns.html">Returns</a></li>
<li><a href="{{store url=""}}privacy-policy.html">Privacy Policy</a></li>
</ul>
</div>
</div>')->save();

// Update the social links block
$block = Mage::getModel('cms/block')->load('footer_social_links', 'identifier');
$block->setTitle('Footer Social Links')->setIdentifier('footer_social_links')->setContent('<div class="social-media">
<h4 class="sm-heading">Follow Us</h4>
<a class="sm-link sm-facebook" href="https://www.facebook.com/Greenesshoes/">Facebook</a> <a class="sm-link sm-twitter" href="http://twitter.com/Greenesshoes">Twitter</a> <a class="sm-link sm-pinterest" href="https://www.pinterest.com/greeneshoes/">Pinterest</a> <a class="sm-link sm-youtube" href="http://www.youtube.com/user/GreenesShoes">YouTube</a> <a class="sm-link sm-instagram" href="https://www.instagram.com/greenes_shoes/">Instagram</a> <a class="sm-link sm-google" href="https://plus.google.com/115002500394431362422">Google+</a>
</div>')->save();

// Disable the footer_links block
$block = Mage::getModel('cms/block')->load('footer_links', 'identifier')->setIsActive(0)->save();
