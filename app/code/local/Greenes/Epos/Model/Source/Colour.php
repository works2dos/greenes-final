<?php

/**
 * Colour Source Model
 *
 * Class Greenes_Epos_Model_Source_Colour
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Colour
    extends Greenes_Epos_Model_Source_Abstract
{
    use Greenes_Epos_Model_Source_Csv_Trait;

    /**
     * @return false|string
     */
    public function getValue()
    {
        return $this->getCsvValue($this->getColumnValue());
    }

    /**
     * @return string
     */
    public function getFileConfigPath()
    {
        return "greenes_epos/simple/colour_csv";
    }

}