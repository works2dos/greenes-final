<?php

/**
 * Name Source Model
 *
 * Appends the colour and size to the name
 *
 * Class Greenes_Epos_Model_Source_Name
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Name
    extends Greenes_Epos_Model_Source_Abstract
{
    /**
     * @return false|string
     */
    public function getValue()
    {
        $name = $this->getColumnValue();
        if (!$name) {
            return false;
        }
        $colour = $this->getColour();
        if ($colour) {
            $name .= " - $colour";
        }

        $size = $this->getSize();
        if ($size) {
            $name .= " - $size";
        }

        return $name;
    }

    /**
     * @return bool|string
     */
    public function getColour()
    {
        $code = "colour";
        $sourceModel = $this->getSourceModelByCode($code);
        if (!$sourceModel) {
            return false;
        }
        $column = $this->getAttributeHelper()->getColumnByCode($code);
        $sourceModel->setRowData($this->getRowData(), $column);
        return $sourceModel->getColourLabel();
    }

    /**
     * @return bool|string
     */
    public function getSize()
    {
        $columnName = $this->getAttributeHelper()->getColumnByCode("ladies_size");
        $size = Mage::getModel("greenes_epos/source_size");
        $size->setRowData($this->getRowData(), $columnName);
        return $size->getValue();
    }


}