<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Validate_Required
 */
class Greenes_Epos_Model_Validate_RequiredSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Greenes_Epos_Model_Validate_Required');
    }

    function it_should_validate()
    {
        $this->validate("123")->shouldReturn(true);
        $this->validate("test")->shouldReturn(true);
        $this->validate("1")->shouldReturn(true);
        $this->validate("Hello World")->shouldReturn(true);
    }

    function it_should_not_validate()
    {
        $this->validate("0")->shouldReturn(false);
        $this->validate("")->shouldReturn(false);
    }

}
