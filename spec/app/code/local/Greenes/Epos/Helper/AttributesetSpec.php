<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Helper_Attributeset
 */
class Greenes_Epos_Helper_AttributesetSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Greenes_Epos_Helper_Attributeset');
    }
    
    function it_is_valid()
    {
        $this->getSize("LAD")->shouldBe("ladies_size");
        $this->getSize("MEN")->shouldBe("mens_size");
        $this->getSize("KID")->shouldBe("kids_size");
        $this->getSize("ACC")->shouldBe("accessories_size");
        $this->getSize("UNI")->shouldBe("unisex_size");
        $this->getSize("fdds")->shouldBe(false);

        $this->getSetId("LAD")->shouldBe(9);
        $this->getSetId("MEN")->shouldBe(10);
        $this->getSetId("KID")->shouldBe(11);
        $this->getSetId("ACC")->shouldBe(12);
        $this->getSetId("UNI")->shouldBe(13);
        $this->getSetId("dfs")->shouldBe(false);
    }
    
    
}
