<?php

/**
 * Configurable Source Model
 *
 * Appends the colour and size to the name
 *
 * Class Greenes_Epos_Model_Source_Name
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Configurable_Name
    extends Greenes_Epos_Model_Source_Name
{
    /**
     * @return false|string
     */
    public function getValue()
    {
        $name = $this->getColumnValue();
        if (!$name) {
            return false;
        }
        $colour = $this->getColour();
        if ($colour) {
            $name .= " - $colour";
        }

        return $name;
    }


}