<?php
/**
 * Studioforty9 Stores Installer - 0.0.1
 *
 * @category   Studioforty9
 * @package    Studioforty9_Stores
 * @author     Copyright (c) 2014 StudioForty9 <info@studioforty9.com>
 */

/* @var $installer Studioforty9_Stores_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**********************************************************************************************************************/

/**
 * Stores Table Installer
 *
 * @table 'studioforty9_stores'
 */
$table = $installer->setupTable('studioforty9_stores/store', array(
    'store_id' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'size'    => 11,
        'options' => array(
            'auto_increment' => true,
            'unsigned'       => true,
            'nullable'       => false,
            'primary'        => true
        )
    ),
    'manager' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 255,
        'options' => array(
            'nullable' => true,
        )
    ),
    'photo' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 255,
        'options' => array(
            'nullable' => true,
        )
    ),
    'name' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 255,
        'options' => array(
            'nullable' => false,
        )
    ),
    'telephone' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 45,
        'options' => array(
            'nullable' => false,
        )
    ),
    'email' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 255,
        'options' => array(
            'nullable' => false,
        )
    ),
    'street' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 255,
        'options' => array(
            'nullable' => false,
        )
    ),
    'city' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 255,
        'options' => array(
            'nullable' => false,
        )
    ),
    'post_code' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 255,
        'options' => array(
            'unsigned' => true,
            'nullable' => false
        )
    ),
    'region' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 255,
        'options' => array(
            'nullable' => false,
        )
    ),
    'country' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'size'    => 2,
        'options' => array(
            'nullable' => false,
        )
    ),
    'latitude' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'size'    => array(11, 8),
        'options' => array(
            'nullable' => false,
        )
    ),
    'longitude' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DECIMAL,
        'size'    => array(11, 8),
        'options' => array(
            'nullable' => false,
        )
    ),
    'content' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_TEXT,
        'options' => array(
            'nullable' => true,
        )
    ),
    'hours' => array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'options' => array(
            'nullable' => true
        )
    ),
    'created_at' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'size'    => null,
        'options' => array(
            'nullable' => false,
        )
    ),
    'updated_at' => array(
        'type'    => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'size'    => null,
        'options' => array(
            'nullable' => false,
        )
    )
));

$table->setOption('type', 'InnoDB');
$table->setOption('charset', 'utf8');
$installer->getConnection()->createTable($table);

/**********************************************************************************************************************/
