<?php

/**
 * Base class for importing/updating data
 *
 * Class Greenes_Epos_Model_Base
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Base extends Greenes_Epos_Model_Log_Abstract
{
    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_";
    }

    /**
     * @return Greenes_Epos_Helper_Attributes
     */
    public function getAttributeHelper()
    {
        return Mage::helper('greenes_epos/attributes');
    }

    /**
     * @return Greenes_Epos_Helper_Attributeset
     */
    public function getAttributeSetHelper()
    {
        return Mage::helper('greenes_epos/attributeset');
    }

    /**
     * @return Greenes_Epos_Helper_Import
     */
    public function getImportHelper()
    {
        return Mage::helper('greenes_epos/import');
    }

    /**
     * @return string|false
     */
    public function getStockField()
    {
        return Mage::getStoreConfig("greenes_epos/stock/stock_column");
    }

    /**
     * @return string|false
     */
    public function getSkuField()
    {
        return Mage::getStoreConfig("greenes_epos/stock/sku_column");
    }

    /**
     * @param array $itemIds
     *
     * @return bool
     */
    public function deleteItems(array $itemIds)
    {
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
        $connection = $resource->getConnection("core_write");

        $table = $resource->getTableName("greenes_epos/import");
        $itemIds = implode(", ", $itemIds);
        $query = "DELETE FROM $table WHERE id IN ($itemIds)";

        try {
            $connection->query($query);
            return true;
        } catch (Exception $e) {
            $this->logError($e->getMessage());
            return false;
        }
    }


}
