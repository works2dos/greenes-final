<?php

/**
 * Class Greenes_Epos_Model_Csv_Aggregate
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Theme
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Csv_Aggregate extends Greenes_Epos_Model_Base
{
    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_csv_aggregate_";
    }

    /**
     * @return bool
     */
    public function generateFile()
    {
        $fileName = $this->getFile();
        if (!$fileName) {
            $this->logError("No file to create CSV file");
            return false;
        }

        $csvData = $this->getCsvData($fileName);
        if (!$csvData) {
            $this->logError("Could not get CSV data");
            return false;
        }

        $aggregatedData = $this->getAggregatedData($csvData);
        if (!$aggregatedData) {
            return false;
        }

        try {
            $csv = new Varien_File_Csv();
            $newFileName = $this->getImportPath() . "aggregate.csv";
            $csv->saveData($newFileName, $aggregatedData);
            return true;
        } catch (Exception $e) {
            $this->logError("Could not create CSV file");
            $this->logError($e->getMessage());
            return false;
        }
    }

    /**
     * @return string|false
     */
    public function getFile()
    {
        $files = $this->getFiles();
        if (empty($files)) {
            return false;
        }
        return $files[0];
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        $path = $this->getImportPath();
        return glob($path . '*GREENESD.CSV');
    }

    /**
     * @return mixed
     */
    public function getImportPath()
    {
        return Mage::helper('greenes_epos')->getImportPath();
    }

    /**
     * @param $fileName
     *
     * @return array|bool
     */
    public function getCsvData($fileName)
    {
        $csv = new Varien_File_Csv();
        try {
            $aggregatedData = $csv->getData($fileName);
        } catch (Exception $e) {
            $this->logError($e->getMessage());
            return false;
        }
        return $aggregatedData;
    }

    /**
     * This combines SKU rows by their stock level
     *
     * @param array $csvData
     *
     * @return array|bool
     */
    public function getAggregatedData(array $csvData)
    {
        /**
         * Get Header Id's
         */
        $skuKey = $this->getSkuHeaderKey();
        $stockKey = $this->getStockHeaderKey();
        if ($skuKey === false) {
            $this->logError("No SKU field for the CSV Header");
            return false;
        }
        if ($stockKey === false) {
            $this->logError("No Stock field for the CSV Header");
            return false;
        }

        $aggregatedData = array();
        foreach ($csvData as $row) {

            $sku = $this->cleanString($row[$skuKey]);
            if (strlen($sku) < 2 || !$sku) {
                $this->logWarning("No SKU for the row");
                $this->logWarning($row);
                continue;
            }

            $stockLevel = $this->cleanString($row[$stockKey]);
            if (!$stockLevel) {
                $this->logWarning("No Stock Level for the row");
                $this->logWarning($row);
                continue;
            }
            $stockLevel = (int)$stockLevel;

            /**
             * Row exists we just add the old stock level and replace the old data
             *
             */
            if (array_key_exists($sku, $aggregatedData)) {
                $stockLevel = $this->aggregateStockLevel($stockLevel, $stockKey, $aggregatedData[$sku]);
            }

            $row[$stockKey] = $stockLevel;
            $aggregatedData[$sku] = $row;
        }

        if (empty($aggregatedData)) {
            $this->logError("No Data to aggregate");
            return false;
        }

        array_unshift($aggregatedData, $this->getCsvHeaders());

        return $aggregatedData;
    }

    /**
     * @return bool|mixed
     */
    public function getSkuHeaderKey()
    {
        $headers = $this->getCsvHeaders();
        if (!$headers) {
            return false;
        }
        $skuField = $this->getSkuField();
        if (!$skuField) {
            return false;
        }
        return array_search($skuField, $headers);
    }

    /**
     * @return array
     */
    public function getCsvHeaders()
    {
        return array(
            "T2TBarCode",
            "ItemRef",
            "ItemName",
            "ItemDesc",
            "ColCode",
            "ColName",
            "Size",
            "Sno",
            "Sell1",
            "Sell2",
            "Sell3",
            "Group",
            "Stype",
            "User1",
            "User2",
            "Supplier",
            "StockQty",
            "Notes"
        );
    }

    /**
     * @return bool|mixed
     */
    public function getStockHeaderKey()
    {
        $headers = $this->getCsvHeaders();
        if (!$headers) {
            return false;
        }

        $stockField = $this->getStockField();
        if (!$stockField) {
            return false;
        }
        return array_search($stockField, $headers);
    }


    /**
     * @param $string
     *
     * @return string
     */
    public function cleanString($string)
    {
        $key = trim($string);
        $helper = Mage::helper("core/string");
        $key = $helper->cleanString($key);
        return $helper->escapeHtml($key);
    }

    /**
     * @param       $stockLevel
     * @param       $stockKey
     * @param array $row
     *
     * @return mixed
     */
    public function aggregateStockLevel($stockLevel, $stockKey, array $row)
    {
        return $stockLevel + (int)$row[$stockKey];
    }

}