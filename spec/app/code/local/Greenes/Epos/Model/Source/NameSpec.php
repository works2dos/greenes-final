<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Name
 */
class Greenes_Epos_Model_Source_NameSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Name');
    }

    function it_should_be_valid()
    {
        $nameColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("name");
        $colourColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("colour");
        $sizeColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("ladies_size");
        $categoryColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("category");

        $name = "Sample Product";
        $colour = "BLA";
        $size = 7;
        $category = "LAD";

        $data = array(
            $nameColumn     => $name,
            $colourColumn   => $colour,
            $sizeColumn     => $size,
            $categoryColumn => $category,
        );

        $matchedTitle = "Sample Product - Black - 7 (40)";
        $this->setRowData($data, $nameColumn);
        $this->getValue()->shouldBeString();
        $this->getValue()->shouldBe($matchedTitle);
    }

}
