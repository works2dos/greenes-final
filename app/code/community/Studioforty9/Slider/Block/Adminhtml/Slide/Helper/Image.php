<?php

class Studioforty9_Slider_Block_Adminhtml_Slide_Helper_Image extends Varien_Data_Form_Element_Image
{
    /**
     * _getUrl()
     *
     * @return bool|string
     */
    protected function _getUrl()
    {
        $url = false; // TODO: Check if returning false is necessary
        $type = $this->getName();
        $value = $this->getValue();

        if (is_array($value)) {
            $value = $value['value'];
        }
        if (!empty($value)) {
            $url = Mage::getBaseUrl('media') . 'slide/' . $type . '/'. $value;
        }

        return $url;
    }
}
