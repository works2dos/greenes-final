<?php

class Studioforty9_Slider_Block_Adminhtml_Renderer_Image
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * render()
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $index = $this->getColumn()->getIndex();
        $value = $row->getData($index);
        if (empty($value) || !$this->_imageExists($value)) {
            return 'None';
        }

        return sprintf(
            '<p style="text-align:center;padding-top:5px;"><img src="%s/slide/%s/%s" style="width:100px;"/></p>',
            Mage::getBaseUrl('media'),
            $index,
            $value
        );
    }

    /**
     * imageExists()
     *
     * @param $image
     * @return bool
     */
    protected function _imageExists($image)
    {
        if (is_array($image)) {
            $image = $image[0];
        }

        $imagePath = sprintf(
            '%s/slide/%s/%s',
            Mage::getBaseDir('media'),
            $this->getColumn()->getIndex(),
            $image
        );

        $fileIo = new Varien_Io_File();
        return $fileIo->fileExists($imagePath, true);
    }
}
