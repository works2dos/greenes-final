<?php

class Studioforty9_Slider_Model_Slide extends Mage_Core_Model_Abstract
{
    const STATUS_INACTIVE = '0';
    const STATUS_ACTIVE   = '1';
    const THEME_DARK      = 'dark';
    const THEME_LIGHT     = 'light';

    /**
     * This tells Magento where the related Resource Model can be found.
     *
     * For a Resource Model, Magento will use the standard Model alias,
     * in this case 'studioforty9_slider' and look in
     * config.xml for a child node <resourceModel/>. This will be the
     * location Magento will look for a Model when
     * Mage::getResourceModel() is called. In our case:
     * Studioforty9_Banners_Model_Resource
     */
    protected function _construct()
    {
        $this->_init('studioforty9_slider/slide');
    }

    /**
     * This method is used in the grid for populating a dropdown of available statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return array(
            self::STATUS_ACTIVE   => $this->_getHelper()->__('Active'),
            self::STATUS_INACTIVE => $this->_getHelper()->__('Inactive'),
        );
    }

    /**
     * This method is used in the grid for populating a dropdown of theme options.
     *
     * @return array
     */
    public function getThemeOptions()
    {
        return array(
            self::THEME_DARK   => $this->_getHelper()->__('Dark'),
            self::THEME_LIGHT => $this->_getHelper()->__('Light'),
        );
    }
    
    /**
     * This method is used in the form for populating a dropdown.
     *
     * @return array
     */
    public function getStatusOptions()
    {
        return array(
            array(
                'value' => self::STATUS_ACTIVE,
                'label' => $this->_getHelper()->__('Active'),
            ),
            array(
                'value' => self::STATUS_INACTIVE,
                'label' => $this->_getHelper()->__('Inactive'),
            ),
        );
    }

    /**
     * Perform some actions just before a Brand is saved.
     *
     * @return $this
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        $this->setSequence((int) $this->getSequence());
        $this->_updateTimestamps();
        $this->_prepareUrlKey();
        
        return $this;
    }

    /**
     * Set the last updated timestamp and if we have a new object,
     * set the created timestamp.
     *
     * @return $this
     */
    protected function _updateTimestamps()
    {
        $timestamp = now(); // TODO: Make sure we use the correct method of passing dates
        $this->setUpdatedAt($timestamp);
        if ($this->isObjectNew()) {
            $this->setCreatedAt($timestamp);
        }
        
        return $this;
    }

    /**
     * In this method you might consider ensuring the URL Key entered
     * is unique and contains only alphanumeric characters.
     *
     * @return $this
     */
    protected function _prepareUrlKey()
    {
        return $this;
    }

    /**
     * _getHelper()
     *
     * @return Studioforty9_Slider_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('studioforty9_slider');
    }

    /**
     * _getImagePath
     *
     * @param $type
     * @return string
     */
    protected function _getImagePath($type)
    {
        $path = Mage::helper('studioforty9_slider')->getImagePath($type);
        return $path . $this->getData($type);
    }

    /**
     * Get the image of a certain type: image, image_tablet or image_mobile
     *
     * @param $type
     * @return string
     */
    protected function _getImageUrl($type)
    {
        return sprintf('%sslide/%s/%s',
            Mage::getBaseUrl('media'),
            $type,
            $this->getData($type)
        );
    }
    
    public function exists()
    {
        $large = $this->_getImagePath('image');
        
        return file_exists($large);
    }

    public function getImageUrl()
    {
        return $this->_getImageUrl('image');
    }

    public function getImageTabletUrl()
    {
        return $this->_getImageUrl('image_tablet');
    }

    public function getImageMobileUrl()
    {
        return $this->_getImageUrl('image_mobile');
    }

    public function getResizedImage($type, $width, $height)
    {
        $image = Mage::helper('studioforty9_slider/image');
        $image->init($this->_getImagePath($type), $type);
        $image->setBackgroundColor($this->getColor());
        $image->resize($width, $height);

        return $image;
    }

    /**
     * Get the lowercased version of the theme to be used as a CSS class.
     *
     * @return string
     */
    public function getThemeClass()
    {
        return strtolower($this->getData('theme'));
    }
}
