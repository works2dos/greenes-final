<?php
class Studioforty9_Slider_Model_Resource_Slide extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Tell Magento the database name and primary key field to persist
     * data to. Similar to the _construct() of our Model, Magento finds
     * this data from config.xml by finding the <resourceModel/> node
     * and locating children of <entities/>.
     *
     * In this example:
     * - studioforty9_slider is the Model alias
     * - slide is the entity referenced in config.xml
     * - entity_id is the name of the primary key column
     *
     * As a result Magento will write data to the table
     * 'studioforty9_slider' and any calls to
     * $model->getId() will retrieve the data from the column
     * named 'entity_id'.
     */
    protected function _construct()
    {
        $this->_init('studioforty9_slider/slide', 'entity_id');
    }
}
