<?php

class Studioforty9_Slider_Block_Slide extends Mage_Core_Block_Template
{
    /**
     * @var Varien_Data_Collection $_collection
     */
    protected $_collection;

    /**
     * getCollection()
     *
     * @param int    $limit
     * @param string $orderBy
     * @param string $direction
     * @return Varien_Data_Collection
     */
    public function getCollection($limit = 0, $orderBy = 'sequence', $direction = 'ASC')
    {
        $this->_collection = $this->getModel()->getCollection();

        // Only return active slides
        $this->_collection->getSelect()->where('status', Studioforty9_Slider_Model_Slide::STATUS_ACTIVE);
        // Limit the number of slides to be shown
        if ($limit > 0) {
            $this->_collection->getSelect()->limit($limit);
        }
        // Order by user defined order
        if ($orderBy !== null && $direction !== null) {
            $this->_collection->addOrder($orderBy, $direction);
        }

        return $this->_collection;
    }

    /**
     * getModel()
     *
     * @return Studioforty9_Slider_Model_Slide
     */
    protected function getModel()
    {
        return Mage::getModel('studioforty9_slider/slide');
    }

}
