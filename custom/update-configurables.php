<?php
require_once('../app/Mage.php');
Mage::app();


/**
 * Splits Configurable Out from colour and sizes to different colours
 * e.g. Configurable Red, Green & Blue, will become 3 separate configurable products
 *
 */


/**
 * Get configurable products by colour and size
 *
 * If they have 1 colour
 *
 * - Update SKU
 * - Set colour
 *
 * Else
 */


/**
 * For each colour
 *
 * 1. Duplicate by size
 * 2. Append colour to sku
 * 3. Append colour to url
 * 4. Set status to enabled
 * 5. Set colour
 * 6. Set Stock in stock
 * 7. Associate products
 *
 */

/**
 * 2. Remove Original Configurable Product
 * 3. Relate Products
 */

# Tests
$sku = '001471'; // Mens
$sku = '011596'; // Ladies
$sku = '011595'; // 1 Product
$sku = '011186'; // Kids
$sku = '011632'; // Disabled
#$collection->addAttributeToFilter("sku", $sku);

# Get Products
$collection = Mage::getModel('catalog/product')->getCollection()
    ->addFieldToFilter("type_id", Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE)
    #->addFieldToFilter("status", Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
    #->addFieldToFilter("attribute_set_id", 9)
    #->addFieldToFilter("entity_id", array('lt' => 18402))
    #->addFieldToFilter("entity_id", array('gt' => 17756))
    ->addAttributeToSelect("*");
$collection->addOrder("entity_id", Zend_Db_Select::SQL_ASC);

/*
$collection->getSelect()->limit(150);
var_dump($collection->count());
var_dump($collection->getFirstItem()->getId());
var_dump($collection->getLastItem()->getId());
die();
*/

# Register as secure area to allow products to be deleted
Mage::register('isSecureArea', true);


# Resource Models
$eavModel = Mage::getModel('eav/entity_attribute');
$productResource = Mage::getResourceModel("catalog/product");
$linkResource = Mage::getResourceModel('catalog/product_link');
$transaction = Mage::getModel('core/resource_transaction');

# Database Connection
/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = Mage::getModel("core/resource")->getConnection("core_read");

# Helpers
/** @var Greenes_Theme_Helper_Media $mediaHelper */
$mediaHelper = Mage::helper("greenes_theme/media");

# Get Attributes
$colourAttr = $productResource->getAttribute("colour");
$kidsAttr = $productResource->getAttribute("kids_size");
$ladiesAttr = $productResource->getAttribute("ladies_size");
$mensAttr = $productResource->getAttribute("mens_size");

$colourAttrId = (int)$colourAttr->getId();
$kidsAttrId = (int)$kidsAttr->getId();
$ladiesAttrId = (int)$ladiesAttr->getId();
$mensAttrId = (int)$mensAttr->getId();

foreach ($collection as $originalProduct) {

    $sku = $originalProduct->getSku();
    $status = $originalProduct->getStatus();

    # Get Gallery Images
    $media = $mediaHelper->getMediaByProductId($originalProduct->getId());

    # Get Attribute
    $attributes = array();
    $setId = (int)$originalProduct->getData('attribute_set_id');
    $sizeCode = ($setId === 11)
        ? "kids_size"
        : ($setId === 10 ? "mens_size" : "ladies_size");
    $attributeCodes = array($sizeCode);
    foreach ($attributeCodes as $code) {
        $attributes[$eavModel->getIdByCode('catalog_product', $code)] = $code;
    }

    /**
     * Separate Product Data into different colours
     *
     * We use a direct SQL query as if we use getTypeInstance it causes issues with saving associated products
     */
    $colours = array();
    $associatedProducts = array();
    $parent_id = $originalProduct->getId();
    $query = "SELECT product_id FROM catalog_product_super_link WHERE parent_id = $parent_id";

    $ids = $connection->fetchCol($query);
    foreach ($ids as $simpleId) {

        $sizeId = $productResource->getAttributeRawValue($simpleId, $sizeCode, 1);
        // Some sizes were removed
        if (!$sizeId) {
            Mage::log("$sku,$simpleId", null, "no_size_$status.log", true);
            continue;
        }

        $colourId = $productResource->getAttributeRawValue($simpleId, 'colour', 1);
        if (!$colourId) {
            Mage::log("$sku,$simpleId", null, "no_colour_$status.log", true);
            continue;
        }
        $colour = $colourAttr->getSource()->getOptionText($colourId);
        $colourKey = strtolower($colour);
        $colours[$colourKey] = $colour;
        $associatedProducts[$colourKey][] = $simpleId;
    }

    if (count($colours) === 0) {
        Mage::log($originalProduct->getSku(), null, "no_configurable_colour_$status.log", true);
        continue;
    }

    if (count($colours) === 1) {
        $colourCode = array_keys($colours)[0];
        $colour = array_values($colours)[0];

        $newSku = $sku . '-' . $colourCode;
        $originalProduct->setSku($newSku);
        $originalProduct->setName($originalProduct->getName() . " - " . $colour);
        $originalProduct->setUrlKey($originalProduct->getUrlKey() . " - " . $colourCode);
        $originalProduct->setData("colour", $colourAttr->getSource()->getOptionId($colour));
        $logMessage = $sku . "," . $colour;

        try {
            $originalProduct->save();
            Mage::log($logMessage, null, "configurable_updated.log", true);
        } catch (Exception $e) {
            Mage::log($logMessage, null, "configurable_updated_failed_$status.log", true);
            Mage::log($e->getMessage(), null, "configurable_updated_failed_exception_$status.log", true);
        }
        continue;
    }

    $relatedProducts = array();
    $failed = false;
    foreach ($colours as $colourCode => $colour) {

        if ($failed === true) {
            continue;
        }

        // Create Product
        /** @var Mage_Catalog_Model_Product $configProduct */
        $newSku = $sku . '-' . $colourCode;
        $configProduct = $originalProduct->duplicate();
        $configProduct->setSku($newSku);
        $configProduct->setStatus($status);
        $configProduct->setTaxClassId(0);
        $configProduct->setName($originalProduct->getName() . " - " . $colour);
        $configProduct->setUrlKey($originalProduct->getUrlKey() . " - " . $colourCode);
        $configProduct->setData("colour", $colourAttr->getSource()->getOptionId($colour));
        $configProduct->unsetData('stock_data'); // Causes issues with saving product data

        // Set Configurable Attribute
        $configProduct->getTypeInstance()->setUsedProductAttributeIds(array_keys($attributes));
        $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();
        $configProduct->setCanSaveConfigurableAttributes(true);

        // Set Associated Product
        $configurableProductsData = array();
        foreach ($associatedProducts[$colourCode] as $simpleId) {

            $simpleProduct = Mage::getModel("catalog/product")->load($simpleId);
            $i = 0;
            $data = array();
            foreach ($attributes as $attributeId => $attributeCode) {
                $data[] = array(
                    $i => array(
                        'label'         => $simpleProduct->getAttributeText($attributeCode),
                        'attribute_id'  => $attributeId,
                        'value_index'   => $simpleProduct->getData($attributeCode),
                        'is_percent'    => '0',
                        'pricing_value' => '0'
                    )
                );
                $i++;
                $configurableProductsData[$simpleProduct->getId()] = $data;
            }
        }

        $valuesArray = array();
        foreach ($configurableProductsData as $data) {
            $valuesArray[] = $data;
        }
        $configurableAttributesData[0]['values'] = $valuesArray;
        $configProduct->setConfigurableProductsData($configurableProductsData);
        $configProduct->setConfigurableAttributesData($configurableAttributesData);
        $configProduct->setHasTypeOptions(true);

        // Save Product
        $logMessage = $configProduct->getSku() . "," . $sku . "," . $colour;
        try {
            $configProduct->save();
            Mage::log($logMessage, null, "configurable_saved.log", true);
            $relatedProducts[$configProduct->getId()] = $configProduct;

        } catch (Exception $e) {
            Mage::log($logMessage, null, "configurable_failed_$status.log", true);
            Mage::log($e->getMessage(), null, "configurable_failed_exception_$status.log", true);
            $failed = true;
            continue;
        }

        // Update Media Gallery
        $mediaHelper->insertMediaGallery($media, $configProduct->getId());

        // Stock
        $stockItem = Mage::getModel('cataloginventory/stock_item');
        $stockItem->assignProduct($configProduct);
        $stockItem->setData('use_config_manage_stock', 0);
        $stockItem->setData('manage_stock', 0);
        $transaction->addObject($stockItem);
    }

    // Failed Products
    if ($failed) {
        /** @var Mage_Catalog_Model_Product $product */
        foreach ($relatedProducts as $id => $product) {
            $message = $configProduct->getSku() . "," . $sku;
            Mage::log($message, null, 'deleted_product.log', true);
            $product->delete();
        }

        continue;
    }

    // Related Products
    $relatedLinkData = array();
    $i = 1;
    foreach ($relatedProducts as $id => $product) {
        $relatedLinkData[$id] = array("position" => $i);
        $i++;
    }
    foreach ($relatedProducts as $id => $product) {
        $linkData = $relatedLinkData;
        unset($linkData[$id]);

        try {
            $linkResource->saveProductLinks($product, $linkData, Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED);
        } catch (Exception $e) {
            Mage::log("Related ID: $id", null, "related_failed_$status.log", true);
            Mage::log($linkData, null, "related_failed_$status.log", true);
            Mage::log($e->getMessage(), null, "related_exception_$status.log", true);
        }
    }


    // Delete Original Product
    try {
        $originalProduct->delete();
    } catch (Exception $e) {
        Mage::log($sku, null, "should_be_deleted.log", true);
        Mage::log($e->getMessage(), null, "should_be_deleted_exception.log", true);
    }
}

// Save Stock
try {
    $transaction->save();
} catch (Exception $e) {
    Mage::log($e->getMessage(), null, "stock_failed.log", true);
}
