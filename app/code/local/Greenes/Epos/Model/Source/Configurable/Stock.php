<?php

/**
 * Configurable Stock Source Model
 *
 * Class Greenes_Epos_Model_Source_Configurable_Stock
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Configurable_Stock
    extends Greenes_Epos_Model_Source_Abstract
{
    /**
     * @return false|array
     */
    public function getValue()
    {
        return array(
            'use_config_manage_stock' => 0,
            'manage_stock'            => 0,
        );
    }

}