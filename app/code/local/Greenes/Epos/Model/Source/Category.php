<?php

/**
 * Category Source Model
 *
 * Class Greenes_Epos_Model_Source_Category
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Category
    extends Greenes_Epos_Model_Source_Abstract
{
    use Greenes_Epos_Model_Source_Csv_Trait;

    /**
     * @param string $value
     *
     * @return false|string
     */
    public function getValue()
    {
        $value = $this->getColumnValue();
        if (! $value) {
            return false;
        }
        $name = $this->getCsvValue($value);
        if (!$name) {
            return false;
        }

        $parentId = $this->getParentCategoryId();
        if (! $parentId) {
            return false;
        }
        return $this->getCategoryId($name, $parentId);
    }

    /**
     * Parent Category Id
     *
     * @return int
     */
    public function getParentCategoryId()
    {
        $id = (int)$this->getData("parent_category_id");
        if ($id === 0) {
            return $this->getDefaultCategoryId();
        }

        return $id;
    }

    /**
     * Root Category ID
     *
     * @return int
     */
    public function getDefaultCategoryId()
    {
        return (int)Mage::app()->getStore(1)->getRootCategoryId();
    }

    /**
     * Get Category ID
     *
     * @param $name
     * @param $parentCategoryId
     *
     * @return false|int
     */
    public function getCategoryId($name, $parentCategoryId)
    {
        if (! $name) {
            return false;
        }

        if (! $parentCategoryId) {
            return false;
        }
        /** @var Magento_Db_Adapter_Pdo_Mysql $conn */
        $conn = Mage::getSingleton('core/resource')->getConnection("core_write");
        $attributeId = 41;

        $query
            = "SELECT c.entity_id
        FROM catalog_category_entity as c
        INNER JOIN catalog_category_entity_varchar as v ON (
          c.entity_id = v.entity_id
          AND v.attribute_id = $attributeId
          AND v.value = '$name'
        )
        WHERE c.parent_id = $parentCategoryId
        LIMIT 0,1
        ";

        $row = $conn->fetchCol($query);

        return (array_key_exists(0, $row))
            ? (int)$row[0]
            : false;

    }

    /**
     * @return string
     */
    public function getFileConfigPath()
    {
        return "greenes_epos/simple/category_csv";
    }

}