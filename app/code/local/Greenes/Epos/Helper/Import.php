<?php

/**
 * Class Greenes_Epos_Helper_Import
 *
 * @extends     Mage_Core_Helper_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Helper_Import extends Mage_Core_Helper_Abstract
{

    /**
     * Gets the CSV fields
     *
     * @return false|array
     */
    public function toOptionArray()
    {
        static $fields = array();

        if (!empty($fields)) {
            return $fields;
        }

        $config = Mage::getStoreConfig("greenes_epos/import/csv_headers");
        if (!$config) {
            return false;
        }

        $config = unserialize($config);
        if (!is_array($config)) {
            return false;
        }

        foreach ($config as $row) {
            $key = $row['column'];
            $value = trim($row['validation_model']);
            if ($value) {
                $fields[$key] = $value;
            }
        }

        return $fields;
    }

    /**
     * @param string $attribute
     *
     * @return false|string
     */
    public function getValidationModel($key)
    {
        $attributes = $this->toOptionArray();
        if (!$attributes) {
            return false;
        }

        return array_key_exists($key, $attributes)
            ? $attributes[$key]
            : false;
    }
}