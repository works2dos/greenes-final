<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Price
 */
class Greenes_Epos_Model_Source_PriceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Price');
    }

    function it_should_be_valid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("price");
        $data = array($column => "20.00");
        $this->setRowData($data, $column);
        $this->getValue()->shouldBeString();

        $data = array($column => "200");
        $this->setRowData($data, $column);
        $this->getValue()->shouldBeString();

        $data = array($column => "1.666");
        $this->setRowData($data, $column);
        $this->getValue()->shouldBeString();
    }

    function it_should_be_invalid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("price");
        $data = array($column => "abc");
        $this->setRowData($data, $column);
        $this->getValue()->shouldBe(false);
        $this->setRowData($data);
        $this->getValue()->shouldBe(false);
    }

}
