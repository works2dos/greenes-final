<?php

require_once('../app/Mage.php');
Mage::app();


/**
 * Update SKU based off the EPoS CSV file
 * Save old SKU into another attribute for reference
 *
 * Also log any products missing from the system
 *
 */

/** @var Mage_Core_Model_Resource $resource */
$resource = Mage::getModel("core/resource");
/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $resource->getConnection("core_write");

// Attribute Ids
$eav = Mage::getSingleton('eav/config');
$sizeAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "sizes")->getId();
$colourAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "colour")->getId();
$nameAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "name")->getId();
$importedSkuAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "imported_sku")->getId();

/**
 * Gets sku, name, colour and size from catalog where imported_sku is not set
 *
 */

$query
    = "
SELECT c.entity_id as id, c.sku as sku, v.value as name, i.value as colour, s.value as size
FROM catalog_product_entity as c
INNER JOIN catalog_product_entity_varchar as v ON (c.entity_id = v.entity_id AND v.attribute_id = $nameAttrId)
INNER JOIN catalog_product_entity_int as i ON (c.entity_id = i.entity_id AND i.attribute_id = $colourAttrId)
INNER JOIN catalog_product_entity_int as s ON (c.entity_id = s.entity_id AND s.attribute_id = $sizeAttrId)
WHERE c.type_id = 'simple'
AND NOT EXISTS (
  SELECT t.entity_id
  FROM catalog_product_entity_text as t
  WHERE t.entity_id = c.entity_id
  AND t.attribute_id = $importedSkuAttrId
  AND t.value IS NOT NULL
)
ORDER BY c.entity_id";



$productCollection = $connection->fetchAll($query);


/**
 * Helpers for updating data
 */
$storeId = 0;
$helper = Mage::helper("greenes_theme");
$sourceModel = Mage::getSingleton('greenes_epos/source_colour');

/** @var Mage_Catalog_Model_Resource_Product $resource */
$resource = Mage::getResourceModel('catalog/product');

/** @var Mage_Catalog_Model_Resource_Product_Action $action */
$action = Mage::getModel('catalog/resource_product_action');

// Attributes
$sizeAttr = $resource->getAttribute("sizes");
$colourAttr = $resource->getAttribute("colour");

/** @var array $product */
foreach ($productCollection as $product) {

    $productId = $product['id'];
    $sku = $product['sku'];
    $colourId = $product['colour'];
    $sizeId = $product['size'];
    $name = $product['name'];

    $colorValue = $colourAttr->getSource()->getOptionText($colourId);
    if (!$colorValue) {
        Mage::log("$sku,$colourId,$name", null, 'colour_not_found.log', true);
        continue;
    }

    $size = $sizeAttr->getSource()->getOptionText($sizeId);
    if (!$size) {
        Mage::log("$sku,$sizeId,$name", null, 'size_not_found.log', true);
        continue;
    }

    $color = $sourceModel->getValue($colorValue);
    if (!$color) {
        Mage::log("$sku,$colorValue,$name", null, 'colour_code_not_found.log', true);
        continue;
    }


    $itemRef = substr($sku, 0, strrpos($sku, "-"));

    /** @var Greenes_Epos_Model_Resource_Import_Collection $collection */
    $collection = Mage::getModel("greenes_epos/import")->getCollection()
        ->addFieldToFilter("ItemRef", $itemRef)
        ->addFieldToFilter("ColCode", $color)
        ->addFieldToFilter("Size", $size);

    if (!$collection->count()) {
        Mage::log("$sku,$itemRef,$color,$size", null, 'sku_not_found.log', true);
        continue;
    }

    /** @var Greenes_Epos_Model_Import $row */
    $row = $collection->getFirstItem();
    $newSku = $row->getData("T2TBarCode");
    if (!$newSku) {
        Mage::log("$sku,$itemRef,$color,$size", null, 'new_sku_not_found.log', true);
        continue;
    }

    # Update SKU
    $sql = "UPDATE catalog_product_entity SET sku = '$newSku' WHERE entity_id = $productId";
    try {
        $connection->query($sql);
    } catch (Exception $e) {
        Mage::log("$newSku,$sku", null, 'sku_not_saved.log', true);
        Mage::log($e->getMessage(), null, 'sku_not_saved_exception.log', true);
        continue;
    }
    Mage::log("$newSku,$sku,$itemRef,$color,$size", null, 'sku_saved.log', true);

    # Save Imported SKU
    try {
        $action->updateAttributes(array($productId), array('imported_sku' => $sku), $storeId);
    } catch (Exception $e) {
        Mage::log("$newSku,$sku", null, 'imported_sku_not_saved.log', true);
        Mage::log($e->getMessage(), null, 'imported_sku_exception.log', true);
    }

    # Delete Row from epos table
    try {
        $row->delete();
    } catch (Exception $e) {
        Mage::log($row->getId(), null, 'row_not_deleted.log', true);
        continue;
    }
}
