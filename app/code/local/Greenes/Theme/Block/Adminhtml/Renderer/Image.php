<?php

/**
 * Class Greenes_Theme_Block_Adminhtml_Renderer_Image
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Theme
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Theme_Block_Adminhtml_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param Varien_Object $row
     *
     * @return mixed|string
     */
    public function render(Varien_Object $row)
    {
        return $this->_getValue($row);
    }

    protected function _getValue($row)
    {
        $val = $row->getData($this->getColumn()->getIndex());
        $val = str_replace("no_selection", "", $val);
        if (! $val) {
            return "No Image";
        }
        $url = Mage::getBaseUrl("media") . "catalog/product" . $val;
        return "<img src='$url' width='60px' />";
    }
}