<?php
/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();

$conn = $this->getConnection();
/** @var Varien_Db_Ddl_Table $table */
$table = $conn->newTable($this->getTable('greenes_epos/import'));
$table
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        array(
            'nullable' => false,
            'auto_increment' => true,
            'primary' => true
        )
    )
    ->addColumn(
        'T2TBarCode',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'nullable' => false
        )
    )
    ->addColumn(
        'ItemDesc',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(
            'nullable' => true
        )
    )
    ->addColumn(
        'ItemRef',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'nullable' => false
        )
    )
    ->addColumn(
        'ItemName',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'nullable' => false
        )
    )
    ->addColumn(
        'StockQty',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        array(
            'nullable' => false
        )
    )
    ->addColumn(
        'ColCode',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'ColName',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'Size',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'Sno',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'Sell1',
        Varien_Db_Ddl_Table::TYPE_DECIMAL,
        '12,4'
    )
    ->addColumn(
        'Sell2',
        Varien_Db_Ddl_Table::TYPE_DECIMAL,
        '12,4'
    )
    ->addColumn(
        'Sell3',
        Varien_Db_Ddl_Table::TYPE_DECIMAL,
        '12,4'
    )
    ->addColumn(
        'Group',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'Stype',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'User1',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'User2',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'Supplier',
        Varien_Db_Ddl_Table::TYPE_VARCHAR
    )
    ->addColumn(
        'Notes',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        255,
        array(
            'nullable' => true
        )
    )
    ->addColumn(
        'imported_at',
        Varien_Db_Ddl_Table::TYPE_TIMESTAMP,
        null,
        array(
            'nullable' => false,
            "default"  => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
        )
    );

$this->getConnection()->createTable($table);
$this->endSetup();