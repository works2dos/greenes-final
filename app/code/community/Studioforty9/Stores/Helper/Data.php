<?php
/**
 * Studioforty9 Stores Helper
 *
 * @category    Studioforty9
 * @package     Studioforty9_Stores
 * @copyright   Copyright (c) 2014 StudioForty9 (http://www.studioforty9.com)
 */
class Studioforty9_Stores_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**#@+
     * Backend Configuration paths
     * @var string
     * @constant
     */
    const BACKEND_MAP_DEFAULT_ZOOM      = 'store_locator/map_backend/zoom';
    const BACKEND_MAP_DEFAULT_LATITUDE  = 'store_locator/map_backend/latitude';
    const BACKEND_MAP_DEFAULT_LONGITUDE = 'store_locator/map_backend/longitude';
    const BACKEND_MAP_DEFAULT_ICON      = 'store_locator/map_backend/icon';
    /**#@-*/

    /**#@+
     * Frontend Configuration paths
     * @var string
     * @constant
     */
    const FRONTEND_MAP_DEFAULT_ZOOM      = 'store_locator/map_frontend/zoom';
    const FRONTEND_MAP_DEFAULT_LATITUDE  = 'store_locator/map_frontend/latitude';
    const FRONTEND_MAP_DEFAULT_LONGITUDE = 'store_locator/map_frontend/longitude';
    const FRONTEND_MAP_DEFAULT_ICON      = 'store_locator/map_frontend/icon';
    /**#@-*/

    /**
     * getDefaultMapZoom()
     *
     * Fetches the default map zoom value from the config for either
     * the FRONTEND or the BACKEND template.
     *
     * @param string $type
     * @return string
     */
    public function getDefaultMapZoom($type = 'FRONTEND')
    {
        return ($type === 'FRONTEND')
             ? Mage::getStoreConfig(self::FRONTEND_MAP_DEFAULT_ZOOM)
             : Mage::getStoreConfig(self::BACKEND_MAP_DEFAULT_ZOOM);
    }

    /**
     * getDefaultMapZoom()
     *
     * Fetches the default map latitude value from the config for either
     * the FRONTEND or the BACKEND template.
     *
     * @param  string $type
     * @return string
     */
    public function getDefaultMapLatitude($type = 'FRONTEND')
    {
        return ($type === 'FRONTEND')
             ? Mage::getStoreConfig(self::FRONTEND_MAP_DEFAULT_LATITUDE)
             : Mage::getStoreConfig(self::BACKEND_MAP_DEFAULT_LATITUDE);
    }

    /**
     * getDefaultMapZoom()
     *
     * Fetches the default map longitude value from the config for either
     * the FRONTEND or the BACKEND template.
     *
     * @param  string $type
     * @return string
     */
    public function getDefaultMapLongitude($type = 'FRONTEND')
    {
        return ($type === 'FRONTEND')
             ? Mage::getStoreConfig(self::FRONTEND_MAP_DEFAULT_LONGITUDE)
             : Mage::getStoreConfig(self::BACKEND_MAP_DEFAULT_LONGITUDE);
    }

    /**
     * getDefaultMapZoom()
     *
     * Fetches the default map marker value from the config for either
     * the FRONTEND or the BACKEND template.
     *
     * @param string $type
     * @return mixed
     */
    public function getDefaultMapMarker($type = 'FRONTEND')
    {
        return ($type === 'FRONTEND')
            ? Mage::getStoreConfig(self::FRONTEND_MAP_DEFAULT_ICON)
            : Mage::getStoreConfig(self::BACKEND_MAP_DEFAULT_ICON);
    }

    /**
     * getSlug()
     *
     * @param string $str
     * @param string $separator
     * @return string
     */
    public function getSlug($str, $separator = '-')
    {
        $str = trim($str);
        $str = Mage::helper('core')->removeAccents($str);
        $str = Mage::helper('catalog/product_url')->format($str);
        $slug = preg_replace('#[^0-9a-z]+#i', $separator, $str);
        $slug = strtolower($slug);
        $slug = trim($slug, $separator);

        return $slug;
    }
}
