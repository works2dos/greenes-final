<?php

/**
 * Greenes_Brands_Helper_Rewrite_Product
 *
 * @category   Studioforty9
 * @package    Studioforty9_Brands
 * @subpackage Helper
 */
class Greenes_Brands_Helper_Rewrite_Product extends Studioforty9_Brands_Helper_Product
{
    /**
     * Find products by Brand Id.
     *
     * @param int $brandId
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function findByBrandId($brandId)
    {
        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addPriceData()
            ->addTaxPercents()
            ->addUrlRewrite()
            ->addAttributeToFilter('brand_id', array('eq' => $brandId))
            ->addAttributeToFilter(
                'visibility', array(
                'in' => array(
                    Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                    Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
                )
            )
            )
            ->addStoreFilter()
            ->joinTable(
                'studioforty9_brands/brand',
                'entity_id=brand_id',
                array('brand_name' => 'name', 'brand_image' => 'logo_image'),
                '(studioforty9_brands.visibility=1 AND studioforty9_brands.entity_id=' . $brandId . ')',
                'left'
            );

        return $products;
    }


    /**
     *
     * @param string $url
     *
     * @return string|false
     */
    public function getBrandNameByUrlKey($url)
    {
        /** @var Studioforty9_Brands_Model_Resource_Brand_Collection $collection */
        $collection = Mage::getModel('studioforty9_brands/brand')
            ->getCollection()
            ->addFieldToFilter('url_key', $url)
            ->addFieldToSelect('name');

        if (!$collection->count()) {
            return false;
        }

        return $collection->getFirstItem()->getName();
    }
}
