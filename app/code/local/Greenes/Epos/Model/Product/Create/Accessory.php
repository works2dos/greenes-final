<?php

/**
 * Class Greenes_Epos_Model_Product_Create_Accessory
 *
 * @extends     Greenes_Epos_Model_Product_Create_Simple
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Create_Accessory extends Greenes_Epos_Model_Product_Create_Simple
{

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_product_accessory_";
    }

    /**
     * Updated Accessory Defaults
     *
     * @return array
     */
    public function getDefaultProductValues()
    {
        $data = parent::getDefaultProductValues();
        $data["visibility"] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
        return $data;
    }

    /**
     * @return Greenes_Epos_Helper_Accessories
     */
    public function getAttributeHelper()
    {
        return Mage::helper('greenes_epos/accessories');
    }
}
