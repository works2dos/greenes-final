<?php

/**
 * Class Greenes_Epos_Model_Csv_Import
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Csv_Import extends Greenes_Epos_Model_Base
{

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_csv_import_";
    }


    /**
     * @return bool
     */
    public function import()
    {
        ini_set("memory_limit", "512M");

        $file = $this->getFile();
        if (!$file) {
            $this->log("No file to import");
            return false;
        }

        if (!$this->parseCsv($file)) {
            $this->log("Could not parse CSV for $file");
            return false;
        }

        $columns = $this->getColumnHeaders();
        $data = $this->getCsvData();
        return $this->importCsv($columns, $data);
    }

    /**
     * @param array $columns
     * @param array $data
     *
     * @return bool
     */
    public function importCsv(array $columns, array $data)
    {
        $count = count($columns);
        $insert = array();

        /** @var Mage_Core_Helper_String $helper */
        $helper = Mage::helper('core/string');

        $skuField = $this->getAttributeHelper()->getColumnByCode("sku");
        foreach ($data as $i => $row) {

            $values = array();
            $failed = false;

            for ($a = 0; $a < $count; $a++) {
                if ($failed) {
                    continue;
                }
                $key = trim($columns[$a]);
                $value = trim($row[$a]);

                $validator = $this->getValidationModel($key);
                if ($validator) {
                    if (!$validator->validate($value)) {
                        $failed = true;
                        $this->log("Could not validate value '$value' for row $i and attribute $key");
                        continue;
                    }
                }
                $values[$key] = $value;
            }

            if ($failed) {
                $this->log("Could not import row $i. See errors");
                $this->log($row);
                continue;
            }


            $sku = $values[$skuField];
            $insert[$sku] = $values;
        }

        if (empty($insert)) {
            $this->log("No data to import");
            return false;
        }

        /** @var Magento_Db_Adapter_Pdo_Mysql $adapter */
        $table = Mage::getResourceModel('greenes_epos/import')->getMainTable();
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_write');

        try {
            $adapter->insertMultiple($table, $insert);
        } catch (Exception $e) {
            $this->log("Could not import all data");
            $this->log($e);
            return false;
        }

        return true;
    }


    /**
     * @return array
     */
    public function getFiles()
    {
        $path = Mage::helper('greenes_epos')->getImportPath();
        return glob($path . '*aggregate.csv');
    }


    /**
     * @return string|false
     */
    public function getFile()
    {
        $files = $this->getFiles();
        if (empty($files)) {
            return false;
        }
        return $files[0];
    }

    /**
     * @param string $file
     *
     * @return bool
     * @throws Exception
     */
    public function parseCsv($file)
    {
        $csv = new Varien_File_Csv();
        $data = $csv->getData($file);

        if (empty($data)) {
            return false;
        }

        if (!array_key_exists(1, $data)) {
            return false;
        }

        $this->setColumnHeaders($data[0]);
        array_shift($data);
        $this->setCsvData($data);

        return true;

    }

    /**
     * @return false|array
     */
    public function getColumnHeaders()
    {
        $data = $this->getData('column_headers');
        if (array($data)) {
            return $data;
        }

        return false;
    }

    /**
     * @return false|array
     */
    public function getCsvData()
    {
        $data = $this->getData('csv_data');
        if (array($data)) {
            return $data;
        }

        return false;
    }

    /**
     * @return object
     */
    public function getCollection()
    {
        return Mage::getModel('greenes_epos/import')->getCollection();
    }

    /**
     * @param string $column
     *
     * @return false|Greenes_Epos_Model_Validate_Abstract
     */
    public function getValidationModel($column)
    {
        $helper = $this->getImportHelper();
        $class = $helper->getValidationModel($column);

        if (!$class) {
            return false;
        }
        return Mage::getSingleton($class);
    }

}