<?php

/**
 * Class Greenes_Epos_Block_Adminhtml_Import
 *
 * @extends     Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Block_Adminhtml_Import extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    public function _prepareToRender()
    {
        $this->addColumn(
            'column', array(
                'label' => Mage::helper('greenes_epos')->__('Column'),
            )
        );

        $this->addColumn(
            'validation_model', array(
                'label' => Mage::helper('greenes_epos')->__('Validation Model')
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('greenes_epos')->__('Add');
    }
}