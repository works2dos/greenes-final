<?php

/**
 * Class Greenes_Theme_Helper_Data
 *
 * @extends     Mage_Core_Helper_Abstract
 * @category    Greenes
 * @package     Greenes_Theme
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Theme_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param string $attribute
     *
     * @return array
     */
    public function getAttributeOptions($attribute)
    {
        $options = array();
        $items = Mage::getModel('eav/entity_attribute_option')->getCollection()->setStoreFilter()
            ->join('attribute', 'attribute.attribute_id=main_table.attribute_id', 'attribute_code');

        foreach ($items as $item) {
            if ($item->getAttributeCode() == $attribute) {
                $key = $item->getOptionId();
                $value = $item->getValue();
                $options[$key] = $value;
            }
        }

        return $options;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param                            $categoryId
     *
     * @return bool
     */
    public function hasCategory(Mage_Catalog_Model_Product $product, $categoryId)
    {
        $categoryIds = $product->getCategoryIds();
        return in_array($categoryId, $categoryIds);
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function getAttributeSetId($name)
    {
        $collection = Mage::getModel('eav/entity_attribute_set')
            ->getCollection()
            ->addFieldToFilter("attribute_set_name", $name)
            ->addFieldToSelect("attribute_set_id");

        return $collection->getFirstItem()->getData("attribute_set_id");
    }


}