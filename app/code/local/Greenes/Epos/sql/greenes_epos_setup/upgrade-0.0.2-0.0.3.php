<?php
/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$conn = $this->getConnection();

$table = $this->getTable('greenes_epos/configurable');
$conn->addColumn(
    $table,
    'epos_id',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => false,
        'comment' => 'Epos Import Id'
    )
);
$this->endSetup();