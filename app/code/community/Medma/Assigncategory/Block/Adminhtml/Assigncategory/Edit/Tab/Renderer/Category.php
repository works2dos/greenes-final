<?php
class Medma_Assigncategory_Block_Adminhtml_Assigncategory_Edit_Tab_Renderer_Category extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $productCategories = array();
        $product = Mage::getModel('catalog/product')->load($row->getData('entity_id'));
        $categories = $product->getCategoryCollection()->addAttributeToSelect('name');
        foreach ($categories as $category) {
            array_push($productCategories, $category->getName());
        }
        return implode('<br>', $productCategories);
    }
}