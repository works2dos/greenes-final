<?php

/**
 * Class Greenes_Brands_Model_Source_Manufacturer
 *
 * @extends     Greenes_Brands_Model_Manufacturer
 * @category    Greenes
 * @package     Greenes_Brands
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Brands_Model_Source_Manufacturer extends Mage_Core_Model_Abstract
{
    public function toOptionArray()
    {
        /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attributes */
        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, "manufacturer");
        $options = $attribute->getSource()->getAllOptions(true, true);
        $data = array();

        foreach ($options as $option) {
            $key = trim($option['value']);
            $value = trim($option['label']);
            if ($key == '' || $value == '' || $value == 'Manufacturer' || $value == 'Morgan&CO') {
                continue;
            }
            $data[$key] = $value;
        }

        return $data;
    }
}