<?php

/**
 * Studioforty9_Stores_IndexController
 *
 * @category   Studioforty9
 * @package    Studioforty9_Stores
 * @author     Copyright (c) 2014 StudioForty9
 */
class Studioforty9_Stores_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * indexAction()
     */
    public function indexAction()
    {
        $this->_title($this->__('Store Locator'));
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');

        return $this->renderLayout();
    }

    /**
     * viewAction()
     *
     * @return Mage_Core_Controller_Varien_Action
     */
    public function viewAction()
    {
        if ($storeId = (int) $this->getRequest()->getParam('id')) {
            $store = Mage::getModel('pharmacy/pharmacy')->load($storeId);
            if (! $store->getId()) {
                return $this->norouteAction();
            }
        }

        $this->loadLayout();
        $this->_removeDefaultTitle = true;
        $this->_title($this->__('%s Stores %s | Studioforty9 Store Locations', $store->getName(), $store->getRegion()), true);
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('studioforty9.stores.list')->setData('store', $store);
        $this->getLayout()->getBlock('studioforty9.stores.view')->setData('store', $store);

        return $this->renderLayout();
    }
}
