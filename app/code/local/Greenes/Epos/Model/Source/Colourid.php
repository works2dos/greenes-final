<?php

/**
 * Colour Source Model
 *
 * Class Greenes_Epos_Model_Source_Colourid
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Colourid
    extends Greenes_Epos_Model_Source_Colour
{
    use Greenes_Epos_Model_Source_Csv_Trait;

    /**
     * @return false|string
     */
    public function getValue()
    {
        $colour = $this->getColourLabel();
        if (! $colour) {
            return false;
        }
        return $this->getOptionHelper()->getOptionId($colour, "colour");
    }


    /**
     * @return false|string
     */
    public function getColourLabel()
    {
        return parent::getValue();
    }

    /**
     * @return string
     */
    public function getFileConfigPath()
    {
        return parent::getFileConfigPath();
    }

}
