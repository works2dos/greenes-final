<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Validate_Sku
 */
class Greenes_Epos_Model_Validate_SkuSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Greenes_Epos_Model_Validate_Sku');
    }

    function it_should_validate_12_character_string()
    {
        $this->validate("000003002005")->shouldReturn(true);
    }

    function it_should_not_validate()
    {
        $this->validate("00000300205")->shouldReturn(false);
        $this->validate("")->shouldReturn(false);
    }

}
