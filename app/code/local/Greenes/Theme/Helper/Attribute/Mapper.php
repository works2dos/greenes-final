<?php

/**
 * Class Greenes_Theme_Helper_Attribute_Mapper
 *
 * @extends     Mage_Core_Helper_Abstract
 * @category    Greenes
 * @package     Greenes_Theme
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Theme_Helper_Attribute_Mapper extends Mage_Core_Helper_Abstract
{
    /**
     * @param string $size
     *
     * @return string|false
     */
    public function getKidsSize($size)
    {
        return Mage::getModel("greenes_epos/source_size")->getSizeByCategory($size, "KID");
    }

    /**
     * @param string $size
     *
     * @return string|false
     */
    public function getLadiesSize($size)
    {
        return Mage::getModel("greenes_epos/source_size")->getSizeByCategory($size, "LAD");
    }

    /**
     * @param string $size
     *
     * @return string|false
     */
    public function getMensSize($size)
    {
        return Mage::getModel("greenes_epos/source_size")->getSizeByCategory($size, "MEN");
    }


}