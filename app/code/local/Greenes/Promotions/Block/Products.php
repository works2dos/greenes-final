<?php

/**
 * Class Greenes_Promotions_Block_Products
 *
 * @method Mage_Catalog_Model_Resource_Product_Collection getProducts()
 */
class Greenes_Promotions_Block_Products extends Mage_Catalog_Block_Product_Abstract
{
    const COLLECTION_LIMIT = 4;

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     */
    public function _beforeToHtml()
    {
        if ($this->hasCategory()) {
            $_products = $this->getProductsCollection($this->getCategory());
            $this->setProducts($_products);
        }
    }

    /**
     * Query the DB for a collection of products based on the given category.
     *
     * @param Mage_Catalog_Model_Category $category
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductsCollection($category)
    {
        if (!$this->hasPageSize()) {
            $this->setPageSize(static::COLLECTION_LIMIT);
        }

        return Mage::getModel('catalog/product')->getCollection()
            ->addCategoryFilter($category)
            ->addStoreFilter()
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addAttributeToSelect("featured_product", 1)
            ->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite()
            ->setPageSize($this->getPageSize())
            ->addAttributeToSort('position', 'asc');
    }
}
