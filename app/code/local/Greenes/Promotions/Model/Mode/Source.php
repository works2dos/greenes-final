<?php
/**
 * O'Briens Wine
 *
 * The file can not be copied and/or distributed without the express
 * permission of O'Briens Wine and StudioForty9.
 *
 * @category   Obriens
 * @package    Obriens_Categorydisplaymode
 * @copyright  Copyright (c) 2015 StudioForty9 (http://www.studioforty9.com)
 */

/**
 * Mode Source Model
 *
 * @category    Obriens
 * @package     Obriens_Categorydisplaymode
 * @subpackage  Model
 */
class Greenes_Promotions_Model_Mode_Source extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /** @var int */
    const STATUS_HIDE= 0;

    /** @var int */
    const STATUS_CATEGORY = 1;

    /** @var int */
    const STATUS_PRODUCTS = 2;

    /**
     * Get all options for displaying products for an individual category.
     *
     * @return array
     */
    public function getAllOptions()
    {
        $_helper = Mage::helper('greenes_promotions');

        $options[] = array('value' => self::STATUS_HIDE, 'label' => $_helper->__('Hide'));
        $options[] = array('value' => self::STATUS_CATEGORY, 'label' => $_helper->__('Show Category'));
        $options[] = array('value' => self::STATUS_PRODUCTS, 'label' => $_helper->__('Show Category with Products'));

        return $options;
    }
}
