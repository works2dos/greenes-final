<?php

/**
 * Relates the configurable products by colour
 *
 * Class Greenes_Epos_Model_Product_Configurable_Relate
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Configurable_Relate extends Greenes_Epos_Model_Base
{

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_relate_products_";
    }

    /**
     * @param array $configurables
     *
     * @return bool
     */
    public function relate(array $configurables)
    {
        $configurables = $this->groupConfigurables($configurables);
        if (!$configurables) {
            return false;
        }

        foreach ($configurables as $products) {
            $this->_relateProducts($products);
        }

        return true;

    }

    /**
     * Groups Configurable Products by Colour
     *
     * @param array $configurables
     *
     * @return array|bool
     */
    public function groupConfigurables(array $configurables)
    {
        $relatedProducts = array();

        /** @var Mage_Catalog_Model_Product $configurable */
        foreach ($configurables as $configurable) {
            $key = $this->getConfigurableKey($configurable);
            if (!$key) {
                continue;
            }
            $relatedProducts[$key][] = $configurable;
        }

        if (empty($relatedProducts)) {
            return false;
        }

        return $relatedProducts;
    }


    /**
     * Gets configurable key by colour
     *
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return bool|mixed
     */
    public function getConfigurableKey(Mage_Catalog_Model_Product $product)
    {
        $sku = strtolower($product->getSku());
        if (!$sku) {
            return false;
        }

        $colour = strtolower($product->getAttributeText("colour"));
        if (!$colour) {
            return false;
        }
        return str_replace("-" . $colour, "", $sku);
    }


    /**
     * Relates Products
     *
     * @param array $products
     *
     * @return bool
     */
    protected function _relateProducts(array $products)
    {
        if (count($products) === 1) {
            return false;
        }

        $linkResource = Mage::getResourceModel('catalog/product_link');
        $relatedLinkData = array();
        $i = 1;

        /** @var Mage_Catalog_Model_Product $product */
        foreach ($products as $product) {
            $productId = $product->getId();
            $relatedLinkData[$productId] = array("position" => $i);
            $i++;
        }

        $error = false;
        foreach ($products as $id => $product) {
            $linkData = $relatedLinkData;
            unset($linkData[$id]);
            try {
                $linkResource->saveProductLinks(
                    $product, $linkData, Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED
                );
            } catch (Exception $e) {
                $this->logError("Related ID: $id");
                $this->logError($linkData);
                $this->logError($e->getMessage());
                $error = true;
            }
        }

        if ($error) {
            return false;
        }

        return true;

    }
}
