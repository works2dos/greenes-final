<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Csv_Aggregate
 */
class Greenes_Epos_Model_Csv_AggregateSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Csv_Aggregate');
    }

    function it_should_aggregate()
    {
        $this->getFile()->shouldBeString();
        $this->getSkuHeaderKey()->shouldBeInteger();
        $this->getStockHeaderKey()->shouldBeInteger();
        $this->getCsvHeaders()->shouldBeArray();
        $this->generateFile()->shouldBe(true);
    }
}
