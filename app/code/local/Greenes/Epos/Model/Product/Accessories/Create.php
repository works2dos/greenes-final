<?php

/**
 * Class Greenes_Epos_Model_Product_Accessories_Create
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Accessories_Create extends Greenes_Epos_Model_Base
{
    /**
     * Set are as admin and clear the config
     */
    protected function _construct()
    {
        /**
         * Set the current store as admin to create the products
         */
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        /**
         * Clear the config cache
         */
        Mage::app()->getCacheInstance()->cleanType('config');

        return parent::_construct();
    }

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_product_accesories_";
    }

    /**
     * Creates the products
     *
     * @return bool
     */
    public function createProducts()
    {
        ini_set("memory_limit", "1024M");

        /**
         * Get Products
         */
        $products = $this->getProducts();
        if (!$products) {
            $this->logWarning("No accessory products found for creating new products.");
            return true;
        }

        /**
         * Create Simple Products
         */
        $products = $this->_createProducts($products);
        if (!$products) {
            $this->logError("No accessory products products created.");
            return false;
        }

        return true;
    }


    /**
     * @return false|Greenes_Epos_Model_Resource_Import_Collection
     */
    public function getProducts()
    {
       $products = Mage::getSingleton('greenes_epos/import')
           ->getCollection()
           ->addFieldToSelect("*")
           ->addFieldToFilter("Group", array("in" => array("ACC")))
           ->setOrder("id", "ASC");

        if (! $products->count())
        {
            return false;
        }

        return $products;

    }


    /**
     * @param Greenes_Epos_Model_Resource_Import_Collection $collection
     *
     * @return bool
     */
    protected function _createProducts(Greenes_Epos_Model_Resource_Import_Collection $collection)
    {
        $attributes = $this->getAttributes();
        if (!$attributes) {
            $this->logError("Attributes need to set for simple products in configuration");
            return false;
        }

        /** @var Greenes_Epos_Model_Import $row */
        foreach ($collection as $row)
        {
            $model = Mage::getModel('greenes_epos/product_create_accessory');
            if (!$model->create($attributes, $row->getData())) {
                $this->logWarning("Could not create accessory product.");
                continue;
            }

            $rowId = $row->getId();
            try {
                $this->deleteItems(array($rowId));
            } catch (Exception $e) {
                $this->logWarning("Could not delete row $rowId");
                $this->logWarning($e->getMessage());
            }

        }
    }

    /**
     * @return Greenes_Epos_Helper_Accessories
     */
    public function getAttributeHelper()
    {
        return Mage::helper('greenes_epos/accessories');
    }

    /**
     * @return array|bool
     */
    public function getAttributes()
    {
        $attributes = $this->getAttributeHelper()->getAttributes();
        if (!$attributes) {
            return false;
        }
        return array_keys($attributes);
    }
}
