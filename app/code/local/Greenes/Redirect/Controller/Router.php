<?php

/**
 * Custom Router for Redirect of old products
 *
 * Redirects old products to the ladies category
 *
 * All old product redirects have a the following format
 * (number)-(string) or (string)/(number)-(string)
 *
 * @extends     Mage_Core_Controller_Varien_Router_Abstract
 * @category    Greenes
 * @package     Greenes_Redirect
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Redirect_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{

    /**
     * Adds router for redirect
     *
     * @param $observer
     */
    public function controllerFrontInitRouters($observer)
    {
        /* @var $front Mage_Core_Controller_Varien_Front */
        $front = $observer->getEvent()->getFront();
        $front->addRouter('greenes_redirect', $this);
    }


    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        /**
         * Get the first part of the URL
         */
        $path = trim($request->getPathInfo(), '/');
        $redirect = $this->getCategoryRedirect($path);
        if (!$redirect) {
            $redirect = $this->getBrandRedirect($path);
        }

        if (!$redirect) {
            return false;
        }


        /**
         * Redirect
         */
        Mage::app()->getFrontController()->getResponse()
            ->setRedirect($redirect, 301)
            ->sendResponse();
        exit;
    }

    /**
     * @param $path
     *
     * @return bool
     */
    public function getCategoryRedirect($path)
    {
        /**
         * Remove any subcategory from the URL
         *
         * e.g. boots/12227-ladies
         * e.g. boots/12227-mens
         * e.g. boots/12227-kids
         *
         */
        $url = $path;
        $pos = strpos($url, "/");
        if ($pos !== false) {
            $url = substr($url, $pos + 1);
        }

        $urls = explode("-", $url);
        if (count($urls) < 2) {
            return false;
        }
        $url = $urls[0];

        /**
         * Check if numeric
         */
        if (!is_numeric($url)) {
            return false;
        }

        /**
         * Determine which category page to redirect to
         */
        $category = "Ladies";
        if (strpos($path, "kids") !== false) {
            $category = "Kids";
        } elseif (strpos($path, "mens") !== false) {
            $category = "Mens";
        }

        return $this->_getCategoryRedirectUrl($category);
    }

    /**
     * @return string
     *
     * @throws Mage_Core_Exception
     */
    protected function _getCategoryRedirectUrl($category = "Ladies")
    {
        /** @var Mage_Catalog_Model_Category $category */
        $category = Mage::getSingleton("catalog/category")->loadByAttribute("name", $category);
        $url = $category->getUrl();
        if (!$url) {
            $url = Mage::app()->getStore()->getBaseUrl();
        }

        return $url;
    }


    /**
     * @param $path
     *
     * @return bool|string
     */
    public function getBrandRedirect($path)
    {
        /**
         * E.g. skechers-slip-on-shoe-ddssd-grey.html
         * goor-975-brown.html
         *
         */
        $url = $path;
        $pos = strpos($url, "/");
        if ($pos !== false) {
            $url = substr($url, $pos + 1);
        }

        $urls = explode("-", $url);
        if (count($urls) < 2) {
            return false;
        }
        return $this->_getBrandRedirectUrl($urls[0]);
    }

    /**
     * @param $url_key
     *
     * @return bool|string
     */
    protected function _getBrandRedirectUrl($url_key)
    {
        $brand = Mage::getModel("studioforty9_brands/brand")
            ->load($url_key, "url_key");

        if ($brand->getData()) {
            return Mage::getUrl("brands/" . $brand->getUrlKey());
        }

        return false;
    }
}