<?php

/**
 * Class Greenes_Epos_Model_Rewrite_Configurable
 *
 * @extends     Mage_Catalog_Model_Product_Type_Configurable
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Rewrite_Configurable extends Mage_Catalog_Model_Product_Type_Configurable
{
    /**
     * Retrieve used product by attribute values
     *  $attrbutesInfo = array(
     *      $attributeId => $attributeValue
     *  )
     *
     * @param  array $attributesInfo
     * @param  Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Product|null
     */
    public function getProductByAttributes($attributesInfo, $product = null)
    {
        if (is_array($attributesInfo) && !empty($attributesInfo)) {
            /** @var Mage_Catalog_Model_Resource_Product_Type_Configurable_Product_Collection $productCollection */
            $productCollection = $this->getUsedProductCollection($product)->addAttributeToSelect('name');
            foreach ($attributesInfo as $attributeId => $attributeValue) {
                $productCollection->addAttributeToFilter($attributeId, $attributeValue);
            }

            /**
             * Order product collection by stock level
             *
             * Order by inventory
             */
            $productCollection->getSelect()->joinLeft(
                array('inventory_table'=>'cataloginventory_stock_item'),
                "inventory_table.product_id = e.entity_id ",
                array('qty')
            )->order(array('inventory_table.qty DESC'));


            $productObject = $productCollection->getFirstItem();
            if ($productObject->getId()) {
                return $productObject;
            }

            foreach ($this->getUsedProducts(null, $product) as $productObject) {
                $checkRes = true;
                foreach ($attributesInfo as $attributeId => $attributeValue) {
                    $code = $this->getAttributeById($attributeId, $product)->getAttributeCode();
                    if ($productObject->getData($code) != $attributeValue) {
                        $checkRes = false;
                    }
                }
                if ($checkRes) {
                    return $productObject;
                }
            }
        }
        return null;
    }
}