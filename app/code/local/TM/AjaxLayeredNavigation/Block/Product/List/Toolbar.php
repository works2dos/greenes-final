<?php

class TM_AjaxLayeredNavigation_Block_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{
    /**
     * Set collection to pager
     *
     * @param Varien_Data_Collection $collection
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setCollection($collection)
    {
        parent::setCollection($collection);

        /**
         * Sort By Type - Ladies, Mens, Kids, Unisex
         */
        $handles = Mage::app()->getLayout()->getUpdate()->getHandles();
        if (in_array("studioforty9_brands_index_list", $handles)) {
            $this->_collection->setOrder('attribute_set_id', $this->getCurrentDirection());
        }


        if ($this->getCurrentOrder()) {
            if (($this->getCurrentOrder()) == 'position') {
                $this->_collection->setOrder('entity_id', $this->getCurrentDirection());
            } else {
                $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
            }
        }

        return $this;
    }

}
