<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Sku
 */
class Greenes_Epos_Model_Source_SkuSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Sku');
    }

    function it_should_be_valid()
    {
        $colourColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("colour");
        $skuColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("sku");
        $data = array(
            $skuColumn => "011692",
            $colourColumn => "BLA"
        );

        $this->setRowData($data, $skuColumn);
        $this->getValue()->shouldBe("011692-black");

        $data = array(
            $skuColumn => "011692",
        );
        $this->setRowData($data, $skuColumn);
        $this->getValue()->shouldBe("011692");
    }

}
