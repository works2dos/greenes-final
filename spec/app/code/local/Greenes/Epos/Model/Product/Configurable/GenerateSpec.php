<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Product_Configurable_Generate
 */
class Greenes_Epos_Model_Product_Configurable_GenerateSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Product_Configurable_Generate');
    }

    function it_should_be_valid()
    {
        $products = array();
        $row = \Mage::getModel('greenes_epos/import')->getCollection()->getFirstItem();
        $id = $row->getId();
        $products[$id] = $row->getData();

        $sku = "configurable";
        $this->getProductData($sku, $products)->shouldBeArray();
    }
}
