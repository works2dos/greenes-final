<?php
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();

$attributeSetId = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getDefaultAttributeSetId();

$installer->removeAttribute("catalog_category", "is_promoted_on_homepage");

$installer->addAttribute('catalog_category', 'is_promoted_on_homepage', array(
    'type'         => 'int',
    'label'        => 'Promote on the Homepage',
    'input'        => 'select',
    'default'      => 0,
    'source'       => 'greenes_promotions/mode_source',
    'sort_order'   => 18,
    'group'        => 'Promotions',
    'required'     => true,
    'visible'      => true,
    'global'       => true,
    'user_defined' => true,
    'filterable'   => false,
    'filterable_in_search' => false,
));


$installer->endSetup();
