<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;


/**
 * @mixin \Greenes_Epos_Model_Stock_Update
 */
class Greenes_Epos_Model_Stock_UpdateSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Stock_Update');
    }

    function it_should_have_valid_credentials()
    {
        $this->hasValidCredentials()->shouldReturn(true);
    }

    function it_should_have_valid_stock_status()
    {
        $this->isInStock(0)->shouldReturn(false);
        $this->isInStock("")->shouldReturn(false);
        $this->isInStock("a")->shouldReturn(false);
        $this->isInStock(1)->shouldReturn(true);
        $this->isInStock(2)->shouldReturn(true);
    }

    function it_should_update_stock()
    {
        $this->update()->shouldReturn(true);
    }

}
