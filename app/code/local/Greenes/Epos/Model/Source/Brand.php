<?php

/**
 * Brand Source Model
 *
 * Class Greenes_Epos_Model_Source_Brand
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Brand
    extends Greenes_Epos_Model_Source_Abstract
{
    use Greenes_Epos_Model_Source_Csv_Trait;

    /**
     * @return bool|int
     */
    public function getValue()
    {
        $value = $this->getColumnValue();
        if (! $value) {
            return false;
        }

        $value = $this->getCsvValue($value);
        if (!$value) {
            return false;
        }

        return $this->getBrandId($value);
    }

    /**
     * @return string
     */
    public function getFileConfigPath()
    {
        return "greenes_epos/simple/brand_csv";
    }

    /**
     * @param $value
     *
     * @return bool|int
     */
    public function getBrandId($value)
    {
        $value = strtolower($value);
        $options = Mage::getSingleton("studioforty9_brands/source_brand")->getAllOptions();
        foreach ($options as $row) {
            $label = strtolower($row['label']);
            if ($label === $value) {
                return (int)$row['value'];
            }
        }

        return false;
    }

}