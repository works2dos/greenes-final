<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

/**
 * Remove UK and EU size from the size attribute and replaces the text with ()
 * e.g. UK 10 EU 40 would be 10 (40)
 */

/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = Mage::getModel("core/resource")->getConnection("core_write");
$query = "SELECT value_id, value FROM eav_attribute_option_value WHERE value LIKE '%UK %'";
$rows = $connection->fetchAll($query);

foreach ($rows as $id => $row) {
    $value = $row['value'];
    $value = str_replace(" (EU ", " (", $value);
    $value = ltrim($value, "UK ");
    $valueId = $row['value_id'];

    $query = "UPDATE eav_attribute_option_value
    SET value = '$value'
    WHERE value_id = $valueId;";

    try {
        $connection->query($query);
    } catch (Exception $e) {
        Mage::log($valueId, null, 'attribute_error.log', true);
        Mage::log($e->getMessage(), null, 'attribute_error.log', true);
    }

}


$this->endSetup();