<?php
/** @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();

/** @var Mage_Core_Model_Config $config */
$config = Mage::getConfig();
$base = rtrim(Mage::getBaseUrl(), DS);

$config->saveConfig("greenes_general/callouts/callout_1", "100% Irish Company", 'default', 0);
$config->saveConfig("greenes_general/callouts/callout_2", "Free Worldwide Shipping", 'default', 0);
$config->saveConfig("greenes_general/callouts/callout_3", "Click and Collect", 'default', 0);
$config->saveConfig("greenes_general/callouts/callout_1_url", "$base/about-us.html", 'default', 0);
$config->saveConfig("greenes_general/callouts/callout_2_url", "$base/delivery.html", 'default', 0);
$config->saveConfig("greenes_general/callouts/callout_3_url", "$base/delivery.html", 'default', 0);
$config->saveConfig("greenes_general/callouts/callout_1_icon", "clubs", 'default', 0);
$config->saveConfig("greenes_general/callouts/callout_2_icon", "truck", 'default', 0);
$config->saveConfig("greenes_general/callouts/callout_3_icon", "mouse", 'default', 0);

$this->endSetup();