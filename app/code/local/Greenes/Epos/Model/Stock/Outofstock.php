<?php

/**
 * Set items not in the epos_import table out of stock
 *
 * Class Greenes_Epos_Model_Stock_Outofstock
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Stock_Outofstock extends Greenes_Epos_Model_Base
{

    /**
     * Set products out of stock
     *
     * @return bool
     */
    public function update()
    {
        $ids = $this->getProductIdsNotInEpos();
        if (! $ids) {
            return false;
        }

        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
        $connection = $resource->getConnection("core_write");
        $ids = implode(",", $ids);

        $itemTable = $resource->getTableName("cataloginventory/stock_item");
        $statusTable = $resource->getTableName("cataloginventory/stock_status");

        $query = "UPDATE $statusTable
        SET qty = 0,stock_status = 0
        WHERE product_id IN ($ids)";

        try {
            $connection->query($query);
        } catch (Exception $e) {
            $this->logError("Could not set stock out of stock");
            $this->logError($e->getMessage());
            return false;
        }

        $query = "UPDATE $itemTable
        SET qty = 0,is_in_stock = 0
        WHERE product_id IN ($ids)";

        try {
            $connection->query($query);
        } catch (Exception $e) {
            $this->logError("Could not set stock out of stock");
            $this->logError($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Gets enabled product ids which are not in the epos
     *
     * @return array|bool
     */
    public function getProductIdsNotInEpos()
    {
        $skus = $this->getEposSkus();
        if (! $skus) {
            $this->logWarning("No skus in the epos table to set out of stock");
            return false;
        }

        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
        $connection = $resource->getConnection("core_write");
        $skus = implode(",", $skus);
        $catalogTable = $resource->getTableName("catalog/product");
        $catalogIntTable = $catalogTable . "_int";
        $statusId = 96;
        $enabled = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;

        $query
            = "
        SELECT c.entity_id
        FROM $catalogTable as c
        LEFT JOIN $catalogIntTable as i ON (
        i.attribute_id = $statusId
        AND i.entity_id = c.entity_id
        AND i.value = $enabled
        )
        WHERE c.type_id = 'simple'
        AND i.value = $enabled
        AND c.sku NOT IN($skus)
        ";

        try {
            $simple = $connection->fetchCol($query);
            return $simple;
        } catch (Exception $e) {
            $this->logError($e->getMessage());
            return false;
        }

    }

    /**
     * @return array|bool
     */
    public function getEposSkus()
    {
        $resource = Mage::getSingleton('core/resource');
        /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
        $connection = $resource->getConnection("core_write");

        $table = $resource->getTableName("greenes_epos/import");
        $sku = $this->getSkuField();
        $query = "SELECT $sku FROM $table";
        try {
            $epos = $connection->fetchCol($query);
        } catch (Exception $e) {
            $this->logError($e->getMessage());
            return false;
        }

        if (empty($epos)) {
            return false;
        }

        return $epos;
    }

}