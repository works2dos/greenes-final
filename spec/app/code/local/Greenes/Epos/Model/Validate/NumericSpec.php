<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class Greenes_Epos_Model_Validate_NumericSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Greenes_Epos_Model_Validate_Numeric');
    }


    function it_should_validate()
    {
        $this->validate("123")->shouldReturn(true);
        $this->validate("0")->shouldReturn(true);
        $this->validate("99")->shouldReturn(true);
        $this->validate("299")->shouldReturn(true);
        $this->validate("0")->shouldReturn(true);
    }

    function it_should_not_validate()
    {
        $this->validate("")->shouldReturn(false);
        $this->validate("test")->shouldReturn(false);
    }

}
