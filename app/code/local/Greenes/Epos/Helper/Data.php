<?php

/**
 * Class Greenes_Epos_Helper_Data
 *
 * @extends     Greenes_Epos_Helper_Data
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param        $message
     * @param string $filename
     */
    public function log($message, $filename = 'epos_error.log')
    {
        if (Mage::getStoreConfig('greenes_epos/general/debug')) {
            Mage::log($message, null, $filename, true);
        }
    }

    /**
     * @return string
     */
    public function getEposPath()
    {
        return trim(Mage::getBaseDir('base')) . DS . 'epos' . DS;
    }

    /**
     * @return string
     */
    public function getImportPath()
    {
        return $this->getEposPath() . 'import' . DS;
    }

    /**
     * @return string
     */
    public function getMediaPath()
    {
        return $this->getEposPath() . 'media' . DS;
    }
}