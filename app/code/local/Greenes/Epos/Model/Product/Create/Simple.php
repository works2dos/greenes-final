<?php

/**
 * Class Greenes_Epos_Model_Product_Create_Simple
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Create_Simple extends Greenes_Epos_Model_Product_Create_Abstract
{

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_product_simple_";
    }

    /**
     * Creates the product
     *
     * @param array $attributes
     * @param array $data
     *
     * @return bool
     */
    public function create(array $attributes, array $data)
    {
        if ($this->doesProductExist($data)) {
            $this->logWarning("Product already exists");
            return false;
        }

        if (!$this->_setProductAttributes($attributes, $data)) {
            return false;
        }
        return $this->_createProduct();
    }

    /**
     * Checks if product exists already
     *
     * @param array $data
     *
     * @return bool
     */
    public function doesProductExist(array $data)
    {
        $column = $this->getAttributeHelper()->getColumnByCode("sku");
        if (!$column) {
            return false;
        }
        if (!array_key_exists($column, $data)) {
            return false;
        }
        $sku = $data[$column];

        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        if (!$product) {
            return false;
        }
        $this->logWarning("Product already exists for $sku");
        $this->setProduct($product);
        return true;
    }

    /**
     * Sets the default values and products attributes
     *
     * @param array $attributes
     * @param array $data
     *
     * @return bool
     */
    protected function _setProductAttributes(array $attributes, array $data)
    {
        foreach ($this->getDefaultProductValues() as $key => $value) {
            $this->setProductData($key, $value);
        }

        foreach ($attributes as $key => $attribute) {
            if (!$this->setProductAttribute($attribute, $data)) {
                return false;
            }
        }

        return true;
    }

}
