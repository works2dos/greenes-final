<?php

/**
 * Updates associated products
 *
 * Class Greenes_Epos_Model_Product_Configurable_Update
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Configurable_Update extends Greenes_Epos_Model_Base
{
    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_configurable_";
    }

    /**
     * Updates associated products
     *
     * @param array $data
     *
     * @return bool
     */
    public function update(Mage_Catalog_Model_Product $configurable, array $associatedProducts)
    {
        /** @var Mage_Catalog_Model_Product_Type_Configurable $type */
        $type = $configurable->getTypeInstance();
        if (! is_a($type, "Mage_Catalog_Model_Product_Type_Configurable")) {
            $this->logError("Could not update configurables as the product is not a configurable");
            return false;
        }

        $simpleProductIds = $this->getAssociatedIds($type, $configurable->getId());
        /** @var Mage_Catalog_Model_Product $simple */
        foreach ($associatedProducts as $simple) {
            $simpleProductIds[] = (string)$simple;
        }
        $simpleProductIds = array_unique($simpleProductIds);

        try {
            Mage::getResourceSingleton('catalog/product_type_configurable')
                ->saveProducts($configurable, $simpleProductIds);
            return true;
        } catch (Exception $e) {
            $sku = $configurable->getSku();
            $this->logError("Could not update associated products for $sku");
            $this->logError($simpleProductIds);
            $this->logError($e->getMessage());
            return false;
        }
    }


    /**
     * @param Mage_Catalog_Model_Product_Type_Configurable $type
     * @param                                              $configurableId
     *
     * @return array
     */
    public function getAssociatedIds(Mage_Catalog_Model_Product_Type_Configurable $type, $configurableId)
    {
        $ids = array();
        $childProducts = $type->getChildrenIds($configurableId);
        if ($childProducts) {
            foreach ($childProducts as $id => $productIds) {
                $ids = array_merge($ids, $productIds);
            }
        }

        return $ids;
    }

}