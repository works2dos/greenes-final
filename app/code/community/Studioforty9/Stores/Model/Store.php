<?php
/**
 * Studioforty9 Stores Model
 *
 * @category   Studioforty9
 * @package    Studioforty9_Stores
 * @author     Copyright (c) 2016 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Model_Store extends Mage_Core_Model_Abstract
{
    /**
     * @var Studioforty9_Stores_Helper_Data $_helper
     */
    protected $_helper;

    /**
     * _construct()
     *
     * @access protected
     */
    protected function _construct()
    {
        $this->_init('studioforty9_stores/store');
        $this->_helper = Mage::helper('studioforty9_stores');
    }

    protected function _afterLoad()
    {
        $this->_decodeHours();

        return parent::_afterLoad();
    }

    /**
     * _beforeSave()
     *
     * @return $this
     * @access protected
     */
    protected function _beforeSave()
    {
        $this->_encodeHours();
        parent::_beforeSave();
        $this->_updateTimestamps();

        return $this;
    }

    /**
     * _afterSave()
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        $requestPath = $this->getRequestPath();
        $conflict = Mage::getModel('core/url_rewrite')->getCollection()
            ->addStoreFilter(1) // TODO: When we support multiple store views, revisit this
            ->addFieldToFilter('request_path', $requestPath)
            ->setPageSize(1)
            ->getFirstItem();

        if ($conflict->getId() === 0) {
            $rewrite = Mage::getModel('core/url_rewrite');
            $rewrite->setStoreId(1)
                ->setIdPath('store-locator/' . $this->getId())
                ->setRequestPath($requestPath)
                ->setTargetPath('store-locator/view/id/' . $this->getId())
                ->setIsSystem(1)
                ->save();
        }

        return parent::_afterSave();
    }

    /**
     * _getRequestPath()
     *
     * @return string
     */
    public function getRequestPath()
    {
        return 'store-locator/' . $this->_helper->getSlug($this->getName());
    }

    /**
     * getCountryName()
     *
     * @return string
     */
    public function getCountryName()
    {
        try {
            $country = Mage::getModel('directory/country')->loadByCode($this->getData('country'))->getName();
        }
        catch(Exception $e) {
            $country = '';
        }

        return $country;
    }

    /**
     * getUrlKey()
     *
     * @return string
     */
    public function getUrlKey()
    {
        return Mage::getBaseUrl() . $this->getRequestPath();
    }

    /**
     * getAddress()
     *
     * @param string $separator
     * @param bool   $wrapHtml
     * @return string
     */
    public function getAddress($separator = ', ', $wrapHtml = false)
    {
        $address = array();

        $address[] = $this->_getStreetHtml($wrapHtml);
        $address[] = $this->_getCityHtml($wrapHtml);
        $address[] = $this->_getRegionHtml($wrapHtml);
        $address[] = $this->_getCountryHtml($wrapHtml);

        return implode($separator, $address);
    }

    /**
     * wrapMicroData()
     *
     * @param string $prop
     * @param string $value
     * @param string $element
     * @return string
     */
    public function wrapMicroData($prop, $value, $element = 'span')
    {
        return sprintf('<%s itemprop="%s">%s</%s>', $element, $prop, $value, $element);
    }

    /**
     * getStreetHtml()
     *
     * @param bool $wrapHtml
     * @param string $default
     * @return string
     */
    protected function _getStreetHtml($wrapHtml, $default = '')
    {
        if ($this->hasData('street')) {
            $street = $this->getData('street');
            return $wrapHtml ? $this->wrapMicroData('addressStreet', $street) : $street;
        }

        return $default;
    }

    /**
     * _getCityHtml()
     *
     * @param bool $wrapHtml
     * @param string $default
     * @return string
     */
    protected function _getCityHtml($wrapHtml, $default = '')
    {
        if ($this->hasData('city')) {
            $city = $this->getData('city');
            return $wrapHtml ? $this->wrapMicroData('addressLocality', $city) : $city;
        }

        return $default;
    }

    /**
     * _getRegionHtml()
     *
     * @param bool $wrapHtml
     * @param string $default
     * @return string
     */
    protected function _getRegionHtml($wrapHtml, $default = '')
    {
        if ($this->hasData('region')) {
            $region = $this->getData('region');
            return $wrapHtml ? $this->wrapMicroData('addressRegion', $region) : $region;
        }

        return $default;
    }

    /**
     * _getCountryHtml()
     *
     * @param bool $wrapHtml
     * @param string $default
     * @return string
     */
    protected function _getCountryHtml($wrapHtml, $default = '')
    {
        if ($this->hasData('country')) {
            $country = $this->getCountryName();
            return $wrapHtml ? $this->wrapMicroData('addressCountry', $country) : $country;
        }

        return $default;
    }

    /**
     * _updateTimestamps()
     *
     * @return $this
     * @access protected
     */
    protected function _updateTimestamps()
    {
        $timestamp = now();
        $this->setUpdatedAt($timestamp);

        if ($this->isObjectNew()) {
            $this->setCreatedAt($timestamp);
        }

        return $this;
    }

    /**
     * Update the hours passed through in raw array format to a JSON string.
     *
     * @return $this
     */
    protected function _encodeHours()
    {
        $hours = $this->getHours();
        if (is_array($hours)) {
            $json = array();
            foreach ($hours as $day => $times) {
                $json[$day] = array(
                    'key' => $day,
                    'day' => jddayofweek($day, 1),
                    'times' => $times
                );
            }
            /** @var Mage_Core_Helper_Data $core */
            $core = Mage::helper('core');
            $this->setHours($core->encrypt($core->jsonEncode(($json))));
        }

        return $this;
    }

    protected function _decodeHours()
    {
        $hours = $this->getHours();
        if (!empty($hours)) {
            /** @var Mage_Core_Helper_Data $core */
            $core = Mage::helper('core');
            try {
                $hours = $core->jsonDecode($core->decrypt(($hours)));
            } catch (Exception $e) {
                $hours = '';
            }
            $this->setHours($hours);
        }
    }
}
