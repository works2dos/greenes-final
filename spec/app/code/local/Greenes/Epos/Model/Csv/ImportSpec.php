<?php
namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Csv_Import
 */
class Greenes_Epos_Model_Csv_ImportSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Csv_Import');
    }

    function it_should_have_file()
    {
        $this->getFile()->shouldBeString();
    }

    function it_should_have_column_headers()
    {
        $file = $this->getFile();
        $this->parseCsv($file)->shouldReturn(true);
        $this->getColumnHeaders()->shouldBeArray();
    }

    function it_should_have_csv_data()
    {
        $file = $this->getFile();
        $this->parseCsv($file)->shouldReturn(true);
        $this->getCsvData()->shouldBeArray();
    }

    function it_import_should_have_rows()
    {
        $this->import()->shouldReturn(true);
        $this->getCollection()->count()->shouldNotReturn(0);
    }
}
