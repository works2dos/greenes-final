<?php

/**
 * Attribute Helper
 *
 * Class Greenes_Epos_Helper_Attributes
 *
 * @extends     Mage_Core_Helper_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Helper_Attributes extends Mage_Core_Helper_Abstract
{
    /**
     * @return mixed
     */
    public function getConfigValues()
    {
        return Mage::getStoreConfig("greenes_epos/simple/attributes");
    }


    /**
     * Gets the CSV fields
     *
     * @return false|array
     */
    public function getAttributes()
    {
        $config = $this->getConfigValues();
        if (!$config) {
            return false;
        }

        $config = unserialize($config);
        if (!is_array($config)) {
            return false;
        }

        $fields = array();
        foreach ($config as $row) {
            $key = $row['attribute'];
            $fields[$key] = $row;
        }

        return $fields;
    }

    /**
     * @param string $code
     *
     * @return false|array
     */
    public function getAttributeByCode($code)
    {
        $attributes = $this->getAttributes();
        if (!$attributes) {
            return false;
        }

        return array_key_exists($code, $attributes)
            ? $attributes[$code]
            : false;
    }

    /**
     * @param string $code
     * @param string $key
     *
     * @return false|array
     */
    public function getAttributeKeyByCode($code, $key)
    {
        $attribute = $this->getAttributeByCode($code);
        if (!$attribute) {
            return false;
        }

        return array_key_exists($key, $attribute)
            ? $attribute[$key]
            : false;
    }

    /**
     * @param string $code
     *
     * @return false|string
     */
    public function getColumnByCode($code)
    {
        return $this->getAttributeKeyByCode($code, 'column');
    }

    /**
     * @param string $code
     *
     * @return false|string
     */
    public function getSourceModelByCode($code)
    {
        return $this->getAttributeKeyByCode($code, 'source_model');
    }

    /**
     * @param string $code
     *
     * @return false|string
     */
    public function getRequiredByCode($code)
    {
        return $this->getAttributeKeyByCode($code, 'required');
    }

    /**
     * @param $code
     *
     * @return bool
     */
    public function isRequiredAttribute($code)
    {
        $value = $this->getRequiredByCode($code);
        if (!$value) {
            return false;
        }

        return (strtolower($value) === 'yes')
            ? true
            : false;
    }
}