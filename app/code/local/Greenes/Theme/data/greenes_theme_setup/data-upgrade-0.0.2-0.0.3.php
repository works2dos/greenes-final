<?php

$stores = array(
    array(
        "manager"    => "John Smith",
        "photo"      => "",
        "name"       => "Greens Shoes, Letterkenny",
        "telephone"  => "074 91 26963",
        "email"      => "john.smith@greenesshoes.com",
        "street"     => "Unit 30 Tesco Shopping Centre",
        "city"       => "Donegal",
        "post_code"  => "",
        "region"     => "Letterkenny",
        "country"    => "IE",
        "latitude"   => 54.95378459,
        "longitude"  => -7.72447523,
        "content"    => "",
        "hours"      => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF19OUKxyGkywpY2/mgkiKXYbxBX1ovtYjVguRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRrptUC6g2MKvSZ8rFXjXxHop6F4UpNB6yyEhgk8X6LtP5wkt90F4wNFhUTJwpqSpDKvhTJZdG1j3yoHZMao/Rm+mXzm0kbd/Onx38fXbuaWafmdnVcpnyWgUvxOpweoXamBeKIPUdiRZrFLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqKpdylr6VkbKsw61RDG2D3Rw4+4trZbtvayNYLMQIE/jZ/TIx/AI1ybxg2Quw3qA7oxe9oovqKqLT5Ke++YI9FdI1noCJiez9IPbHojjGRiKGYsTZJCwoTyQUufAtZXzi7x/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzp8d/H127mlmn5nZ1XKZ8loFL8TqcHqF2pgXiiD1HYkWaxS8+qBhGoHofvphoQVWbH6nOOwTd/Qb5pFQ5hTDiIdGBnKazZy0XXJXFqVpxTJ4/V8NbycH4T9T3dmrC0sXsO",
        "created_at" => "2016-03-04 17:26:36",
        "updated_at" => "2016-03-04 22:25:20"
    ),
    array(
        "manager"    => "John Smith",
        "photo"      => "",
        "name"       => "Greens Shoes, Galway",
        "telephone"  => "091 56 8616",
        "email"      => "john.smith@greenesshoes.com",
        "street"     => "Unit 218 Level 1, Eyre Square Centre",
        "city"       => "Galway",
        "post_code"  => "",
        "region"     => "Eyre Square",
        "country"    => "IE",
        "latitude"   => 53.2731324,
        "longitude"  => -9.0501475,
        "content"    => "",
        "hours"      => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF19OUKxyGkywpY2/mgkiKXYbxBX1ovtYjVguRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRrptUC6g2MKvSZ8rFXjXxHop6F4UpNB6yyEhgk8X6LtP5wkt90F4wNFhUTJwpqSpDKvhTJZdG1j3yoHZMao/Rm+mXzm0kbd/Onx38fXbuaWafmdnVcpnyWgUvxOpweoXamBeKIPUdiRZrFLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqKpdylr6VkbKsw61RDG2D3Rw4+4trZbtvayNYLMQIE/jZ/TIx/AI1ybxg2Quw3qA7oxe9oovqKqLT5Ke++YI9FdI1noCJiez9IPbHojjGRiKGYsTZJCwoTyQUufAtZXzi7x/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzpGDy6fBT2ncn5nZ1XKZ8loFL8TqcHqF2podz28XXvTJmxS8+qBhGoHofvphoQVWbH6nOOwTd/Qb5pFQ5hTDiIdGBnKazZy0XXlFop6OaQYQVjb+aCSIpdhjIG35zE0QwRX82oDklg2Y8=",
        "created_at" => "2016-03-04 18:04:57",
        "updated_at" => "2016-03-04 22:27:08"
    ),
    array(
        "manager"    => "John Smith",
        "photo"      => "",
        "name"       => "Gosh Shoes, Galway",
        "telephone"  => "091 53 5924",
        "email"      => "john.smith@greenesshoes.com",
        "street"     => "Unit 208A Level 0, Eyre Square Centre ",
        "city"       => "Galway",
        "post_code"  => "",
        "region"     => "Eyre Square",
        "country"    => "IE",
        "latitude"   => 53.2731324,
        "longitude"  => -9.0501475,
        "content"    => "",
        "hours"      => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF13/bIlyDKIV9Y2/mgkiKXYYyBt+cxNEMEQuRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRqY1hZ3MK6lu6pcha8TJNeqSVlUC1D/XlqKsAsgJtrS7XP61eEOSSJduLbbmUvxbzZpFQ5hTDiIdGBnKazZy0XXzqHDwWtyWs31vJ4FiPfi37FLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqIOpX55Wprw+vAwKsitjsPX8f5UbEy9CUrg500GaiSc6dTKb0tusv0eEEf85qQw3PCg8rbR+bN2og6lfnlamvD68DAqyK2Ow9fx/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzppjTpQ7nY6MxmLE2SQsKE8u3KKe6Plputq5gyqNv1ITAfl7JqZxvxvioHZMao/Rm+mXzm0kbd/OmmNOlDudjozGYsTZJCwoTyTv1bC0x+9E0=",
        "created_at" => "2016-03-04 18:07:19",
        "updated_at" => "2016-03-04 18:07:19"
    ),
    array(
        "manager"    => "John Smith",
        "photo"      => "",
        "name"       => "Greens Shoes Limerick",
        "telephone"  => "061 30 9389",
        "email"      => "john.smith@greenesshoes.com",
        "street"     => " Unit 29 Cresent Shopping Centre",
        "city"       => "Limerick",
        "post_code"  => "",
        "region"     => "Dooradoyle",
        "country"    => "IE",
        "latitude"   => 52.64039705,
        "longitude"  => -8.64622122,
        "content"    => "",
        "hours"      => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF19OUKxyGkywpY2/mgkiKXYbxBX1ovtYjVguRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRrptUC6g2MKvSZ8rFXjXxHop6F4UpNB6yyEhgk8X6LtP5wkt90F4wNFhUTJwpqSpDKvhTJZdG1j3yoHZMao/Rm+mXzm0kbd/Onx38fXbuaWafmdnVcpnyWgUvxOpweoXamBeKIPUdiRZrFLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqKpdylr6VkbKsw61RDG2D3Rb/EUMi2jj6SyNYLMQIE/jZ/TIx/AI1ybxg2Quw3qA7oxe9oovqKqLT5Ke++YI9FdI1noCJiez9IPbHojjGRiKGYsTZJCwoTyFi8w0HpOvnTx/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzp8d/H127mlmn5nZ1XKZ8loFL8TqcHqF2pgXiiD1HYkWaxS8+qBhGoHofvphoQVWbH6nOOwTd/Qb5pFQ5hTDiIdGBnKazZy0XXKvndJVEk8jIAL4ultuNpfSLeaQ5vrNWgOSPAXufS/ec=",
        "created_at" => "2016-03-04 18:24:07",
        "updated_at" => "2016-03-04 22:28:24"
    )
);

foreach ($stores as $store) {
    $model = Mage::getModel('studioforty9_stores/store')
        ->setData($store)
        ->save();

}
