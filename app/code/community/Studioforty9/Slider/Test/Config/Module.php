<?php

class Studioforty9_Slider_Test_Config_Module extends EcomDev_PHPUnit_Test_Case_Config
{
    /**
     * @group Slider
     */
    public function test_module_is_in_correct_code_pool()
    {
        $this->assertModuleCodePool('community');
    }

    /**
     * @group Slider
     */
    public function test_module_version_is_correct()
    {
        $this->assertModuleVersion('0.0.2');
    }

    /**
     * @group Slider
     */
    public function test_models_are_configured()
    {
        $this->assertConfigNodeValue('global/models/studioforty9_slider/class', 'Studioforty9_Slider_Model');
    }

    /**
     * @group Slider
     */
    public function test_resource_models_are_configured()
    {
        $this->assertConfigNodeValue(
            'global/models/studioforty9_slider/resourceModel',
            'studioforty9_slider_resource'
        );
        $this->assertConfigNodeValue(
            'global/models/studioforty9_slider_resource/class',
            'Studioforty9_Slider_Model_Resource'
        );
        $this->assertConfigNodeValue(
            'global/models/studioforty9_slider_resource/entities/slide/table',
            'studioforty9_slider'
        );
    }

    /**
     * @group Slider
     */
    public function test_helpers_are_configured()
    {
        $this->assertConfigNodeValue(
            'global/helpers/studioforty9_slider/class',
            'Studioforty9_Slider_Helper'
        );
    }

    /**
     * @group Slider
     */
    public function test_adminhtml_blocks_are_configured()
    {
        $this->assertConfigNodeValue(
            'global/blocks/studioforty9_slider_adminhtml/class',
            'Studioforty9_Slider_Block_Adminhtml'
        );
    }

    /**
     * @group Slider
     */
    public function test_access_granted_for_config_acl()
    {
        $this->assertConfigNodeValue(
            'adminhtml/acl/resources/all/title',
            'Allow Everything'
        );
    }

    /**
     * @group Slider
     */
    public function test_config_has_set_correct_route()
    {
        $this->assertConfigNodeValue(
            'admin/routers/slider_admin/args/frontName',
            'slider_admin'
        );
        $this->assertConfigNodeValue(
            'admin/routers/slider_admin/use',
            'admin'
        );
        $this->assertConfigNodeValue(
            'adminhtml/menu/studioforty9_slider/children/slider/action',
            'slider_admin/slider'
        );
    }
}