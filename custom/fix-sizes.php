<?php

require_once('../app/Mage.php');
Mage::app();

$filename = Mage::getBaseDir('var') . DS . 'import/fix_sizes.csv';
if (!file_exists($filename)) {
    $this->endSetup();
    return;
}

$csv = new Varien_File_Csv();
$csvData = $csv->getData($filename);

/** @var Mage_Catalog_Model_Resource_Product_Action $action */
$action = Mage::getModel('catalog/resource_product_action');
$storeId = 0;


foreach ($csvData as $row)
{

    $sku = $row[2];

    $size = getSize($row[1], $row[0]);
    if (! $size) {
        Mage::log($sku, null, 'no_size.log', true);
        continue;
    }

    $product = Mage::getModel("catalog/product")->loadByAttribute("sku", $sku);
    if (! $product)
    {
        Mage::log($sku, null, 'no_product.log', true);
        continue;
    }

    $code = ($row[1] == "LAD") ? "ladies_size" : "mens_size";

    try {
        $action->updateAttributes(array($product->getId()), array($code => $size), $storeId);
    } catch (Exception $e) {
        Mage::log("$sku", null, 'size_not_saved.log', true);
        Mage::log($e->getMessage(), null, 'size_not_saved_exception.log', true);
    }

}


/**
 * @param $key
 * @param $size
 *
 * @return false|int
 */
function getSize($key, $size)
{
    $mapper = array(
        "LAD" => array(
            "3«" => 586,
            "4«" => 588,
            "5«" => 590,
            "6«" => 592,
            "7«" => 594,
        ),
        "MEN" => array(
            "8«"  => 600,
            "9«"  => 602,
            "10«" => 604,
            "11«" => 606,
            "12«" => 608,
        )
    );

    if (! array_key_exists($key, $mapper)) {
        return false;
    }

    $row = $mapper[$key];
    return (array_key_exists($size, $row))
        ? $row[$size]
        : false;
}

