<?php

// Update Social Media Account
Mage::getConfig()->saveConfig("greenes_general/social_media/facebook", "https://www.facebook.com/Greenesshoes", 'default', 0);
Mage::getConfig()->saveConfig("greenes_general/social_media/twitter", "https://twitter.com/Greenesshoes", 'default', 0);
Mage::getConfig()->saveConfig("greenes_general/social_media/pinterest", "https://www.pinterest.com/greeneshoes/", 'default', 0);
Mage::getConfig()->saveConfig("greenes_general/social_media/youtube", "https://www.youtube.com/user/GreenesShoes", 'default', 0);
Mage::getConfig()->saveConfig("greenes_general/social_media/instagram", "https://www.instagram.com/greenes_shoes/", 'default', 0);
Mage::getConfig()->saveConfig("greenes_general/social_media/googleplus", "https://plus.google.com/115002500394431362422/", 'default', 0);
