<?php

/**
 * Class Greenes_Theme_Helper_Sales
 *
 * @extends     Mage_Core_Helper_Abstract
 * @category    Greenes
 * @package     Greenes_Theme
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Theme_Helper_Sales extends Mage_Core_Helper_Abstract
{

    /**
     * Appends the parent title
     *
     * @param Mage_Sales_Model_Order_Item $item
     *
     * @return bool|mixed|string
     */
    public function getProductTitle(Mage_Sales_Model_Order_Item $item)
    {
        /** @var Mage_Catalog_Model_Resource_Product $resource */
        $resource = Mage::getResourceModel("catalog/product");
        $title = $this->escapeHtml($item->getName());

        $options = $item->getProductOptions();
        if (! $options) {
            return $title;
        }

        if (! array_key_exists("super_product_config", $options)) {
            return $title;
        }

        if (! array_key_exists("product_id", $options['super_product_config'])) {
            return $title;
        }

        $parentId = $options['super_product_config']['product_id'];
        $parentTitle = $resource->getAttributeRawValue($parentId, 'name', Mage::app()->getStore($item->getStoreId()));

        if ($parentTitle) {
            return $parentTitle;
        }

        return $title;

    }
}