<?php
/**
 * Studioforty9 Stores Helper
 *
 * @category    Studioforty9
 * @package     Studioforty9_Stores
 * @copyright   Copyright (c) 2014 StudioForty9 (http://www.studioforty9.com)
 */
class Studioforty9_Stores_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup
{
    /**
     * setupStoresTable()
     *
     * Format:
     *   $installer->setupTable(array(
     *      'field_name' => array(
     *          'type'    => 'some_type_value', // Varien_Db_Ddl_Table::TYPE_INTEGER
     *          'size'    => 11, // depends on addColumn size param
     *          'options' => array(), // depends on addColumn options param
     *          'comment' => null // depends on addColumn comment param
     *      )
     *   ));
     *
     * @param  string $tableName
     * @param  array  $columns
     * @return $this
     */
    public function setupTable($tableName, $columns)
    {
        $table = $this->getConnection()->newTable($this->getTable($tableName));

        if (! empty($columns)) {
            foreach ($columns as $columnName => $columnSettings) {
                $table->addColumn(
                    $columnName,
                    $this->_getColumnSetting('type', $columnSettings),
                    $this->_getColumnSetting('size', $columnSettings),
                    $this->_getColumnSetting('options', $columnSettings),
                    $this->_getColumnSetting('comment', $columnSettings)
                );
            }
        }

        return $table;
    }

    /**
     * _getColumnSetting()
     *
     * Helper method to get a setting from the given $settings array.
     *
     * @param  string $setting
     * @param  array  $settings
     * @return mixed|null
     */
    private function _getColumnSetting($setting, &$settings)
    {
        if (array_key_exists($setting, $settings)) {
            return $settings[$setting];
        }

        return null;
    }
}
