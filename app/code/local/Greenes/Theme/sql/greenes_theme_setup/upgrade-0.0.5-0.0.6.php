<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

$entityCode = Mage_Catalog_Model_Product::ENTITY;

$this->addAttribute(
    $entityCode,
    "featured_product",
    array(
        'group'                    => 'Greenes',
        'type'                     => 'int',
        'input'                    => 'boolean',
        'label'                    => "Featured Product",
        'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'                  => 1,
        'required'                 => 0,
        'visible_on_front'         => 1,
        'frontend_class'           => '',
        'is_html_allowed_on_front' => 0,
        'is_configurable'          => 0,
        'searchable'               => 0,
        'filterable'               => 0,
        'comparable'               => 0,
        'unique'                   => false,
        'used_in_product_listing'  => false
    )
);

$this->endSetup();