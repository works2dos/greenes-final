<?php

/**
 * Class Greenes_Epos_Model_Validate_Numeric
 *
 * @extends     Greenes_Epos_Model_Validate_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Validate_Numeric extends Greenes_Epos_Model_Validate_Abstract
{
    /**
     * @param string $value
     *
     * @return bool
     */
    public function validate($value)
    {
        if (! is_numeric($value)) {
            return false;
        }

        return true;
    }

}