<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
/**
 * @mixin \Greenes_Epos_Model_Validate_Price
 */
class Greenes_Epos_Model_Validate_PriceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Greenes_Epos_Model_Validate_Price');
    }

    function it_should_validate_price()
    {
        $this->validate("65")->shouldReturn(true);
        $this->validate("77.99")->shouldReturn(true);
        $this->validate(65)->shouldReturn(true);
        $this->validate(77.99)->shouldReturn(true);
        $this->validate("0.00")->shouldReturn(true);
        $this->validate("0")->shouldReturn(true);
    }

    function it_should_not_validate_incorrect_price()
    {
        $this->validate("test")->shouldReturn(false);
        $this->validate("")->shouldReturn(false);
    }

}
