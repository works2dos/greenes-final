<?php

/**
 * Class Greenes_Brands_Model_Observer
 *
 * @extends     Greenes_Brands_Model_Observer
 * @category    Greenes
 * @package     Greenes_Brands
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Brands_Model_Observer extends Mage_Catalog_Model_Observer
{
    /**
     * Recursively adds categories to top menu
     *
     * @param Varien_Data_Tree_Node_Collection|array $categories
     * @param Varien_Data_Tree_Node $parentCategoryNode
     * @param Mage_Page_Block_Html_Topmenu $menuBlock
     * @param bool $addTags
     */
    protected function _addCategoriesToMenu($categories, $parentCategoryNode, $menuBlock, $addTags = false)
    {
        $categoryModel = Mage::getModel('catalog/category');
        $i = 1;
        $addChild = false;

        foreach ($categories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }

            $nodeId = 'category-node-' . $category->getId();

            $categoryModel->setId($category->getId());
            if ($addTags) {
                $menuBlock->addModelTags($categoryModel);
            }

            $tree = $parentCategoryNode->getTree();
            $categoryData = array(
                'name' => $category->getName(),
                'id' => $nodeId,
                'url' => Mage::helper('catalog/category')->getCategoryUrl($category),
                'is_active' => $this->_isActiveMenuCategory($category)
            );
            $categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $parentCategoryNode);
            $parentCategoryNode->addChild($categoryNode);

            $flatHelper = Mage::helper('catalog/category_flat');
            if ($flatHelper->isEnabled() && $flatHelper->isBuilt(true)) {
                $subcategories = (array)$category->getChildrenNodes();
            } else {
                $subcategories = $category->getChildren();
            }

            $this->_addCategoriesToMenu($subcategories, $categoryNode, $menuBlock, $addTags);


            // Changes
            if ($subcategories) {
                $i++;
            }

            if ($i === 6 && ! $addChild) {
                $addChild = true;
                $data = array(
                    'name'      => Mage::helper('greenes_brands')->__("Brands"),
                    'id'        => 'brands',
                    'url'       => Mage::helper('studioforty9_brands')->getBrandUrl(),
                    'is_active' => 1
                );
                $categoryNode = new Varien_Data_Tree_Node($data, 'id', $tree, $parentCategoryNode);
                $parentCategoryNode->addChild($categoryNode);
            }

        }
    }
}