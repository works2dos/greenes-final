<?php

require_once('../app/Mage.php');
Mage::app();


/**
 * Disable any sku which is not found in the Epos
 */

/** @var Mage_Core_Model_Resource $resource */
$resource = Mage::getModel("core/resource");
/** @var Magento_Db_Adapter_Pdo_Mysql $connection */
$connection = $resource->getConnection("core_write");

// Attribute Ids
$eav = Mage::getSingleton('eav/config');
$sizeAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "sizes")->getId();
$colourAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "colour")->getId();
$nameAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "name")->getId();
$importedSkuAttrId = $eav->getAttribute(Mage_Catalog_Model_Product::ENTITY, "imported_sku")->getId();

/**
 * Gets sku, name, colour and size from catalog where imported_sku is not set
 *
 */

$query
    = "
SELECT c.entity_id as id, c.sku as sku, v.value as name, i.value as colour, s.value as size
FROM catalog_product_entity as c
INNER JOIN catalog_product_entity_varchar as v ON (c.entity_id = v.entity_id AND v.attribute_id = $nameAttrId)
INNER JOIN catalog_product_entity_int as i ON (c.entity_id = i.entity_id AND i.attribute_id = $colourAttrId)
INNER JOIN catalog_product_entity_int as s ON (c.entity_id = s.entity_id AND s.attribute_id = $sizeAttrId)
WHERE c.type_id = 'simple'
AND NOT EXISTS (
  SELECT t.entity_id
  FROM catalog_product_entity_text as t
  WHERE t.entity_id = c.entity_id
  AND t.attribute_id = $importedSkuAttrId
  AND t.value IS NOT NULL
)
ORDER BY c.entity_id";

$productCollection = $connection->fetchAll($query);

/**
 * Helpers for updating data
 */
$storeId = 0;
$helper = Mage::helper("greenes_theme");


/** @var Mage_Catalog_Model_Resource_Product_Action $action */
$action = Mage::getModel('catalog/resource_product_action');
$disabled = Mage_Catalog_Model_Product_Status::STATUS_DISABLED;

/** @var array $product */
foreach ($productCollection as $product) {

    $productId = $product['id'];
    $sku = $product['sku'];

    try {
        $action->updateAttributes(array($productId), array('status' => $disabled), $storeId);
    } catch (Exception $e) {
        Mage::log("$sku", null, 'sku_not_saved.log', true);
        Mage::log($e->getMessage(), null, 'sku_not_saved_exc.log', true);
    }

    $sql = "DELETE FROM catalog_product_super_link WHERE product_id = $productId";
    $connection->query($sql);
    Mage::log($sku, null, 'skus_disabled.log', true);
}
