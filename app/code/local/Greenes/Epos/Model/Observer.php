<?php

/**
 * Class Greenes_Epos_Model_Observer
 *
 * @extends     Greenes_Epos_Model_Observer
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Observer extends Mage_Core_Model_Abstract
{
    public function generateFile()
    {
        Mage::getSingleton('greenes_epos/csv_aggregate')->generateFile();
    }

    public function importCsv()
    {
        Mage::getSingleton('greenes_epos/csv_import')->import();
    }

    public function updateStock()
    {
        Mage::getSingleton('greenes_epos/stock_update')->update();
    }

    public function createSimpleProducts()
    {
        Mage::getSingleton("greenes_epos/product_simple_create")->createSimpleProducts();
    }

    public function createAccessoryProducts()
    {
        Mage::getSingleton("greenes_epos/product_accessories_create")->createProducts();
    }

    public function createConfigurableProducts()
    {
        Mage::getSingleton("greenes_epos/product_configurable_create")->createConfigurableProducts();
    }

    public function errorEmail()
    {
        Mage::getSingleton('greenes_epos/error_check')->check();
    }
}