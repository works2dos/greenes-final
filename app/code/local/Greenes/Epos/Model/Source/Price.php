<?php

/**
 * Price Source Model
 *
 * Class Greenes_Epos_Model_Source_Price
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Price
    extends Greenes_Epos_Model_Source_Abstract
{
    /**
     * @return false|string
     */
    public function getValue()
    {
        $value = $this->getColumnValue();
        if (!$value) {
            return false;
        }

        if (! is_numeric($value)) {
            return false;
        }

        return $this->formatPrice($value);
    }

    /**
     * @param float $price
     *
     * @return string
     */
    public function formatPrice($price)
    {
        return number_format($price, 2, ".", "");
    }

}