<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

Mage::getConfig()->saveConfig("configswatches/general/enabled", 0);
Mage::getConfig()->deleteConfig("configswatches/general/swatch_attributes");
Mage::getConfig()->deleteConfig("configswatches/general/product_list_attribute");
Mage::getConfig()->deleteConfig("configswatches/product_detail_dimensions/width");
Mage::getConfig()->deleteConfig("configswatches/layered_nav_dimensions/width");
Mage::getConfig()->deleteConfig("configswatches/product_listing_dimensions/width");
Mage::getConfig()->deleteConfig("configswatches/product_detail_dimensions/height");
Mage::getConfig()->deleteConfig("configswatches/product_listing_dimensions/height");
Mage::getConfig()->deleteConfig("configswatches/layered_nav_dimensions/height");

$this->endSetup();