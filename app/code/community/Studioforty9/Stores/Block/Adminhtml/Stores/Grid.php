<?php
/**
 * Stores Adminhtml Block: Stores Grid
 *
 * @category   Studioforty9
 * @package    Studioforty9_Pharmacy
 * @author     Copyright (c) 2014 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Block_Adminhtml_Stores_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Edit URL
     *
     * @var string $_editUrl
     */
    protected $_editUrl = '*/*/edit';

    /**
     * _prepareCollection()
     *
     * @access protected
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /* @var Studioforty9_Pharmacy_Model_Resource_Pharmacy_Collection $collection */
        $collection = Mage::getResourceModel('studioforty9_stores/store_collection');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * getRowUrl()
     *
     * @param object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl($this->_editUrl, array('id' => $row->getId()));
    }

    /**
     *_prepareColumns()
     *
     * @access protected
     * @return $this
     */
    protected function _prepareColumns()
    {
        $helper = $this->_getHelper();

        $this->addColumn('store_id', array(
            'header' => $helper->__('ID'),
            'type'   => 'number',
            'index'  => 'store_id',
        ));
        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'type'   => 'text',
            'index'  => 'name',
        ));
        $this->addColumn('manager', array(
            'header' => $helper->__('Manager'),
            'type'   => 'text',
            'index'  => 'manager',
        ));
        $this->addColumn('email', array(
            'header' => $helper->__('Email'),
            'type'   => 'text',
            'index'  => 'email',
        ));
        $this->addColumn('telephone', array(
            'header' => $helper->__('Telephone'),
            'type'   => 'text',
            'index'  => 'telephone',
        ));
        $this->addColumn('created_at', array(
            'header' => $helper->__('Created'),
            'type'   => 'datetime',
            'index'  => 'created_at',
        ));
        $this->addColumn('updated_at', array(
            'header' => $helper->__('Updated'),
            'type'   => 'datetime',
            'index'  => 'updated_at',
        ));
        $this->addColumn('action', array(
            'header'  => $helper->__('Action'),
            'width'   => '50px',
            'type'    => 'action',
            'getter'  => 'getId',
            'actions' => array(
                array(
                    'caption' => $helper->__('Edit'),
                    'url'     => array('base' => $this->_editUrl),
                    'field'   => 'id'
                ),
            ),
            'filter'   => false,
            'sortable' => false,
            'index'    => 'entity_id',
        ));

        return parent::_prepareColumns();
    }

    /**
     * _getHelper()
     *
     * @access protected
     * @return mixed
     */
    protected function _getHelper()
    {
        return Mage::helper('studioforty9_stores');
    }
}
