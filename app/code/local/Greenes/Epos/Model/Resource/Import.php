<?php

/**
 * Class Greenes_Epos_Model_Resource_Import
 *
 * @extends     Greenes_Epos_Model_Resource_Import
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Resource_Import extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('greenes_epos/import', 'id');
    }
}