<?php

// Update stores
$stores = array(
    array(
        "store_id" => 1,
        "manager" => null,
        "photo" => null,
        "name" => "Greens Shoes, Letterkenny",
        "telephone" => "074 91 26963",
        "email" => "",
        "street" => "Unit 30 Tesco Shopping Centre",
        "city" => "Donegal",
        "post_code" => "",
        "region" => "Letterkenny",
        "country" => "IE",
        "latitude" => 54.95378459,
        "longitude" => -7.72447523,
        "content" => null,
        "hours" => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF19OUKxyGkywpY2/mgkiKXYbxBX1ovtYjVguRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRrptUC6g2MKvSZ8rFXjXxHop6F4UpNB6yyEhgk8X6LtP5wkt90F4wNFhUTJwpqSpDKvhTJZdG1j3yoHZMao/Rm+mXzm0kbd/Onx38fXbuaWafmdnVcpnyWgUvxOpweoXamBeKIPUdiRZrFLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqKpdylr6VkbKsw61RDG2D3Rw4+4trZbtvayNYLMQIE/jZ/TIx/AI1ybxg2Quw3qA7oxe9oovqKqLT5Ke++YI9FdI1noCJiez9IPbHojjGRiKGYsTZJCwoTyQUufAtZXzi7x/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzp8d/H127mlmn5nZ1XKZ8loFL8TqcHqF2pgXiiD1HYkWaxS8+qBhGoHofvphoQVWbH6nOOwTd/Qb5pFQ5hTDiIdGBnKazZy0XXlFop6OaQYQVjb+aCSIpdhmAFndKejUemX82oDklg2Y8=",
        "created_at" => "2016-03-11 09:39:30",
        "updated_at" => "2016-04-25 20:51:52"
    ),
    array(
        "store_id" => 2,
        "manager" => null,
        "photo" => null,
        "name" => "Greens Shoes, Galway",
        "telephone" => "091 56 8616",
        "email" => "",
        "street" => "Unit 218 Level 1, Eyre Square Centre",
        "city" => "Galway",
        "post_code" => "",
        "region" => "Eyre Square",
        "country" => "IE",
        "latitude" => 53.2731324,
        "longitude" => -9.0501475,
        "content" => null,
        "hours" => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF11scwYrjXbLhY2/mgkiKXYbxBX1ovtYjVguRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRrptUC6g2MKvciN+cmKf5top6F4UpNB6yyEhgk8X6LtP5wkt90F4wNFhUTJwpqSpDKvhTJZdG1j3yoHZMao/Rm+mXzm0kbd/Onx38fXbuaWacN5zUQZRojgUvxOpweoXamBeKIPUdiRZrFLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqKL++Ta4lYHQMw61RDG2D3Rb/EUMi2jj6SyNYLMQIE/jZ/TIx/AI1ybxg2Quw3qA7oxe9oovqKqLT5Ke++YI9FdI1noCJiez9KYFyXudBDCt2YsTZJCwoTyFi8w0HpOvnTx/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzp8d/H127mlmnDec1EGUaI4FL8TqcHqF2podz28XXvTJmxS8+qBhGoHofvphoQVWbH6nOOwTd/Qb5pFQ5hTDiIdGBnKazZy0XXmHnjlO5tVLEAL4ultuNpfaEpWPzeVusLOSPAXufS/ec=",
        "created_at" => "2016-03-11 09:39:30",
        "updated_at" => "2016-04-25 20:50:56"
    ),
    array(
        "store_id" => 3,
        "manager" => null,
        "photo" => null,
        "name" => "Gosh Shoes, Galway",
        "telephone" => "091 53 5924",
        "email" => "",
        "street" => "Unit 208A, Eyre Square centre, Galway.",
        "city" => "Galway",
        "post_code" => "",
        "region" => "Eyre Square",
        "country" => "IE",
        "latitude" => 53.2731324,
        "longitude" => -9.0501475,
        "content" => null,
        "hours" => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF19K2GJJ2HS/tY2/mgkiKXYYyBt+cxNEMEQuRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRrptUC6g2MKve3p8sVJ83top6F4UpNB6yydQDRWBdVP0pwkt90F4wNFhUTJwpqSpDKvhTJZdG1j3yoHZMao/Rm+mXzm0kbd/OkYPLp8FPadycN5zUQZRojgUvxOpweoXamh3Pbxde9MmbFLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqIh2U73zrtjP8w61RDG2D3RSU0CBE2DyBWyNYLMQIE/jZ/TIx/AI1ybxg2Quw3qA7oxe9oovqKqLT5Ke++YI9FdI1noCJiez9JGCMwU3PfHD2YsTZJCwoTyE6VuTg7xsDXx/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzpGDy6fBT2ncnDec1EGUaI4Mrl6R6VgzTQjXb3FXtKPjKxS8+qBhGoHofvphoQVWbH6nOOwTd/Qb5pFQ5hTDiIdGBnKazZy0XXlFop6OaQYQVjb+aCSIpdhmAFndKejUemX82oDklg2Y8=",
        "created_at" => "2016-03-11 09:39:30",
        "updated_at" => "2016-04-25 20:49:36"
    ),
    array(
        "store_id" => 4,
        "manager" => null,
        "photo" => null,
        "name" => "Greens Shoes Limerick",
        "telephone" => "061 30 9389",
        "email" => "",
        "street" => " Unit 29 Cresent Shopping Centre",
        "city" => "Limerick",
        "post_code" => "",
        "region" => "Dooradoyle",
        "country" => "IE",
        "latitude" => 52.64039705,
        "longitude" => -8.64622122,
        "content" => null,
        "hours" => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF19OUKxyGkywpY2/mgkiKXYbxBX1ovtYjVguRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRrptUC6g2MKvSZ8rFXjXxHop6F4UpNB6yyEhgk8X6LtP5wkt90F4wNFhUTJwpqSpDKvhTJZdG1j3yoHZMao/Rm+mXzm0kbd/Onx38fXbuaWafmdnVcpnyWgUvxOpweoXamBeKIPUdiRZrFLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqKpdylr6VkbKsw61RDG2D3Rb/EUMi2jj6SyNYLMQIE/jZ/TIx/AI1ybxg2Quw3qA7oxe9oovqKqLT5Ke++YI9FdI1noCJiez9IPbHojjGRiKGYsTZJCwoTyFi8w0HpOvnTx/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzp8d/H127mlmn5nZ1XKZ8loFL8TqcHqF2pgXiiD1HYkWaxS8+qBhGoHofvphoQVWbH6nOOwTd/Qb5pFQ5hTDiIdGBnKazZy0XXKvndJVEk8jIAL4ultuNpfSLeaQ5vrNWgOSPAXufS/ec=",
        "created_at" => "2016-03-11 09:39:30",
        "updated_at" => "2016-04-25 20:47:36"
    ),
    array(
        "store_id" => 5,
        "manager" => null,
        "photo" => null,
        "name" => "Greenes Shoes, Eyre Square",
        "telephone" => "091 56 7988",
        "email" => "",
        "street" => "Unit 120, Ground Floor",
        "city" => "Galway",
        "post_code" => "",
        "region" => "Eyre Square Centre",
        "country" => "IE",
        "latitude" => 53.27065677,
        "longitude" => -9.05680928,
        "content" => null,
        "hours" => "Yy9wHa8rBV9vSx3p7BytW2AtbUgsrsSIaRUOYUw4iHRgZyms2ctF13/bIlyDKIV9Y2/mgkiKXYYyBt+cxNEMEQuRcvN2zqGqCpF72f+kyDyoeKgRNnrtnrVB3mZ910advigDI1lAcRrptUC6g2MKvYqooVNq52Q4p6F4UpNB6yydQDRWBdVP0pwkt90F4wNFhUTJwpqSpDKvhTJZdG1j3yoHZMao/Rm+mXzm0kbd/OkYPLp8FPadyfmdnVcpnyWgUvxOpweoXamh3Pbxde9MmbFLz6oGEageaEXmvauJXa2hoStNnO7AshBH/OakMNzwoPK20fmzdqLS2gV6VMMxfMw61RDG2D3RSU0CBE2DyBWyNYLMQIE/jZ/TIx/AI1ybxg2Quw3qA7oxe9oovqKqLT5Ke++YI9FdI1noCJiez9KMQGhU8xbguGYsTZJCwoTybe0d90hx2MHx/lRsTL0JSk6ExjzfMx8rVjm2j1bTe0AqB2TGqP0Zvpl85tJG3fzpGDy6fBT2ncn5nZ1XKZ8loCDOKTiB82vmjXb3FXtKPjKxS8+qBhGoHofvphoQVWbH6nOOwTd/Qb5pFQ5hTDiIdGBnKazZy0XXlFop6OaQYQVjb+aCSIpdhmAFndKejUemX82oDklg2Y8=",
        "created_at" => "2016-04-25 20:46:51",
        "updated_at" => "2016-04-25 20:46:51"
    )
);

foreach ($stores as $store) {
    $model = Mage::getModel('studioforty9_stores/store')
        ->setData($store)
        ->save();
}

// Update Callout
Mage::getConfig()->saveConfig("greenes_general/callouts/callout_2", "Free Delivery and Returns in Ireland", 'default', 0);

// Hide SALE & Back to School Categories
$categories = Mage::getModel('catalog/category')
    ->getCollection()
    ->addAttributeToFilter('url_key', array(
        'in' => array('sale', 'back-to-school'))
    );
foreach ($categories as $category) {
    $category->setData('is_active', 0)->save();
}
