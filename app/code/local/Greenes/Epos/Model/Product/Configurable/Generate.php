<?php

/**
 * Generates configurable product data
 *
 * Class Greenes_Epos_Model_Product_Configurable_Generate
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Configurable_Generate extends Greenes_Epos_Model_Base
{
    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_product_";
    }

    /**
     * Gets the product data
     *
     * @param       $sku
     * @param array $products
     *
     * @return false|array
     */
    public function getProductData($sku, array $products)
    {
        $data = $this->getSimpleProductData($products);
        if (!$data) {
            $this->logError("Could not generate the configurable data as there was no row id");
            return false;
        }

        $skuColumn = $this->getAttributeHelper()->getColumnByCode("sku");
        if (!$skuColumn) {
            $this->logError("No SKU column to generate the configurable data");
            return false;
        }
        $data[$skuColumn] = $sku;
        $data['associated_products'] = $this->getSimpleProductIds($products);

        return $data;
    }

    /**
     * Gets the data from the first simple product
     *
     * @param array $products
     *
     * @return bool|mixed
     */
    public function getSimpleProductData(array $products)
    {
        $id = $this->getFirstEposId($products);
        $product = Mage::getModel('greenes_epos/import')->load($id);
        if (!$product) {
            return false;
        }
        return $product->getData();
    }

    /**
     * @param array $products
     *
     * @return int
     */
    public function getFirstEposId(array $products)
    {
        foreach ($products as $sku => $data)
        {
            return (int)$data["epos_id"];
        }
    }

    /**
     * @param array $products
     *
     * @return int
     */
    public function getSimpleProductIds(array $products)
    {
        $ids = array();
        foreach ($products as $sku => $data)
        {
            $ids[] = (int)$data["product_id"];
        }

        return $ids;
    }

    /**
     * @return Greenes_Epos_Helper_Configurable
     */
    public function getAttributeHelper()
    {
        return Mage::helper("greenes_epos/configurable");
    }

}