;(function($, window, document, undefined) {

    "use strict";

    // Create the defaults once
    var pluginName = "CurrencySwitcher",
        defaults = {
            switcher: '.currency-switcher',
            icon: 'fa fa-angle-down'
        };

    function CurrencySwitcher ( element, options ) {
        this.element = $(element);
        this.settings = $.extend( {}, defaults, options );
        this.currency = $(this.settings.switcher);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    $.extend(CurrencySwitcher.prototype, {
        init: function() {
            var self = this,
                $options = this.element.find('option'),
                $container = self.buildContainer();
            // Hide the select field
            self.element.hide();
            // Loop over the select option and build links
            $options.each(function () {
                var $option = $(this);
                var $link = self.buildLink($option);
                $container.append($link);
                if ($option.attr('selected') === 'selected') {
                    var selected = self.buildLabel($option);
                    self.currency.append(selected);
                }
            });
            self.currency.append($container);

            $('body').on('click', function(evt) {
                var $target = $(evt.target);
                if ($target.is(self.currency)) {
                    // Note: relies on a sneaky css trick to hide the
                    // elements inside the div from click tracking.
                    // See layout/_header.scss .currency-switcher
                    evt.preventDefault();
                    self.currency.toggleClass('open');
                } else {
                    self.currency.removeClass('open');
                }
            })
        },

        buildContainer: function() {
            return $('<div/>', {
                'class': 'currency-list'
            })
        },

        buildLink: function(option) {
            var currency = this.extractClassName(option);
            var $option = $('<a/>', {
                'href': option.val(),
                'class': 'currency-option currency-' + currency
            }).text(this.extractCurrencyName(option));

            return $option;
        },

        buildLabel: function(option) {
            var self = this,
                label = $('<div/>', {
                'class': 'currency-selected'
            }).text(this.extractCurrencyName(option));

            var icon = $('<span/>', {'class': self.settings.icon});
            label.append(icon);

            return label;
        },

        extractCurrencyName: function(option) {
            return option.text().replace(/\s\-\s(.*)/gi, '')
        },

        extractClassName: function(option) {
            return option.text().replace(/(.*)\s\-\s(.*)/gi, '$2').trim().toLowerCase();
        }
    });

    // Prevent against multiple instantiations
    $.fn[pluginName] = function( options ) {
        return this.each( function() {
            if ( !$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new CurrencySwitcher(this, options));
            }
        } );
    };

} )( jQuery, window, document );
