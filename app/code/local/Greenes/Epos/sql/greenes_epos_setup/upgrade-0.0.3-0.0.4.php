<?php
/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$conn = $this->getConnection();

$conn->addForeignKey(
    $this->getFkName("greenes_epos/configurable", "epos_id", "greenes_epos/import", "id"),
    $this->getTable("greenes_epos/configurable"),
    "epos_id",
    $this->getTable("greenes_epos/import"),
    "id",
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_CASCADE
);
$this->endSetup();