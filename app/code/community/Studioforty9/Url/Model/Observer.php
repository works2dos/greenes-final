<?php

/**
 * Class Studioforty9_Url_Model_Test
 *
 * @extends     Studioforty9_Url_Model_Test
 * @category    Studioforty9
 * @package     Studioforty9_Url
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Studioforty9_Url_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * This removes any duplicate URL's for hidden products
     * and then removes any custom URL's which were created as a redirect as a result of the duplicate URL
     * issue
     *
     */
    public function removeDuplicateUrls()
    {
        if (!Mage::getStoreConfig('system/studioforty9_urls/enabled')) {
            return $this;
        }

        Mage::getModel('core/config')->saveConfig('catalog/seo/save_rewrites_history', 0);
        Mage::app()->getCacheInstance()->cleanType('config');

        $stores = Mage::getModel('core/store')->getCollection()
            ->addFieldToFilter('store_id', array('neq' => 0));

        $count = 0;

        /**
         * Update URL's for hidden products with duplicate URL's to use the SKU instead
         */
        foreach ($stores as $store) {
            $hiddenProducts = Mage::getModel('catalog/product')->getCollection()
                ->setStoreId($store->getId())
                ->addAttributeToSelect('url_key')
                ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);

            foreach ($hiddenProducts as $hiddenProduct) {
                if ($hiddenProduct->getSku() !== $hiddenProduct->getUrlKey()) {
                    $hiddenProduct->setUrlKey($hiddenProduct->getSku());
                    $hiddenProduct->setStoreId($store->getId());
                    $hiddenProduct->getResource()->saveAttribute($hiddenProduct, 'url_key');
                    $count++;
                }
            }
        }

        /**
         * Truncate Table for system and URL rewrites
         */
        if ($count > 0) {
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $table = $resource->getTableName('core/url_rewrite');
            $writeConnection->query("DELETE FROM $table WHERE is_system = 1;");
            $writeConnection->query("DELETE FROM $table WHERE is_system = 0 AND id_path LIKE '%0%';");

            //reindex
            $index = Mage::getModel('index/process')->load('catalog_url', 'indexer_code');
            $index->reindexAll();

            //refresh fpc
            if (Mage::helper('core')->isModuleEnabled('Lesti_Fpc')) {
                Mage::app()->getCacheInstance()->cleanType('fpc');
            }
        }

    }
}