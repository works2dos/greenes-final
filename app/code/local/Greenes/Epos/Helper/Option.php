<?php

/**
 * Option Id Helper
 *
 * Class Greenes_Epos_Helper_Option
 *
 * @extends     Mage_Core_Helper_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Helper_Option extends Mage_Core_Helper_Abstract
{

    /**
     * Gets Option Id
     *
     * @param $label
     * @param $attribute
     *
     * @return bool
     */
    public function getOptionId($label, $attribute)
    {
        static $options = array();

        if (!array_key_exists($attribute, $options)) {
            $ids = $this->getAttributeOptions($attribute);
            if (!$ids) {
                return false;
            }
            $options[$attribute] = $this->getAttributeOptions($attribute);
        }


        $ids = $options[$attribute];

        if (array_key_exists($label, $ids)) {
            return (int)$ids[$label];
        }

        return false;
    }

    public function getAttributeOptions($attribute)
    {
        static $items = false;
        if (! $items) {
            $items =  Mage::getModel('eav/entity_attribute_option')->getCollection()->setStoreFilter()
                ->join('attribute', 'attribute.attribute_id=main_table.attribute_id', 'attribute_code');
        }

        $options = array();
        foreach ($items as $item) {
            if ($item->getAttributeCode() == $attribute) {
                $key = $item->getValue();
                $value = $item->getOptionId();
                $options[$key] = $value;
            }
        }

        return $options;
    }

}