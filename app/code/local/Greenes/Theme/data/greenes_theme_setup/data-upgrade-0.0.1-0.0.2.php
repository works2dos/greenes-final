<?php

// About Us Page
$block = Mage::getModel('cms/page')->load('about-us.html', 'identifier');
$block->setRootTemplate('one_column')->setContentHeading('Failte/ Welcome')->setContent('<h2 class="subtitle">Our History</h2>
<blockquote>Winners of the 2015 JCI National Digital Experience Award</blockquote>
<p>In business since 1939, Greenes Shoes is an Irish owned and operated company based in Donegal. With shops in Falcarragh, Letterkenny, Galway and Limerick, we sell a large selection of mens, ladies and kids shoes.</p>
<blockquote>75 Years in Business</blockquote>
<p>We feature top brands from around the word including Amy huberman, Fly London, Irregular choice, Ruby Shoo, Marco Tozzi, Heavenly feet, Ecco, gabor, Fit Flops, Tommy Bowe, Base london, Wrangler , Converse, vans , Superdry and New Balance and many many more!!<br /> <br />Whether ordering in store with our friendly, helpful staff, or online with our fast, easy, ordering system, we guarantee good customer service and a pleasant shopping experience.</p>
<p>Thank you for your Custom!<br />Please call again.</p>
<p>Winners of the 2015 JCI National Digital Experience Award</p>
<p>Go Raibh Mile Maith agat!</p>')->save();

// Delivery Page
$block = Mage::getModel('cms/page')->load('delivery.html', 'identifier');
$block->setRootTemplate('one_column')->setContentHeading('Delivery Information')->setContent('<blockquote>We offer Free Delivery and No Hassle Free Returns In Ireland and Northern Ireland</blockquote>
<p> All our deliveries are handled by &nbsp;DPD &nbsp;and all orders &nbsp;take between 2 and 3 working days. Please call our FreePhone number <strong>1800 989 500</strong> if you have any queries or email us on <a href="mailto:customerservice@greenesshoes.com">customerservice@greenesshoes.com</a></p>
<p>*Gift Cards only for use in store. Call Freephone 1800 989 500 if you would like to use your Gift Card online.</p>
<p><strong>Deliveries are free to England &nbsp;if the order is over &euro;49.00 and take between 2 and 3 working days with &nbsp;a &euro;5 delivery charge if goods cost less than 49.00 .</strong></p>
<p>Due to heavy rush these days, some products might go out of stock and your order may be cancelled.</p>
<p>Please check the product and notify the company within 48 hrs if there is any problem with the goods delivered.</p>')->save();

// Home Page
$block = Mage::getModel('cms/page')->load('home', 'identifier');
$block->setRootTemplate('one_column')->setLayoutUpdateXml('')->setContentHeading('')->setContent('<span></span>')->save();

// 404 Page
$block = Mage::getModel('cms/page')->load('no-route', 'identifier');
$block->setRootTemplate('one_column')->setTitle('404 Not Found')->setContentHeading('')->setContent('<div class="page-title">
<h1>Whoops, our bad...</h1>
</div>
<dl><dt>The page you requested was not found, and we have a fine guess why.</dt><dd>
<ul class="disc">
<li>If you typed the URL directly, please make sure the spelling is correct.</li>
<li>If you clicked on a link to get here, the link is outdated.</li>
</ul>
</dd></dl><dl><dt>What can you do?</dt><dd>Have no fear, help is near! There are many ways you can get back on track with Magento Store.</dd><dd>
<ul class="disc">
<li><a onclick="history.go(-1); return false;" href="#">Go back</a> to the previous page.</li>
<li>Use the search bar at the top of the page to search for your products.</li>
<li>Follow these links to get you back on track!<br /><a href="{{store url=""}}">Homepage</a>&nbsp;<span class="separator">|</span> <a href="{{store url="customer/account"}}">My Account</a></li>
</ul>
</dd></dl>')->save();

// Privacy Page
$block = Mage::getModel('cms/page')->load('privacy-policy.html', 'identifier');
$block->setRootTemplate('one_column')->setContentHeading('Privacy Policy')->setContent('<p>Please read the following privacy policy carefully before using this website.&nbsp; Use of the website implies an acceptance of this privacy policy. If you have any queries or concerns regarding the use of personal information please contact Greenes Shoes at: <a>info@greenesshoes.com</a>.</p>
<ul>
<li>This policy may change on occasion so please check back from time to time.</li>
<li>This policy only relates to the use of data provided to us via internet and e-mail source.</li>
<li>In order to receive information about your Personal Data, the purposes and the parties the Data is shared with, contact the Owner.</li>
</ul>
<hr />
<h2 class="subtitle">Types of Data collected</h2>
<p>The owner does not provide a list of Personal Data types collected.<br />Other Personal Data collected may be described in other sections of this privacy policy or by dedicated explanation text contextually with the Data collection.<br />The Personal Data may be freely provided by the User, or collected automatically when using this Application.<br />Any use of Cookies - or of other tracking tools - by this Application or by the owners of third party services used by this Application, unless stated otherwise, serves to identify Users and remember their preferences, for the sole purpose of providing the service required by the User.<br />Failure to provide certain Personal Data may make it impossible for this Application to provide its services.<br />Users are responsible for any Personal Data of third parties obtained, published or shared through this Application and confirm that they have the third party\'s consent to provide the Data to the Owner.<br />As you browse <a href="http://greenesshoes.com">http://greenesshoes.com</a> and other websites, online ad networks we work with may place anonymous cookies on your computer, and use similar technologies, in order to understand your interests based on your (anonymous) online activities, and thus to tailor more relevant ads to you.&nbsp;&nbsp; If you do not wish to receive such tailored advertising, you can visit this <a href="http://www.networkadvertising.org/choices/">page</a> to opt out of most companies that engage in such advertising.&nbsp; (This will not prevent you from seeing ads; the ads simply will not be delivered through these targeting methods.)</p>
<hr />
<h2 class="subtitle">Mode and place of processing the Data</h2>
<h3><strong>Methods of processing</strong></h3>
<p>The Data Controller processes the Data of Users in a proper manner and shall take appropriate security measures to prevent unauthorized access, disclosure, modification, or unauthorized destruction of the Data.<br />The Data processing is carried out using computers and/or IT enabled tools, following organizational procedures and modes strictly related to the purposes indicated. In addition to the Data Controller, in some cases, the Data may be accessible to certain types of persons in charge, involved with the operation of the site (administration, sales, marketing, legal, system administration) or external parties (such as third party technical service providers, mail carriers,hosting providers, IT companies, communications agencies) appointed, if necessary, as Data Processors by the Owner. The updated list of these parties may be requested from the Data Controller at any time.</p>
<h3><strong>Place</strong></h3>
<p>The Data is processed at the Data Controller\'s operating offices and in any other places where the parties involved with the processing are located. For further information, please contact the Data Controller.</p>
<h3><strong>Retention time</strong></h3>
<p>The Data is kept for the time necessary to provide the service requested by the User, or stated by the purposes outlined in this document, and the User can always request that the Data Controller suspend or remove the data.</p>
<p>&nbsp;</p>
<h2 class="subtitle">Additional information about Data collection and processing</h2>
<h3><strong>Legal action</strong></h3>
<p>The User\'s Personal Data may be used for legal purposes by the Data Controller, in Court or in the stages leading to possible legal action arising from improper use of this Application or the related services.<br /> The User declares to be aware that the Data Controller may be required to reveal personal data upon request of public authorities.</p>
<h3><strong>Additional information about User\'s Personal Data</strong></h3>
<p>In addition to the information contained in this privacy policy, this Application may provide the User with additional and contextual information concerning particular services or the collection and processing of Personal Data upon request.</p>
<h3><strong>System Logs and Maintenance</strong></h3>
<p>For operation and maintenance purposes, this Application and any third party services may collect files that record interaction with this Application (System Logs) or use for this purpose other Personal Data (such as IP Address).</p>
<h3>Information not contained in this policy</h3>
<p>More details concerning the collection or processing of Personal Data may be requested from the Data Controller at any time. Please see the contact information at the beginning of this document.</p>
<h3>The rights of Users</h3>
<p>Users have the right, at any time, to know whether their Personal Data has been stored and can consult the Data Controller to learn about their contents and origin, to verify their accuracy or to ask for them to be supplemented, cancelled, updated or corrected, or for their transformation into anonymous format or to block any data held in violation of the law, as well as to oppose their treatment for any and all legitimate reasons. Requests should be sent to the Data Controller at the contact information set out above.<br /> This Application does not support &ldquo;Do Not Track&rdquo; requests.<br /> To determine whether any of the third party services it uses honour the &ldquo;Do Not Track&rdquo; requests, please read their privacy policies.</p>
<h3>Changes to this privacy policy</h3>
<p>The Data Controller reserves the right to make changes to this privacy policy at any time by giving notice to its Users on this page. It is strongly recommended to check this page often, referring to the date of the last modification listed at the bottom. If a User objects to any of the changes to the Policy, the User must cease using this Application and can request that the Data Controller removes the Personal Data. Unless stated otherwise, the then-current privacy policy applies to all Personal Data the Data Controller has about Users.</p>
<h3>Definitions and legal references</h3>
<hr />
<p><strong>Personal Data (or Data)</strong></p>
<p>Any information regarding a natural person, a legal person, an institution or an association, which is, or can be, identified, even indirectly, by reference to any other information, including a personal identification number.</p>
<p><strong>Usage Data</strong></p>
<p>Information collected automatically from this Application (or third party services employed in this Application ), which can include: the IP addresses or domain names of the computers utilized by the Users who use this Application, the URI addresses (Uniform Resource Identifier), the time of the request, the method utilized to submit the request to the server, the size of the file received in response, the numerical code indicating the status of the server\'s answer (successful outcome, error, etc.), the country of origin, the features of the browser and the operating system utilized by the User, the various time details per visit (e.g., the time spent on each page within the Application) and the details about the path followed within the Application with special reference to the sequence of pages visited, and other parameters about the device operating system and/or the User\'s IT environment.</p>
<p><strong>User</strong></p>
<p>The individual using this Application, which must coincide with or be authorized by the Data Subject, to whom the Personal Data refer.</p>
<p><strong>Data Subject</strong></p>
<p>The legal or natural person to whom the Personal Data refers.</p>
<p><strong>Data Processor (or Data Supervisor)</strong></p>
<p>The natural person, legal person, public administration or any other body, association or organization authorized by the Data Controller to process the Personal Data in compliance with this privacy policy.</p>
<p><strong>Data Controller (or Owner)</strong></p>
<p>The natural person, legal person, public administration or any other body, association or organization with the right, also jointly with another Data Controller, to make decisions regarding the purposes, and the methods of processing of Personal Data and the means used, including the security measures concerning the operation and use of this Application. The Data Controller, unless otherwise specified, is the Owner of this Application.</p>
<p><strong>This Application</strong></p>
<p>The hardware or software tool by which the Personal Data of the User is collected.</p>
<p><strong>Legal information</strong></p>
<p>Notice to European Users: this privacy statement has been prepared in fulfilment of the obligations under Art. 10 of EC Directive n. 95/46/EC, and under the provisions of Directive 2002/58/EC, as revised by Directive 2009/136/EC, on the subject of Cookies. This privacy policy relates solely to this Application.</p>')->save();

// Returns Page
$block = Mage::getModel('cms/page')->load('returns.html', 'identifier');
$block->setTitle('Returns')->setRootTemplate('one_column')->setContentHeading('Returns')->setContent('<h2 class="subtitle">We offer No Hassle Free Returns In throughout Ireland and Northern Ireland.</h2>
<p>Full price goods can be returned FREE of charge with our DPD collection service, just email us at <a href="mailto:customerservice@greenesshoes.com">customerservice@greenesshoes.com</a> and we will arrange collection or call us on our FreePhone 1800 989 500 .</p>
<h3>We do not currently offer free returns on sale items</h3>
<ul>
<li>Please contact us on our FreePhone number for more information. You can also simply drop your shoes into one of our stores, within <strong>28 days</strong> of the delivery date.</li>
<li>Please also enclose a copy of the receipt for a refund/exchange to be processed.  </li>
<li>Please read our returns guidlelines below</li>
</ul>
<h2 class="subtitle">Our Returns Guidelines</h2>
<p>We want you to be happy with every purchase you make. Accordingly, you may inspect the items you buy as if you were in one of our stores. While you are deciding to keep your purchases, you do have a statutory duty to take reasonable care of the goods. Therefore please do not damage the items whilst you inspect them or try them on for size. You must ensure goods returned are unworn and repacked in the original box.</p>
<p>Also If the product is defective, please arrange return it as soon as possible and we will either deliver a new one to you or refund you the cost of the product and if applicable the original delivery cost.&nbsp; Any breakages or damages should be reported to us within 24 hours of delivery.</p>
<p>Thank you for your custom and please do not hesitate to contact us should you have any further queries.Free Phone 9 am to 6 pm 1800 989 500</p>
<p>Our return postal address is:</p>
<p>Greenes Shoes<br />Market Square Centre<br />Letterkenny<br />Co. Donegal</p>
<p><strong>or</strong><br /><a href="mailto:customerservice@greenesshoes.com">customerservice@greenesshoes.com</a></p>
<p>Due to heavy rush these days, some products might go out of stock and your order may be cancelled.</p>')->save();

// Terms Page
$block = Mage::getModel('cms/page')->load('terms', 'identifier');
$block->setRootTemplate('one_column')->setContentHeading('Terms of Service')->setContent('<ul class="disc">
<li><a href="#answer1">Shipping &amp; Delivery</a></li>
<li><a href="#answer2">Privacy &amp; Security</a></li>
<li><a href="#answer3">Returns &amp; Replacements</a></li>
<li><a href="#answer4">Ordering</a></li>
<li><a href="#answer5">Payment, Pricing &amp; Promotions</a></li>
<li><a href="#answer6">Viewing Orders</a></li>
<li><a href="#answer7">Updating Account Information</a></li>
</ul>
<dl><dt id="answer1">Shipping &amp; Delivery</dt><dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer2">Privacy &amp; Security</dt><dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer3">Returns &amp; Replacements</dt><dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer4">Ordering</dt><dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer5">Payment, Pricing &amp; Promotions</dt><dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer6">Viewing Orders</dt><dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd><dt id="answer7">Updating Account Information</dt><dd>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi luctus. Duis lobortis. Nulla nec velit. Mauris pulvinar erat non massa. Suspendisse tortor turpis, porta nec, tempus vitae, iaculis semper, pede. Cras vel libero id lectus rhoncus porta. Suspendisse convallis felis ac enim. Vivamus tortor nisl, lobortis in, faucibus et, tempus at, dui. Nunc risus. Proin scelerisque augue. Nam ullamcorper. Phasellus id massa. Pellentesque nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc augue. Aenean sed justo non leo vehicula laoreet. Praesent ipsum libero, auctor ac, tempus nec, tempor nec, justo.</dd></dl>')->save();

// Secure Payment Page
$block = Mage::getModel('cms/page')->load('secure-payment.html', 'identifier');
$block->setRootTemplate('one_column')->setContentHeading('Secure Payment')->setContent('<p>Greenes Shoes use Realex Payments to provide a secure, robust and reliable payment processing service. Information security is their top business priority. To this end they have invested in extensive security controls and infrastructure. Realex Payments are certified and approved by several leading financial institutions.</p>
<p>Their systems and security controls are based on current industry standards. There are several layers of technology in place to ensure the confidentiality, authentication and integrity of information.</p>
<ol>
<li>Realex Payments have been accredited with the AIS (Account Information Security) certification by VISA and is one of the few companies in Europe to achieve this. This shows their commitment to above industry standard in every aspect of payment processing.</li>
<li>All information when in transit via the internet is encrypted (128bit SSL) to ensure confidentiality of sensitive data.</li>
<li>All messages sent to Realex Payments and the responses from Realex Payments are authenticated using digitally signed digests.</li>
<li>All requests are verified against a legal list of IP address and referring URLs.</li>
<li>Realex Payments\' technical infrastructure is located in secure co-location facilities that have 24x7 manned security and advanced building management systems along with environmental controls.</li>
<li>Critical servers and applications are monitored constantly to threshold levels and Realex Payments staff is instantly alerted via their real time monitoring and alerting service.</li>
<li>All connections to the financial institutions are over private dedicated leased circuits, backed up with ISDN lines and in certain cases VPN connections over the internet.</li>
<li>The network is designed to be highly resilient with duplicate and triplicate systems in place depending on the critical nature of each component.</li>
<li>Realex Payments are registered with the data protection commissioner as a "Data Processor". Cardholder information is encrypted and not displayed in our reporting systems - realcontrol.</li>
</ol>')->save();
