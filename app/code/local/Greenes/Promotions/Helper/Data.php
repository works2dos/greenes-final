<?php

class Greenes_Promotions_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get a pre-scoped promotions collection.
     *
     * @return Mage_Catalog_Model_Resource_Category_Collection
     * @throws Mage_Core_Exception
     */
    protected function getPromotionsCollection()
    {
        $_collection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect(array(
                'name',
                'promotion_homepage_notes',
                'promotion_homepage_button',
                'promotion_homepage_image'
            ));

        return $_collection;
    }

    /**
     * Get a pre-scoped category promotions collection.
     *
     * @return Mage_Catalog_Model_Resource_Category_Collection
     * @throws Mage_Core_Exception
     */
    public function getCategoryPromotionsCollection()
    {
        $_collection = $this->getPromotionsCollection()
            ->addAttributeToFilter(
                'is_promoted_on_homepage', array('eq' => 1)
            );

        return $_collection;
    }

    /**
     * Get a pre-scoped product promotions collection.
     *
     * @return Mage_Catalog_Model_Resource_Category_Collection
     * @throws Mage_Core_Exception
     */
    public function getProductPromotionsCollection()
    {
        $_collection = $this->getPromotionsCollection()
            ->addAttributeToFilter(
                'is_promoted_on_homepage', array('eq' => 2)
            );

        return $_collection;
    }
}
