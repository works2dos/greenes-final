<?php

/**
 * Class Greenes_Epos_Block_Adminhtml_Attributes
 *
 * @extends     Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Block_Adminhtml_Attributes extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    public function _prepareToRender()
    {
        $this->addColumn(
            'attribute', array(
                'label' => Mage::helper('greenes_epos')->__('Attribute'),
            )
        );
        $this->addColumn(
            'source_model', array(
                'label' => Mage::helper('greenes_epos')->__('Source Model'),
            )
        );
        $this->addColumn(
            'column', array(
                'label' => Mage::helper('greenes_epos')->__('Database Column'),
            )
        );
        $this->addColumn(
            'required', array(
                'label' => Mage::helper('greenes_epos')->__('Required'),
                'style' => 'width:50px;'
            )
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('greenes_epos')->__('Add');
    }
}