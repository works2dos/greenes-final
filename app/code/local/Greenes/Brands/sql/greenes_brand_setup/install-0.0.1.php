<?php
/** @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
/** @var Magento_Db_Adapter_Pdo_Mysql $conn */

$options = Mage::getSingleton('greenes_brands/source_manufacturer')->toOptionArray();
foreach ($options as $key => $label) {
    $url_key = str_replace(' ', '-', strtolower($label));
    $data = array(
        'name' => $label,
        'url_key' => $url_key,
        'visibility' => 1,
        'description' => $label . ' products.'
    );
    $brand = Mage::getModel('studioforty9_brands/brand');
    $brand->setData($data);
    $brand->save();
}
$this->endSetup();