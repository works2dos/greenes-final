<?php
/**
 * Stores Adminhtml Index Controller
 *
 * @category   Studioforty9
 * @package    Studioforty9_Stores
 * @author     Copyright (c) 2014 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Adminhtml_StoresController extends Mage_Adminhtml_Controller_Action
{
    /**
     * indexAction()
     */
    public function indexAction()
    {
        $this->_title($this->__('Manage Stores'));

        $this->loadLayout();
        $block = $this->getLayout()->createBlock('studioforty9_stores_adminhtml/stores');
        $this->_addContent($block);

        $this->renderLayout();
    }

    /**
     * editAction()
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function editAction()
    {

        $store = Mage::getModel('studioforty9_stores/store');
        if ($storeId = $this->getRequest()->getParam('id', false)) {
            $this->_title($this->__('Edit Store'));
            $store->load($storeId);
            if ($store->getId() < 1) {
                $this->_getSession()->addError($this->__('The store you requested does not exist.'));
                return $this->_redirect('*/*/');
            }
        }

        if ($this->getRequest()->isPost()) {
            try {
                $data = $this->getRequest()->getPost('store');
                $store->addData($data);
                $store->save();

                $this->_getSession()->addSuccess($this->__('The store has been saved.'));
                $page = $this->getRequest()->getParam('back', false);
                $args = ($page) ? array('id' => $store->getId()) : array();
                return $this->_redirect('*/*/' . $page, $args);
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }

        Mage::register('current_store', $store);

        $editBlock = $this->getLayout()->createBlock('studioforty9_stores_adminhtml/stores_edit');
        $mapBlock = $this->getLayout()->createBlock('studioforty9_stores_adminhtml/stores_edit_map');
        $template = $this->getLayout()->createBlock('core/template')->setTemplate('studioforty9/stores.phtml');
        $template->setChild('store.form', $editBlock);
        $template->setChild('store.map', $mapBlock);

        $this->loadLayout()
            ->_addContent($template)
            ->getLayout()
            ->getBlock('head')
            ->addItem('skin_css', 'studioforty9/stores/css/locationpicker.css');
        
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }

        return $this->renderLayout();
    }

    /**
     * deleteAction()
     *
     * @return $this|Mage_Core_Controller_Varien_Action
     */
    public function deleteAction()
    {
        $this->_title($this->__('Delete Store'));
        $store = Mage::getModel('studioforty9_stores/stores');

        if ($storeId = $this->getRequest()->getParam('id', false)) {
            $store->load($storeId);
        }

        if ($store->getId() < 1) {
            $this->_getSession()->addError($this->__('The store you requested does not exist.'));
            return $this->_redirect('*/*/index');
        }

        try {
            $store->delete();
            $this->_getSession()->addSuccess($this->__('The store has been deleted.'));
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        return $this->_redirect('*/*/index');
    }

    /**
     * _isAllowed()
     *
     * @access protected
     * @return bool
     */
    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'index':
            case 'edit':
            case 'delete':
            default:
                $session = Mage::getSingleton('admin/session');
                $isAllowed = $session->isAllowed('studioforty9_stores/stores');
                break;
        }

        return $isAllowed;
    }
}
