<?php

/**
 * Greenes_Brands_Helper_Rewrite_Data
 *
 * @category   Studioforty9
 * @package    Studioforty9_Brands
 * @subpackage Helper
 */
class Greenes_Brands_Helper_Rewrite_Data extends Studioforty9_Brands_Helper_Data
{

    /**
     * Updates the title with product title
     *
     * @return string
     */
    public function getSeoTitle()
    {
        $title = parent::getSeoTitle();
        $handles = Mage::app()->getLayout()->getUpdate()->getHandles();
        if (!array_search('studioforty9_brands_index_list', $handles)) {
            return $title;
        }
        $urlKey = Mage::app()->getRequest()->get('url_key');
        return Mage::helper('greenes_brands/rewrite_product')->getBrandNameByUrlKey($urlKey) . " | " . $title;

    }

}
