<?php
/**
 * Studioforty9 Stores Resource Model
 *
 * @category   Studioforty9
 * @package    Studioforty9_Stores
 * @author     Copyright (c) 2014 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Model_Resource_Store extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * _construct()
     *
     * @access protected
     */
    protected function _construct()
    {
        $this->_init('studioforty9_stores/store', 'store_id');
    }
}
