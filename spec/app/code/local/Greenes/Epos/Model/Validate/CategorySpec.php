<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Validate_Category
 */
class Greenes_Epos_Model_Validate_CategorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Greenes_Epos_Model_Validate_Category');
    }

    function it_should_be_valid()
    {
        $this->validate("MEN")->shouldReturn(true);
        $this->validate("LAD")->shouldReturn(true);
        $this->validate("KID")->shouldReturn(true);
        $this->validate("UNI")->shouldReturn(true);
        $this->validate("ACC")->shouldReturn(true);
    }

    function it_should_not_be_valid()
    {
        $this->validate("")->shouldReturn(false);
        $this->validate("SHO")->shouldReturn(false);
        $this->validate("MENS")->shouldReturn(false);
        $this->validate("Ladies")->shouldReturn(false);
        $this->validate("122233")->shouldReturn(false);
    }
}
