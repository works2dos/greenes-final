<?php

$this->startSetup();

$table = $this->getConnection()->newTable($this->getTable('studioforty9_slider/slide'));

$table->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
    'identity'       => true,
    'auto_increment' => true,
    'unsigned'       => true,
    'nullable'       => false,
    'primary'        => true
));
$table->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false));
$table->addColumn('image', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false));
$table->addColumn('image_tablet', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false));
$table->addColumn('image_mobile', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false));
$table->addColumn('color', Varien_Db_Ddl_Table::TYPE_VARCHAR, 7, array('nullable' => false, 'default' => '#000000'));
$table->addColumn('products_linked', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array('nullable' => false, 'default' => 0));
$table->addColumn('products', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false));
$table->addColumn('link', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false));
$table->addColumn('summary', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array('nullable' => false));
$table->addColumn('status', Varien_Db_Ddl_Table::TYPE_SMALLINT, 6, array('nullable' => false, 'default'=> 0));
$table->addColumn('sequence', Varien_Db_Ddl_Table::TYPE_SMALLINT, 6, array('nullable' => false, 'default'=> 0));
$table->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array('nullable' => false));
$table->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array('nullable' => false));

$table->setOption('type', 'InnoDB');
$table->setOption('charset', 'utf8');

$this->getConnection()->createTable($table);

$this->endSetup();
