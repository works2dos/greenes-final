<?php
/** @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
/** @var Magento_Db_Adapter_Pdo_Mysql $conn */

/** @var Studioforty9_Brands_Model_Resource_Brand_Collection $brands */
$brands = Mage::getModel('studioforty9_brands/brand')
    ->getCollection();

$brands->addFieldToFilter(
    'name',
    array('in', array(
        'ANK',
        'ANY',
        'Art',
        'ASS',
        'BAR',
        'BBS',
        'Birkenstock',
        'BlowFish',
        'BNO',
        'BPW',
        'Bronx',
        'Bruno Premi',
        'Camper',
        'CAR',
        'COX',
        'DBS',
        'DC Shoes',
        'Dolcis',
        'ELM',
        'EQU',
        'GOR',
        'HIT',
        'Hummel',
        'Hush Puppies',
        'Imac',
        'INU',
        'IPA',
        'IPANEMA',
        'Iron Fist',
        'Jana',
        'JAY',
        'Jessica',
        'JNR',
        'KRO',
        'LBA',
        'Lilli Kelly',
        'LMG',
        'Lotus',
        'LUN',
        'LYD',
        'MOC',
        'Mustang',
        'NEW',
        'NOR',
        'Odessa',
        'ORI',
        'PAD',
        'Padders',
        'PAN',
        'PIT',
        'Pitillos',
        'PLR',
        'Poetic Licence',
        'Rascal',
        'RMW',
        'Rocket Dog',
        'Rockport',
        'SAN',
        'Sauve',
        'Sebago',
        'SEL',
        'SPF',
        'Sponge',
        'Spot On',
        'SPS',
        'Timeless',
        'UKD',
        'UNI',
        'Vivo Barefoot',
        'Vivo Barefoot',
        'WHE',
        'WLR',
    )
    )
);


/** @var Studioforty9_Brands_Model_Brand $brand */
foreach ($brands as $brand) {
    $brand->setVisibility(0);
    $brand->save();
}
$this->endSetup();