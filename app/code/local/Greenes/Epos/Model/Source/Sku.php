<?php

/**
 * SKU Source Model
 *
 * Class Greenes_Epos_Model_Source_Sku
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Sku extends Greenes_Epos_Model_Source_Abstract
{

    /**
     * @return false|string
     */
    public function getValue()
    {
        $value = $this->getColumnValue();
        if (! $value) {
            return false;
        }

        $colour = $this->getColour();
        if (! $colour) {
            return $value;
        }

        $value = $value . "-" . strtolower($colour);

        if ($this->isUnisexProduct())
        {
            $value .= "-unisex";
        }

        return $value;
    }

    /**
     * @param $code
     *
     * @return false|string
     */
    public function getColour()
    {
        $code = "colour";
        $sourceModel = $this->getSourceModelByCode($code);
        if (!$sourceModel) {
            return false;
        }
        $column = $this->getAttributeHelper()->getColumnByCode($code);
        $sourceModel->setRowData($this->getRowData(), $column);
        return $sourceModel->getColourLabel();
    }

    /**
     * We append "-unisex" to Unisex products due to conflicts with the current catalogue
     *
     * @return bool
     */
    public function isUnisexProduct()
    {
        $unisexId = $this->getAttributeSetHelper()->getSetId("UNI");
        $attributeSetId = $this->getRowValueByAttributeCode("attribute_set_id");
        return ($unisexId === $attributeSetId)
            ? true
            : false;
    }

}