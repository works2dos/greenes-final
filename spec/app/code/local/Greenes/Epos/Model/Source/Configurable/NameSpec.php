<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Configurable_Name
 */
class Greenes_Epos_Model_Source_Configurable_NameSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Name');
    }

    function it_should_be_valid()
    {
        $nameColumn = \Mage::helper("greenes_epos/configurable")->getColumnByCode("name");
        $colourColumn = \Mage::helper("greenes_epos/configurable")->getColumnByCode("colour");

        $name = "Sample Product";
        $colour = "BLA";

        $data = array(
            $nameColumn     => $name,
            $colourColumn   => $colour,
        );

        $matchedTitle = "Sample Product - Black";
        $this->setRowData($data, $nameColumn);
        $this->getValue()->shouldBeString();
        $this->getValue()->shouldBe($matchedTitle);
    }

}
