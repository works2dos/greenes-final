<?php

/**
 * Class Greenes_Epos_Model_Validate_Abstract
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
abstract class Greenes_Epos_Model_Validate_Abstract extends Mage_Core_Model_Abstract
{
    /**
     * @param string $value
     *
     * @return bool
     */
    abstract public function validate($value);

}