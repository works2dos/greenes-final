<?php

/**
 * Studioforty9_Stores_Block_Stores
 *
 * @category   Studioforty9
 * @package    Studioforty9_Pharmacy
 * @author     Copyright (c) 2014 StudioForty9
 */
class Studioforty9_Stores_Block_Stores extends Mage_Core_Block_Template
{
    /**
     * getPharmacies()
     *
     * @return Studioforty9_Stores_Model_Resource_Store_Collection
     */
    public function getStores()
    {
        return Mage::getModel('studioforty9_stores/store')->getCollection();
    }

    /**
     * getJson()
     *
     * @param Studioforty9_Stores_Model_Store $stores
     * @return string
     */
    public function getJson($stores)
    {
        $collection = array();
        foreach ($stores as $store) {
            $store->setData('address', $store->getAddress());
            $store->setData('url_key', $store->getUrlKey());
            $collection[] = $store->toArray();
        }
        /* @var Mage_Core_Helper_Data $helper */
        return Mage::helper('core')->jsonEncode($collection);
    }

    /**
     * Determine if the hours provided indicate that the store is closed.
     *
     * @param array $hours
     *
     * @return bool
     */
    public function isClosed($hours)
    {
        if (!array_key_exists('times', $hours)) {
            return true;
        }

        if (!isset($hours['times']['open']) || !isset($hours['times']['close'])) {
            return true;
        }

        $open = str_replace('-', '', trim($hours['times']['open'])) ;
        $close = str_replace('-', '', trim($hours['times']['close']));

        if (empty($open) || empty($close)) {
            return true;
        }

        return false;
    }

    /**
     * isActive()
     *
     * @param Studioforty9_Stores_Model_Store $store
     * @return bool
     */
    public function isActive($store)
    {
        if (! $this->hasStore()) {
            return false;
        }

        return ($store->getId() === $this->getStore()->getId());
    }
}
