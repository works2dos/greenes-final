<?php

/**
 * HTML Source Model
 *
 * Class Greenes_Epos_Model_Source_Html
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Html
    extends Greenes_Epos_Model_Source_Abstract
{
    /**
     * @return false|string
     */
    public function getValue()
    {
        $value = $this->getColumnValue();
        if (!$value) {
            return false;
        }
        return "<p>" . $value . "</p>";
    }

}