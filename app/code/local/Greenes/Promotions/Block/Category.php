<?php

class Greenes_Promotions_Block_Category extends Mage_Core_Block_Template
{
    /** @var Mage_Catalog_Model_Category */
    protected $_category;

    /**
     * Set the category to display.
     *
     * @param Mage_Catalog_Model_Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->_category = $category;
        return $this;
    }

    /**
     * Get the category to display.
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        return $this->_category;
    }

    /**
     * Before rendering html, but after trying to load cache
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        if (is_null($this->_category) && $this->hasCategoryId()) {
            $_collection = Mage::helper('greenes_promotions')
                ->getCategoryPromotionsCollection()
                ->addAttributeToFilter('entity_id', array('eq' => $this->getCategoryId()));

            $this->_category = $_collection->getFirstItem();
        }
    }

    /**
     * Get the full URL to the promotion image.
     *
     * @return string
     */
    public function getPromotionImage()
    {
        $path = sprintf('media/catalog/category/%s',
            $this->getCategory()->getPromotionHomepageImage()
        );

        return $this->getBaseUrl() . $path;
    }

    /**
     * Get the promotion notes for display.
     *
     * @return string
     */
    public function getPromotionNotes()
    {
        return $this->getCategory()->getPromotionHomepageNotes();
    }

    /**
     * Get the full category URL of the promotion.
     *
     * @return string
     */
    public function getPromotionUrl()
    {
        return $this->getCategory()->getUrl();
    }

    /**
     * Get the button text for display.
     *
     * @return string
     */
    public function getPromotionButtonText()
    {
        return $this->getCategory()->getPromotionHomepageButton();
    }
}
