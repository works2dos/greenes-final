<?php

/**
 * Class Greenes_Epos_Model_Product_Create_Configurable
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Create_Configurable extends Greenes_Epos_Model_Product_Create_Simple
{

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_product_configurable_";
    }

    /**
     * Updated Configurable Defaults
     *
     * @return array
     */
    public function getDefaultProductValues()
    {
        $data = parent::getDefaultProductValues();
        $data["visibility"] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
        $data["type_id"] = Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE;
        return $data;
    }

    /**
     * @return Greenes_Epos_Helper_Configurable
     */
    public function getAttributeHelper()
    {
        return Mage::helper('greenes_epos/configurable');
    }

    /**
     * @param array $attributes
     * @param array $data
     */
    public function create(array $attributes, array $data)
    {
        if ($this->doesProductExist($data)) {
            return $this->updateAssociatedProducts($data);
        }

        if (!$this->_setProductAttributes($attributes, $data)) {
            return false;
        }

        if (!$this->setAssociatedProducts($data)) {
            $this->logError("Could not associate products");
            $this->logError($data);
            return false;
        }

        return $this->_createProduct();

    }

    /**
     * Sets the associated products
     *
     * @param array $data
     *
     * @return bool
     */
    public function setAssociatedProducts(array $data)
    {
        if (!array_key_exists("associated_products", $data)) {
            return false;
        }

        $associatedProducts = $data["associated_products"];
        if (!$this->setConfigurableAttribute()) {
            return false;
        }

        $attributeId = $this->getSizeId();
        $attributeCode = $this->getSizeCode();
        $configurable = $this->getProduct();

        $configurableProductsData = array();
        foreach ($associatedProducts as $productId) {
            $product = Mage::getModel("catalog/product")->load($productId);
            if (! $product) {
                return false;
            }
            $data = array();
            $data[0] = array(
                0 => array(
                    'label'         => $product->getAttributeText($attributeCode),
                    'attribute_id'  => $attributeId,
                    'value_index'   => $product->getData($attributeCode),
                    'is_percent'    => '0',
                    'pricing_value' => '0'
                )
            );
            $configurableProductsData[$product->getId()] = $data;
        }


        $valuesArray = array();
        foreach ($configurableProductsData as $data) {
            $valuesArray[] = $data;
        }

        $configurableAttributesData[0]['values'] = $valuesArray;
        $configurable->setConfigurableProductsData($configurableProductsData);
        $this->setProduct($configurable);
        return true;
    }

    /**
     * Sets the configurable attribute
     *
     * @return bool
     */
    public function setConfigurableAttribute()
    {
        $product = $this->getProduct();
        $id = $this->getSizeId();
        if (!$id) {
            return false;
        }

        $product->getTypeInstance()->setUsedProductAttributeIds(array($id));
        $configurableAttributesData = $product->getTypeInstance()->getConfigurableAttributesAsArray();
        $product->setCanSaveConfigurableAttributes(true);
        $product->setConfigurableAttributesData($configurableAttributesData);
        $product->setHasTypeOptions(true);

        $this->setProduct($product);
        return true;
    }

    /**
     * @return bool|int
     */
    public function getSizeId()
    {
        $data = $this->getData();
        if (array_key_exists("size_id", $data)) {
            return $data['size_id'];
        }

        $code = $this->getSizeCode();
        if (!$code) {
            return false;
        }

        $eavModel = Mage::getModel('eav/entity_attribute');
        $id = (int)$eavModel->getIdByCode('catalog_product', $code);
        $this->setData("size_id", $id);
        return $id;
    }

    /**
     * @return string
     */
    public function getSizeCode()
    {
        $product = $this->getProduct();
        $setId = (int)$product->getData('attribute_set_id');
        if (!$setId) {
            $this->logWarning("No atribute set id to associate size for the product");
            return false;
        }

        /**
         * Get the size by the set id
         */
        foreach ($this->getAttributeSetHelper()->getAttributes() as $key => $row) {
            if ((int)$row["set_id"] === $setId) {
               return $row["size"];
            }
        }
    }

    /**
     * Updates associated products
     *
     * @param array $data
     *
     * @return bool
     */
    public function updateAssociatedProducts(array $data)
    {
        $configurable = $this->getProduct();
        if (! array_key_exists("associated_products", $data)) {
            $this->logError("Could not update configurables as there were no simple products added to the data array");
            return false;
        }

        return Mage::getModel("greenes_epos/product_configurable_update")
            ->update($configurable, $data['associated_products']);
    }

}