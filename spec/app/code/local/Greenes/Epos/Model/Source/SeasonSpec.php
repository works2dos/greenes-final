<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Season
 */
class Greenes_Epos_Model_Source_SeasonSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Season');
    }

    function it_should_be_valid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("season");
        $file = $this->getFile();
        $value = $file[0][0];
        $label = $file[0][1];

        $data = array($column => $value);
        $this->setRowData($data, $column);
        $this->getValue()->shouldBe($label);
    }
}
