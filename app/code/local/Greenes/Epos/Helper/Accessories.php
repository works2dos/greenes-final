<?php

/**
 * Attribute Helper
 *
 * Class Greenes_Epos_Helper_Accessories
 *
 * @extends     Greenes_Epos_Helper_Attributes
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Helper_Accessories extends Greenes_Epos_Helper_Attributes
{
    /**
     * @return mixed
     */
    public function getConfigValues()
    {
        return Mage::getStoreConfig("greenes_epos/accessories/attributes");
    }
}