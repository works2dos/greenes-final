<?php

/**
 * Class Greenes_Epos_Model_Abstract
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
abstract class Greenes_Epos_Model_Log_Abstract extends Mage_Core_Model_Abstract
{
    const ERROR = 'error';
    const WARNING = 'warning';
    const SUCCESS = 'success';

    /**
     * @return string
     */
    abstract public function getLogPrefix();

    /**
     * @param        $message
     * @param string $type
     *
     * @return $this
     */
    public function log($message, $type = self::ERROR)
    {
        if ($this->shouldLog($type)) {
            $filename = $this->getLogFileName($type);
            Mage::log($message, null, $filename, true);
        }

        return $this;
    }

    /**
     * @param $message
     *
     * @return Greenes_Epos_Model_Base
     */
    public function logWarning($message)
    {
        return $this->log($message, self::WARNING);
    }

    /**
     * @param $message
     *
     * @return Greenes_Epos_Model_Base
     */
    public function logError($message)
    {
        return $this->log($message, self::ERROR);
    }

    /**
     * @param $message
     *
     * @return Greenes_Epos_Model_Base
     */
    public function logSuccess($message)
    {
        return $this->log($message, self::SUCCESS);
    }


    /**
     * @param $type
     *
     * @return bool
     */
    public function shouldLog($type)
    {
        if ($type == self::ERROR) {
            return true;
        }
        if (Mage::getStoreConfig('greenes_epos/general/debug')) {
            return true;
        }

        return false;
    }

    /**
     * @param $type
     *
     * @return string
     */
    public function getLogFileName($type)
    {
        if ($type == self::WARNING) {
            return $this->getLogPrefix() . self::WARNING . ".log";
        }

        if ($type == self::SUCCESS) {
            return $this->getLogPrefix() . self::SUCCESS . ".log";
        }

        return $this->getLogPrefix() . self::ERROR . ".log";
    }
}