<?php

/**
 * Class Greenes_Epos_Model_Error_Check
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Error_Check extends Greenes_Epos_Model_Base
{
    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_email_";
    }

    /**
     * @return bool
     */
    public function check()
    {
        $simple = Mage::getModel("greenes_epos/import")->getCollection();
        $configurable = Mage::getModel("greenes_epos/configurable")->getCollection();
        if (!$simple->count() && !$configurable->count()) {
            return true;
        }

        return $this->sendErrorEmail();
    }

    /**
     * @return bool
     */
    public function sendErrorEmail()
    {
        $template = Mage::getStoreConfig("greenes_epos/error/template");
        $email = Mage::getStoreConfig("greenes_epos/error/email");
        if (!$template || !$email) {
            $this->logError("Email could not be sent as there was either no email or template set");
            return false;
        }

        $replyTo = "info@greenesshoes.com";
        $sender = array(
            "email" => $replyTo,
            "name"  => "Greenes Shoes"
        );

        try {
            $this->sendTransactionalEmail($replyTo, $template, $sender, $email);
            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    /**
     * sendTransactionalEmail()
     *
     * @param  string $replyTo
     * @param  string $template
     * @param  string $sender
     * @param  string $recipient
     * @param  array  $data
     *
     * @return bool
     */
    public function sendTransactionalEmail($replyTo, $template, $sender, $recipient, array $data = array())
    {
        $mailTemplate = Mage::getModel('core/email_template');
        $mailTemplate->setDesignConfig(array('area' => 'frontend'));
        $mailTemplate->setReplyTo($replyTo);

        try {
            $mailTemplate->sendTransactional($template, $sender, $recipient, null, $data);
        } catch (Exception $e) {
            $this->logError($e->getMessage());
            return false;
        }

        return true;
    }
}