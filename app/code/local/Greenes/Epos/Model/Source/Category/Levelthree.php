<?php

/**
 * Sub Category Level 3
 *
 * Class Greenes_Epos_Model_Source_Category_Levelthree
 *
 * @extends     Greenes_Epos_Model_Source_Category_Leveltwo
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Category_Levelthree
    extends Greenes_Epos_Model_Source_Category_Leveltwo
{
    use Greenes_Epos_Model_Source_Csv_Trait;

    /**
     * @param string $value
     *
     * @return false|string
     */
    public function getValue()
    {
        return parent::getValue();
    }

    /**
     * @return string
     */
    public function getFileConfigPath()
    {
        return "greenes_epos/simple/category_csv";
    }


    /**
     * @return string
     */
    public function getParentCategoryAttributeCode()
    {
        return "subcategory";
    }
}