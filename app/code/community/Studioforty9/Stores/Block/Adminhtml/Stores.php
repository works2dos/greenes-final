<?php
/**
 * Studioforty9_Stores_Block_Adminhtml_Stores
 *
 * @category   Studioforty9
 * @package    Studioforty9_Pharmacy
 * @author     Copyright (c) 2014 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Block_Adminhtml_Stores extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * @var string $_blockGroup
     */
    protected $_blockGroup = 'studioforty9_stores_adminhtml';

    /**
     * This actually refers to the folder containing our Grid.php and Edit.php files
     * @var string $_controller
     */
    protected $_controller = 'stores';

    /**
     * _construct()
     *
     * This is not a typical constructor, it is called by the parent
     * constructor.
     *
     * @access protected
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_headerText = Mage::helper('studioforty9_stores')->__('Stores');
    }

    /**
     * getCreateUrl()
     *
     * Returns the create/edit url.
     *
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/edit');
    }
}
