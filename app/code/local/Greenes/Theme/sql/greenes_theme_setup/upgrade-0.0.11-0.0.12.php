<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

/**
 * Create size attributes
 */
$entityCode = Mage_Catalog_Model_Product::ENTITY;
$this->addAttribute(
    $entityCode,
    "kids_size",
    array(
        'user_defined'             => 1,
        'type'                     => 'int',
        'input'                    => 'select',
        'label'                    => "Kids Size",
        'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'apply_to'                 => 'simple',
        'visible'                  => 1,
        'required'                 => 1,
        'visible_on_front'         => 0,
        'frontend_class'           => '',
        'is_html_allowed_on_front' => 0,
        'is_configurable'          => 1,
        'searchable'               => 0,
        'filterable'               => 1,
        'comparable'               => 1,
        'unique'                   => 0,
        'used_in_product_listing'  => 0,
        'used_for_promo_rules'     => 1,
        'position'                 => 1,
        'option'                   => array(
            'values' => array(
                'UK 6 (EU 23)',
                'UK 7 (EU 24)',
                'UK 7.5 (EU 25)',
                'UK 8 (EU 26)',
                'UK 9 (EU 27)',
                'UK 10 (EU 28)',
                'UK 11 (EU 29)',
                'UK 12 (EU 30)',
                'UK 13 (EU 31)',
                'UK 1 (EU 32)',
                'UK 1.5 (EU 33)',
                'UK 2 (EU 34)',
                'UK 2.5 (EU 35)',
                'UK 3 (EU 36)',
                'UK 4 (EU 37)',
                'UK 5 (EU 38)',
            )
        ),
    )
);

$this->addAttribute(
    $entityCode,
    "ladies_size",
    array(
        'user_defined'             => 1,
        'type'                     => 'int',
        'input'                    => 'select',
        'label'                    => "Ladies Size",
        'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'apply_to'                 => 'simple',
        'visible'                  => 1,
        'required'                 => 1,
        'visible_on_front'         => 0,
        'frontend_class'           => '',
        'is_html_allowed_on_front' => 0,
        'is_configurable'          => 1,
        'searchable'               => 0,
        'filterable'               => 1,
        'comparable'               => 1,
        'unique'                   => 0,
        'used_in_product_listing'  => 0,
        'used_for_promo_rules'     => 1,
        'position'                 => 2,
        'option'                   => array(
            'values' => array(
                'UK 3 (EU 36)',
                'UK 3.5 (EU 36.5)',
                'UK 4 (EU 37)',
                'UK 4.5 (EU 37.5)',
                'UK 5 (EU 38)',
                'UK 5.5 (EU 38.5)',
                'UK 6 (EU 39)',
                'UK 6.5 (EU 39.5)',
                'UK 7 (EU 40)',
                'UK 7.5 (EU 41)',
                'UK 8 (EU 42)',
                'UK 9 (EU 43)'
            )
        ),
    )
);

$this->addAttribute(
    $entityCode,
    "mens_size",
    array(
        'user_defined'             => 1,
        'type'                     => 'int',
        'input'                    => 'select',
        'label'                    => "Mens Size",
        'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'apply_to'                 => 'simple',
        'visible'                  => 1,
        'required'                 => 1,
        'visible_on_front'         => 0,
        'frontend_class'           => '',
        'is_html_allowed_on_front' => 0,
        'is_configurable'          => 1,
        'searchable'               => 0,
        'filterable'               => 1,
        'comparable'               => 1,
        'unique'                   => 0,
        'used_in_product_listing'  => 0,
        'used_for_promo_rules'     => 1,
        'position'                 => 3,
        'option'                   => array(
            'values' => array(
                'UK 6 (EU 40)',
                'UK 7 (EU 41)',
                'UK 8 (EU 42)',
                'UK 8.5 (EU 42.5)',
                'UK 9 (EU 43)',
                'UK 9.5 (EU 43.5)',
                'UK 10 (EU 44)',
                'UK 10.5 (EU 44.5)',
                'UK 11 (EU 45)',
                'UK 11.5 (EU 45.5)',
                'UK 12 (EU 46)',
                'UK 12.5 (EU 46.5)',
                'UK 13 (EU 47)',
                'UK 14 (EU 48)',
                'UK 14.5 (EU 49)',
                'UK 15 (EU 50)',
            )
        ),
    )
);

$this->endSetup();