<?php

/**
 * Class Greenes_Epos_Model_Product_Simple_Create
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Simple_Create extends Greenes_Epos_Model_Base
{
    /**
     * Set are as admin and clear the config
     */
    protected function _construct()
    {
        /**
         * Set the current store as admin to create the products
         */
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        /**
         * Clear the config cache
         */
        Mage::app()->getCacheInstance()->cleanType('config');

        return parent::_construct();
    }

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_product_simple_";
    }

    /**
     * Creates the products
     *
     * @return bool
     */
    public function createSimpleProducts()
    {
        ini_set("memory_limit", "1024M");

        /**
         * Get Products
         */
        $products = $this->getProducts();
        if (!$products) {
            $this->logWarning("No products found for importing.");
            return true;
        }

        /**
         * Create Simple Products
         */
        $products = $this->_createProducts($products);
        if (!$products) {
            $this->logError("No simple products created.");
            return false;
        }

        return true;
    }

    /**
     * Groups Products by SKU and Colour
     *
     * @return array|false
     */
    public function getProducts()
    {
        return Mage::getModel("greenes_epos/product_simple_get")->getProducts();
    }


    /**
     * @param array $productGroups
     *
     * @return bool
     */
    protected function _createProducts(array $productGroups)
    {
        $attributes = $this->getAttributes();
        if (!$attributes) {
            $this->logError("Attributes need to set for simple products in configuration");
            return false;
        }


        $error = true;
        foreach ($productGroups as $configurable => $products) {

            $createdProducts = array();

            foreach ($products as $row) {
                $model = Mage::getModel('greenes_epos/product_create_simple');
                if (!$model->create($attributes, $row)) {
                    continue;
                }
                $id = $row['id'];
                $createdProducts[] = array(
                    "configurable_sku" => $configurable,
                    "id"               => $model->getProductId(),
                    "epos_id"          => $id
                );
            }


            if (empty($createdProducts)) {
                $this->logWarning("Could not create simple products for $configurable");
                continue;
            }

            // Add to configurable table
            if (!$this->updateConfigurableTable($createdProducts)) {
                $this->logError("Could not add products to the configurable table for $configurable");
                $this->logError($products);
                $this->_deleteProducts($createdProducts);
                $error = true;
                continue;
            }

        }

        return ($error) ? false : true;
    }

    /**
     * @param array $rows
     *
     * @return int
     */
    public function updateConfigurableTable(array $rows)
    {
        $write = Mage::getSingleton("core/resource")->getConnection("core_write");
        $table = Mage::getModel("core/resource")->getTableName("greenes_epos/configurable");
        try {
            $write->insertMultiple($table, $rows);
        } catch (Exception $e) {
            $this->logError($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param array $products
     *
     * @return $this
     */
    protected function _deleteProducts(array $products)
    {
        $error = false;
        foreach ($products as $row) {
            $product = Mage::getModel("catalog/product")->load($row['id']);
            try {
                $product->delete();
            } catch (Exception $e) {
                $error = true;
                $this->logError("Could not delete product");
                $this->logError($e->getMessage());
                $this->logError($row);
            }
        }

        return ($error) ? false : true;
    }

    /**
     * @return array|bool
     */
    public function getAttributes()
    {
        $attributes = $this->getAttributeHelper()->getAttributes();
        if (!$attributes) {
            return false;
        }
        return array_keys($attributes);
    }
}
