<?php

/**
 * Attribute Set Mapper
 *
 * Class Greenes_Epos_Helper_Attributeset
 *
 * @extends     Mage_Core_Helper_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Helper_Attributeset extends Mage_Core_Helper_Abstract
{

    /**
     * @var array
     */
    protected $_attributes = array(
        "LAD" => array(
            "set_id" => 9,
            "size" => "ladies_size",
        ),
        "MEN" => array(
            "set_id" => 10,
            "size" => "mens_size",
        ),
        "KID" => array(
            "set_id" => 11,
            "size" => "kids_size",
        ),
        "ACC" => array(
            "set_id" => 12,
            "size" => "accessories_size",
        ),
        "UNI" => array(
            "set_id" => 13,
            "size" => "unisex_size",
        ),
    );

    /**
     * @param string $key
     *
     * @return false|int
     */
    public function getSetId($key)
    {
        return $this->getAttributeValue($key, "set_id");
    }

    /**
     * @param $key
     * @param $value
     *
     * @return bool
     */
    public function getAttributeValue($key, $value)
    {
        $attributes = $this->getAttributes();
        if (! array_key_exists($key, $attributes)) {
            return false;
        }

        $attribute = $attributes[$key];

        if (! array_key_exists($value, $attribute)) {
            return false;
        }

        return $attribute[$value];
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * @param string $key
     *
     * @return false|int
     */
    public function getSize($key)
    {
        return $this->getAttributeValue($key, "size");
    }


    /**
     * @return array
     */
    public function getAllSizes()
    {
        $sizes = array();
        foreach ($this->getAttributes() as $key => $row)
        {
            $sizes[] = $row["size"];
        }

        return $sizes;
    }

}