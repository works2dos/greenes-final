<?php

class Studioforty9_Slider_Model_Resource_Slide_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Tell Magento the Model and Resource Model to use for this
     * Collection. Since both aliases are the same we can omit the
     * second parameter if we wish.
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('studioforty9_slider/slide');
    }
}
