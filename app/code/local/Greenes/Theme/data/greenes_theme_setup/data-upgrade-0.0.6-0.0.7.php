<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();
$colorAttributeId = 140;
$height = 50;
$width = 50;

Mage::getConfig()->saveConfig("configswatches/general/enabled", 1, 'default', 0);
Mage::getConfig()->saveConfig("configswatches/general/swatch_attributes", $colorAttributeId, 'default', 0);
Mage::getConfig()->saveConfig("configswatches/general/product_list_attribute", $colorAttributeId, 'default', 0);
Mage::getConfig()->saveConfig("configswatches/product_detail_dimensions/width", $width, 'default', 0);
Mage::getConfig()->saveConfig("configswatches/layered_nav_dimensions/width", $width, 'default', 0);
Mage::getConfig()->saveConfig("configswatches/product_listing_dimensions/width", $width, 'default', 0);
Mage::getConfig()->saveConfig("configswatches/product_detail_dimensions/height", $height, 'default', 0);
Mage::getConfig()->saveConfig("configswatches/product_listing_dimensions/height", $height, 'default', 0);
Mage::getConfig()->saveConfig("configswatches/layered_nav_dimensions/height", $height, 'default', 0);

$this->endSetup();