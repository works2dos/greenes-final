<?php
/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$conn = $this->getConnection();
/** @var Varien_Db_Ddl_Table $table */
$table = $conn->newTable($this->getTable('greenes_epos/configurable'));
$table
    ->addColumn(
        'configurable_sku',
        Varien_Db_Ddl_Table::TYPE_VARCHAR,
        255,
        array(
            'nullable' => false
        )
    )
    ->addColumn(
        'id',
        Varien_Db_Ddl_Table::TYPE_INTEGER,
        11,
        array(
            'nullable'       => false
        )
    );

$this->getConnection()->createTable($table);
$this->endSetup();