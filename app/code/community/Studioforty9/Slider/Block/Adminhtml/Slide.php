<?php

class Studioforty9_Slider_Block_Adminhtml_Slide extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * The $_blockGroup property tells Magento which alias to use to
     * locate the blocks to be displayed within this grid container.
     * @var string $_blockGroup
     */
    protected $_blockGroup = 'studioforty9_slider_adminhtml';

    /**
     * $_controller refers to the folder containing our Grid and Edit
     * @var string $_controller
     */
    protected $_controller = 'slide';

    /**
     * _construct()
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_headerText = $this->_getHelper()->__('Slider Manager');
    }

    /**
     * When the Add button is clicked, this is where the user should
     * be redirected to.
     *
     * @return string
     */
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/edit');
    }

    /**
     * _getHelper()
     *
     * @return Studioforty9_Slider_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('studioforty9_slider');
    }
}
