<?php

$slides = array(
    array(
        "title"           => "Amy Huberman Collection",
        "image"           => "amy-huberman.jpg",
        "image_tablet"    => "",
        "image_mobile"    => "",
        "color"           => "#272727",
        "products_linked" => 0,
        "products"        => "",
        "link"            => "/brands/amy-huberman",
        "summary"         => "Slideshow sub headline lorem ipsum dolor sit amet, consectetur adipisicing elit, sed.",
        "status"          => 1,
        "sequence"        => 1,
        "created_at"      => "2016-03-04 03:05:04",
        "updated_at"      => "2016-03-04 04:32:25",
        "theme"           => "dark"
    ),
    array(
        "title"           => "15% Off Only this Mother's Day",
        "image"           => "mothers-day.jpg",
        "image_tablet"    => "",
        "image_mobile"    => "",
        "color"           => "#ffffff",
        "products_linked" => 0,
        "products"        => "",
        "link"            => "/mothers-day",
        "summary"         => "Treat your mom to a special shoe! She deserves it.",
        "status"          => 1,
        "sequence"        => 2,
        "created_at"      => "2016-03-04 04:00:54",
        "updated_at"      => "2016-03-04 04:45:07",
        "theme"           => "light"
    )
);

foreach ($slides as $slide) {
    $model = Mage::getModel('studioforty9_slider/slide')
        ->setData($slide)
        ->save();
}
