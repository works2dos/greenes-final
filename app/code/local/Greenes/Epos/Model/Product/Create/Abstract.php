<?php

/**
 * Class Greenes_Epos_Model_Product_Create_Abstract
 *
 * @method Mage_Catalog_Model_Product  getProduct()
 * @method setProduct($product)
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
abstract class Greenes_Epos_Model_Product_Create_Abstract extends Greenes_Epos_Model_Base
{
    /**
     * Set the ability to save products outside the admin
     */
    protected function _construct()
    {
        Mage::register('isSecureArea', true, true);
        return parent::_construct();
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    abstract public function create(array $attributes, array $data);

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_product_";
    }

    /**
     * @return bool|mixed
     */
    public function getProductId()
    {
        $product = $this->getProduct();
        if (!$product) {
            return false;
        }
        return $product->getId();
    }

    /**
     * @return array
     *
     */
    public function getDefaultProductValues()
    {
        return array(
            "website_ids"  => array(1),
            "status"       => Mage_Catalog_Model_Product_Status::STATUS_ENABLED,
            "weight"       => 1,
            "tax_class_id" => 2,
            "type_id"      => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE,
            "visibility"   => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
        );
    }

    /**
     * @return array
     *
     */
    public function getDefaultMappedKeys()
    {
        return array(
            "category"       => "category_ids",
            "subcategory"    => "category_ids",
            "subsubcategory" => "category_ids",
        );
    }

    /**
     * Sets a product attribute value
     *
     * @param       $attribute
     * @param array $data
     *
     * @return bool
     */
    public function setProductAttribute($attribute, array $data)
    {
        $value = $this->getProductAttributeValue($attribute, $data);
        if (!$value) {
            if ($this->isRequiredAttribute($attribute) || $this->isConfigurableSizeAttribute($attribute, $data)) {
                $this->logError("No value found for required attribute $attribute");
                $key = $this->getAttributeHelper()->getColumnByCode($attribute);
                if ($key) {
                    $this->logError($data[$key]);
                } else {
                    $this->logError($data);
                }
                return false;
            } else {
                $this->logWarning("No value found for attribute $attribute");
            }
        } else {
            $this->setProductData($attribute, $value);
        }

        return true;
    }

    /**
     * If Configurable Size attribute
     *
     * Need to refactor
     *
     * @param       $attribute
     * @param array $data
     *
     * @return bool
     */
    public function isConfigurableSizeAttribute($attribute, array $data)
    {
        $sizes = $this->getAttributeSetHelper()->getAllSizes();
        if (! in_array($attribute, $sizes)) {
            return false;
        }
        $attributeSetKey = $this->getAttributeHelper()->getColumnByCode("attribute_set_id");
        $key = $data[$attributeSetKey];

        if ($this->getAttributeSetHelper()->getSize($key) === $attribute) {
            return true;
        }

        return false;
    }


    /**
     * Gets the product attribute value
     *
     *
     * @param       $attribute
     * @param array $data
     *
     * @return bool|false|mixed
     */
    public function getProductAttributeValue($attribute, array $data)
    {
        $helper = $this->getAttributeHelper();
        $sourceModel = $helper->getSourceModelByCode($attribute);
        $column = $helper->getColumnByCode($attribute);

        if (!$sourceModel) {
            return (array_key_exists($column, $data)) ? $data[$column] : false;
        }

        /** @var Greenes_Epos_Model_Source_Abstract $model */
        $model = Mage::getModel($sourceModel);
        if (!$model) {
            $this->logError("Source model did not exist $sourceModel");
            return false;
        }
        $model->setRowData($data, $column);
        return $model->getValue();
    }

    /**
     * @param $attribute
     *
     * @return bool
     */
    public function isRequiredAttribute($attribute)
    {
        return $this->getAttributeHelper()->isRequiredAttribute($attribute);
    }


    /**
     * Gets Mapped Key
     *
     * @param $key
     *
     * @return bool
     */
    public function getMappedKey($key)
    {
        $mappedKeys = $this->getDefaultMappedKeys();
        if (array_key_exists($key, $mappedKeys)) {
            return $mappedKeys[$key];
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function _createProduct()
    {
        $data = $this->getProductData();
        $product = $this->getProduct();
        if (!$data || ! $product) {
            return false;
        }

        try {
            $product->save();
        } catch (Exception $e) {
            $sku = $product->getSku();
            $this->log("Could not create product $sku");
            $this->log($data);
            $this->log($e->getMessage());
            return false;
        }

        $this->setProduct($product);
        return true;
    }

    /**
     * @param string $key
     *
     * @return array|bool
     */
    public function getProductData($key = '')
    {
        $product = $this->getProduct();
        if (!$product) {
            return false;
        }
        if (!$key) {
            return $product->getData();
        }

        $data = $product->getData();

        if (array_key_exists($key, $data)) {
            return $data[$key];
        }

        return false;
    }

    /**
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function setProductData($key, $value)
    {
        $product = $this->getProduct();
        if (!$product) {
            $product = Mage::getModel("catalog/product");
        }

        $mappedKey = $this->getMappedKey($key);
        if ($mappedKey) {
            $value = $this->combineValues($value, $mappedKey);
            $key = $mappedKey;
        }

        $product->setData($key, $value);
        $this->setProduct($product);
        return $this;
    }

    /**
     * Combines values for mapped keys into an array
     *
     * @param $original
     * @param $key
     *
     * @return array
     */
    public function combineValues($original, $key)
    {
        $value = $this->getProductData($key);
        if (!$value) {
            return $original;
        }

        if (is_array($value)) {
            $value[] = $original;
            return $value;
        } else {
            return array($value, $original);
        }
    }


}