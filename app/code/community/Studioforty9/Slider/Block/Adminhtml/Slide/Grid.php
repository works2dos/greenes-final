<?php

class Studioforty9_Slider_Block_Adminhtml_Slide_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * @param array $attributes
     */
    public function __construct($attributes = array())
    {
        parent::__construct($attributes = array());

        $this->setId('sliderGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * We need to tell Magento which collection to use to display
     * the data for our grid.
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('studioforty9_slider/slide_collection');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * When a grid row is clicked, this is the URL the user should be
     * redirected to.
     *
     * @param  $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * getGridUrl()
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    /**
     * We define which columns we want to be displayed in the grid.
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header' => $this->_getHelper()->__('ID'),
            'align'  => 'right',
            'width'  => '50px',
            'index'  => 'entity_id'
        ));

        $this->addColumn('image', array(
            'header'   => $this->_getHelper()->__('Image'),
            'index'    => 'image',
            'width'    => '120px',
            'renderer' => 'Studioforty9_Slider_Block_Adminhtml_Renderer_Image'
        ));

        $this->addColumn('image_tablet', array(
            'header'   => $this->_getHelper()->__('Mobile Tablet'),
            'index'    => 'image_tablet',
            'width'    => '120px',
            'renderer' => 'Studioforty9_Slider_Block_Adminhtml_Renderer_Image'
        ));

        $this->addColumn('image_mobile', array(
            'header'   => $this->_getHelper()->__('Mobile Image'),
            'index'    => 'image_mobile',
            'width'    => '120px',
            'renderer' => 'Studioforty9_Slider_Block_Adminhtml_Renderer_Image'
        ));

        $this->addColumn('title', array(
            'header' => $this->_getHelper()->__('Title'),
            'align'  =>'left',
            'index'  => 'title'
        ));

        $this->addColumn('sequence', array(
            'header'   => $this->_getHelper()->__('Sequence'),
            'align'    => 'left',
            'index'    => 'sequence',
            'width'    => '80px',
            'type'     => 'text',
            'filter'   => false,
            'sortable' => true
        ));

        $this->addColumn('status', array(
            'header'  => $this->_getHelper()->__('Status'),
            'align'   => 'left',
            'width'   => '80px',
            'index'   => 'status',
            'type'    => 'options',
            'options' => $this->_getHelper()->getAvailableStatuses()
        ));

        $this->addColumn('action', array(
            'header'  => $this->_getHelper()->__('Action'),
            'width'   => '50px',
            'type'    => 'action',
            'actions' => array(
                array(
                    'caption' => $this->_getHelper()->__('Edit'),
                    'url'     => array('base' => '*/*/edit'),
                    'field'   => 'id'
                ),
            ),
            'index'    => 'entity_id',
            'filter'   => false,
            'sortable' => false
        ));

        return parent::_prepareColumns();
    }

    /**
     * _getHelper()
     *
     * @return Studioforty9_Slider_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('studioforty9_slider');
    }
}
