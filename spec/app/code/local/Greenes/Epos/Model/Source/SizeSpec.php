<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Size
 */
class Greenes_Epos_Model_Source_SizeSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Size');
    }

    function it_should_be_valid()
    {
        $sizeColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("mens_size");
        $categoryColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("category");
        $data = array(
            $sizeColumn => "10",
            $categoryColumn => "MEN"
        );
        $this->setRowData($data, $sizeColumn);
        $this->getValue()->shouldBeString();

        $sizeColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("ladies_size");
        $data = array(
            $sizeColumn => "8",
            $categoryColumn => "LAD"
        );
        $this->setRowData($data, $sizeColumn);
        $this->getValue()->shouldBeString();


        $sizeColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("kids_size");
        $data = array(
            $sizeColumn => "1",
            $categoryColumn => "KID"
        );
        $this->setRowData($data, $sizeColumn);
        $this->getValue()->shouldBeString();
    }

    function it_should_be_invalid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("colour");
        $data = array($column => "");
        $this->setRowData($data, $column);
        $this->getValue()->shouldBe(false);
        $this->setRowData($data);
        $this->getValue()->shouldBe(false);
    }
}
