<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Colourid
 */
class Greenes_Epos_Model_Source_ColouridSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Colourid');
    }

    function it_should_be_valid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("colour");
        $file = $this->getFile();
        $value = $file[0][0];

        $data = array($column => $value);
        $this->setRowData($data, $column);
        $this->getValue()->shouldBeInteger();
    }

}
