<?php

/**
 * Sub Category Source Model
 *
 * Class Greenes_Epos_Model_Source_Category_Leveltwo
 *
 * @extends     Greenes_Epos_Model_Source_Category
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Category_Leveltwo
    extends Greenes_Epos_Model_Source_Category
{
    use Greenes_Epos_Model_Source_Csv_Trait;

    /**
     * @param string $value
     *
     * @return false|string
     */
    public function getValue()
    {
        return parent::getValue();
    }


    /**
     * @return string
     */
    public function getFileConfigPath()
    {
        return "greenes_epos/simple/category_csv";
    }

    /**
     * @return string
     */
    public function getParentCategoryAttributeCode()
    {
        return "category";
    }

    /**
     * Gets the parent category id
     *
     * @return bool
     */
    public function getDefaultCategoryId()
    {
        $code = $this->getParentCategoryAttributeCode();
        return $this->getRowValueByAttributeCode($code);
    }
}