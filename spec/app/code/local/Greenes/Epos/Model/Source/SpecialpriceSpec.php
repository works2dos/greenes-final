<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Specialprice
 */
class Greenes_Epos_Model_Source_SpecialpriceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Specialprice');
    }

    function it_should_be_valid()
    {
        $priceColumn = \Mage::helper("greenes_epos/configurable")->getColumnByCode("price");
        $specialPriceColumn = \Mage::helper("greenes_epos/configurable")->getColumnByCode("special_price");

        $data = array(
            $priceColumn => "20.00",
            $specialPriceColumn => "10.00"
        );
        $this->setRowData($data, $specialPriceColumn);
        $this->getValue()->shouldBeString();

        $data = array(
            $priceColumn => "20.00",
            $specialPriceColumn => "20.00"
        );
        $this->setRowData($data, $specialPriceColumn);
        $this->getValue()->shouldBe(false);
    }

}
