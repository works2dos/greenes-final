<?php
/**
 * Stores Adminhtml Block: Stores Edit
 *
 * @category   Studioforty9
 * @package    Studioforty9_Pharmacy
 * @author     Copyright (c) 2014 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Block_Adminhtml_Stores_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * @var string $_blockGroup
     */
    protected $_blockGroup = 'studioforty9_stores_adminhtml';

    /**
     * @var string $_controller
     */
    protected $_controller = 'stores';

    /**
     * @var string $_mode
     */
    protected $_mode = 'edit';

    /**
     * _construct()
     *
     * @access protected
     */
    protected function _construct()
    {        
        $newOrEdit = $this->getRequest()->getParam('id')
            ? $this->__('Edit')
            : $this->__('New');

        $this->_headerText =  $newOrEdit . ' ' . $this->__('Stores');

        $this->_setupButtons();
    } 
    protected function _prepareLayout()
    {
        // Load Wysiwyg on demand and Prepare layout
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled() && ($block = $this->getLayout()->getBlock('head'))) {
            $block->setCanLoadTinyMce(true);
        }
        parent::_prepareLayout();
    } 

    /**
     * _setupButtons()
     *
     * @access protected
     * @return $this
     */
    protected function _setupButtons()
    {
        $this->_addButton('save_and_continue', array(
            'label' => $this->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);
        $this->_updateButton('save', 'label', $this->__('Save'));

        $this->_addSaveAndContinueScript();

        return $this;
    }

    /**
     * _addSaveAndContinueScript()
     *
     * @access protected
     * @return $this
     */
    protected function _addSaveAndContinueScript()
    {
        $this->_formScripts[] = <<<JS
function saveAndContinueEdit() {
    editForm.submit($('edit_form').action + 'back/edit/');
}
JS;
        return $this;
    }

    protected function _addToggleScript()
    {
        $this->_formScripts[] = <<<JS
function toggleEditor() {
    if (tinyMCE.getInstanceById('form_content') == null) {
        tinyMCE.execCommand('mceAddControl', false, 'edit_form');
    } else {
        tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
    }
}
JS;
        return $this;
    }
}
