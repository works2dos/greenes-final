<?php

class Studioforty9_Slider_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * getStatusOptions()
     *
     * @return array
     */
    public function getStatusOptions()
    {
        return Mage::getSingleton('studioforty9_slider/slide')->getStatusOptions();
    }

    /**
     * getStatusOptions()
     *
     * @return array
     */
    public function getThemeOptions()
    {
        return Mage::getSingleton('studioforty9_slider/slide')->getThemeOptions();
    }

    /**
     * getAvailableStatuses()
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return Mage::getSingleton('studioforty9_slider/slide')->getAvailableStatuses();
    }

    /**
     * getImagePath
     *
     * @param string $type
     * @return string
     */
    public function getImagePath($type)
    {
        $types = array('image', 'image_tablet', 'image_mobile');
        $type = (!in_array($type, $types)) ? 'image' : $type;

        return Mage::getBaseDir('media') . DS . 'slide' . DS . $type . DS;
    }
}
