<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Product_Create_Simple
 */
class Greenes_Epos_Model_Product_Create_SimpleSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Product_Create_Simple');
    }

    function it_should_create_simple_product()
    {
        $row = array(
            'id'         => 1,
            'T2TBarCode' => 'test',
            'ItemDesc'   => 'test description',
            'ItemRef'    => '000001',
            'ItemName'   => 'Test Product',
            'StockQty'   => 2,
            'ColCode'    => 'BLA',
            'ColName'    => 'BLACK',
            'Size'       => 37,
            'Sno'        => 03,
            'Sell1'      => 90.0000,
            'Sell2'      => 90.0000,
            'Sell3'      => 70.0000,
            'Group'      => 'LAD',
            'Stype'      => 'SAN',
            'User1'      => 'S16',
            'User2'      => 'WED',
            'Supplier'   => 'BUS',
            'Notes'      => ''
        );

        $helper = \Mage::helper("greenes_epos/attributes");
        $attributes = array_keys($helper->getAttributes());
        $this->create($attributes, $row)->shouldReturn(true);
        $this->getProductId()->shouldBeString();
    }
}
