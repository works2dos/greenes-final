<?php
/**
 * StudioForty9 Stores Source Model
 *
 * @category   StudioForty9
 * @package    StudioForty9_Stores
 * @author     Copyright (c) 2016 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Model_Source_Stores extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * getAllOptions()
     *
     * @return array
     */
    public function getAllOptions()
    {
        $stores = Mage::getModel('studioforty9_stores/store')->getCollection();

        $options = array();

        foreach ($stores as $store) {
            $options[$store->getId()] = $store->getName();
        }

        $options[''] = '-- Please choose a store --';

        return $options;
    }

    /**
     * toOptionArray()
     *
     * @return array
     */
    public function toOptionArray()
    {
        /* @var Studioforty9_Stores_Model_Resource_Store_Collection $pharmacies */
        $stores = Mage::getModel('studioforty9_stores/store')->getCollection();

        $options = array();
        if ($stores->count() > 0) {
            foreach ($stores as $store) {
                $options[$store->getId()] = $store->getName();
            }
        }

        return $options;
    }
}
