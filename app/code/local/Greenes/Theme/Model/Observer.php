<?php

class Greenes_Theme_Model_Observer
{

    /**
     * Adds admin columns to the grid
     *
     * @param Varien_Event_Observer $observer
     */
    public function addColumnsToProductGrid(Varien_Event_Observer $observer)
    {
        /** @var Mage_Adminhtml_Block_Catalog_Product_Grid $block */
        $block = $observer->getBlock();
        if (!isset($block)) {
            return;
        }
        if ($block->getType() == 'adminhtml/catalog_product_grid') {

            $block->addColumnAfter(
                'image',
                array(
                    'header' => Mage::helper('catalog')->__('Image'),
                    'align'  => 'left',
                    'index'  => 'image',
                    'width'  => '70',
                    'renderer' => 'Greenes_Theme_Block_Adminhtml_Renderer_Image'
                ),
                'sku'
            );

            $block->addColumnAfter(
                'colour',
                array(
                    'header'  => Mage::helper('catalog')->__('Colour'),
                    'width'   => '100px',
                    'type'    => 'options',
                    'index'   => 'colour',
                    'options' => Mage::helper('greenes_theme')->getAttributeOptions('colour')
                ),
                'image'
            );

            $block->addColumnAfter(
                'ladies_size',
                array(
                    'header'  => Mage::helper('catalog')->__('Ladies Size'),
                    'width'   => '100px',
                    'type'    => 'options',
                    'index'   => 'ladies_size',
                    'options' => Mage::helper('greenes_theme')->getAttributeOptions('ladies_size')
                ),
                'colour'
            );
            $block->addColumnAfter(
                'mens_size',
                array(
                    'header'  => Mage::helper('catalog')->__('Mens Size'),
                    'width'   => '100px',
                    'type'    => 'options',
                    'index'   => 'mens_size',
                    'options' => Mage::helper('greenes_theme')->getAttributeOptions('mens_size')
                ),
                'ladies_size'
            );
            $block->addColumnAfter(
                'kids_size',
                array(
                    'header'  => Mage::helper('catalog')->__('Kids Size'),
                    'width'   => '100px',
                    'type'    => 'options',
                    'index'   => 'kids_size',
                    'options' => Mage::helper('greenes_theme')->getAttributeOptions('kids_size')
                ),
                'mens_size'
            );
            $block->addColumnAfter(
                'unisex_size',
                array(
                    'header'  => Mage::helper('catalog')->__('Unisex Size'),
                    'width'   => '100px',
                    'type'    => 'options',
                    'index'   => 'kids_size',
                    'options' => Mage::helper('greenes_theme')->getAttributeOptions('unisex_size')
                ),
                'kids_size'
            );
        }
    }

    /**
     * Adds admin columns to the grid
     *
     * @param Varien_Event_Observer $observer
     */
    public function onEavLoadBefore(Varien_Event_Observer $observer)
    {
        $collection = $observer->getCollection();
        if (!isset($collection)) {
            return;
        }

        if (is_a($collection, 'Mage_Catalog_Model_Resource_Product_Collection')) {

            /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
            $collection->addAttributeToSelect(array('colour', 'ladies_size', 'mens_size', 'kids_size', "unisex_size"));
        }
    }


    /**
     * Add a handle for an empty cart.
     *
     * @param  Varien_Event_Observer $observer
     *
     * @return Varien_Event_Observer
     */
    public function addHandle(Varien_Event_Observer $observer)
    {
        if ($this->isCartEmpty($observer)) {
            /** @var $layout Mage_Core_Model_Layout */
            $observer->getEvent()
                ->getLayout()
                ->getUpdate()
                ->addHandle('checkout_cart_empty');
        }

        return $observer;
    }

    /**
     * Check if the current request is for the cart index page and if the cart is empty.
     *
     * @param  Varien_Event_Observer $observer
     *
     * @return bool
     */
    private function isCartEmpty(Varien_Event_Observer $observer)
    {
        return $this->isCartPage($observer->getEvent()->getAction()) && 0 === $this->getCartItemsQty();
    }

    /**
     * Get the number of items in the cart.
     *
     * @return int
     */
    private function getCartItemsQty()
    {
        return (int)Mage::getModel('checkout/cart')->getQuote()->getItemsQty();
    }

    /**
     * Is the current request for the cart index page.
     *
     * @param Mage_Core_Controller_Front_Action $action
     *
     * @return bool
     */
    private function isCartPage(Mage_Core_Controller_Front_Action $action)
    {
        return 'checkout_cart_index' === $action->getFullActionName();
    }
}
