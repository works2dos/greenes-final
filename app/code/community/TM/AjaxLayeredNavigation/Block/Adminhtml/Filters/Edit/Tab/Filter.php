<?php

class TM_AjaxLayeredNavigation_Block_Adminhtml_Filters_Edit_Tab_Filter
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        /** @var $model TM_AjaxLayeredNavigation_Model_Filters */
        $model = Mage::registry('ajaxlayerednavigation_filters');
        /*
         * Checking if user have permissions to save information
         */
        if ($this->_isAllowedAction('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('filters_fieldset', array('legend'=>Mage::helper('ajaxlayerednavigation')->__('Attribute Information'),'class'=>'fieldset'));

        $fieldset->addField('display_type', 'select', array(
            'name'      => 'display_type',
            'label'     => Mage::helper('ajaxlayerednavigation')->__('Display Type'),
            'title'     => Mage::helper('ajaxlayerednavigation')->__('Display Type'),
            'values'	=> Mage::helper('ajaxlayerednavigation')->getFiltersDisplayValues()
        ));

        // $fieldset->addField('show_quantities', 'select', array(
            // 'name'      => 'show_quantities',
            // 'label'     => Mage::helper('ajaxlayerednavigation')->__('Show Quantities'),
            // 'title'     => Mage::helper('ajaxlayerednavigation')->__('Show Quantities'),
            // 'values'	=> Mage::helper('ajaxlayerednavigation')->getFiltersYesNoOptions()
        // ));

        $fieldset->addField('sort', 'select', array(
            'name'      => 'sort',
            'label'     => Mage::helper('ajaxlayerednavigation')->__('Sort By'),
            'title'     => Mage::helper('ajaxlayerednavigation')->__('Sort By'),
            'values'	=> Mage::helper('ajaxlayerednavigation')->getFiltersSortOptions()
        ));
        
        $fieldset->addField('order', 'select', array(
            'name'      => 'order',
            'label'     => Mage::helper('ajaxlayerednavigation')->__('Order By'),
            'title'     => Mage::helper('ajaxlayerednavigation')->__('Order By'),
            'values'    => Mage::helper('ajaxlayerednavigation')->getFiltersSortOrder()
        ));

        // $fieldset->addField('show_list', 'select', array(
            // 'name'      => 'show_list',
            // 'label'     => Mage::helper('ajaxlayerednavigation')->__('Show On List'),
            // 'title'     => Mage::helper('ajaxlayerednavigation')->__('Show On List'),
            // 'values'	=> Mage::helper('ajaxlayerednavigation')->getFiltersYesNoOptions()
        // ));

        // $fieldset->addField('show_product', 'select', array(
            // 'name'      => 'show_product',
            // 'label'     => Mage::helper('ajaxlayerednavigation')->__('Show On Product Page'),
            // 'title'     => Mage::helper('ajaxlayerednavigation')->__('Show On Product Page'),
            // 'values'	=> Mage::helper('ajaxlayerednavigation')->getFiltersYesNoOptions()
        // ));

        $fieldset->addField('f_position', 'text', array(
            'name'      => 'f_position',
            'label'     => Mage::helper('ajaxlayerednavigation')->__('Position'),
            'title'     => Mage::helper('ajaxlayerednavigation')->__('Position')
        ));

        $fieldset->addField('filters_id', 'hidden', array(
            'name'      => 'filters_id',
        ));

        $form->setValues($model->getData());
        $this->setForm($form);

//        Mage::dispatchEvent('adminhtml_cms_page_edit_tab_content_prepare_form', array('form' => $form));

        return parent::_prepareForm();
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('adminhtml/filters/' . $action);
    }
}
