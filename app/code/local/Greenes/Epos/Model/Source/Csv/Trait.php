<?php

/**
 * Trait for getting CSV value
 *
 * Class Greenes_Epos_Model_Source_Csv_Trait
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
trait Greenes_Epos_Model_Source_Csv_Trait
{
    /**
     * Config path of the CSV file
     *
     * @return bool
     */
    public function getFileConfigPath()
    {
        return false;
    }

    /**
     * @return false|string
     */
    public function getValue()
    {
        return $this->getCsvValue($this->getColumnValue());
    }

    /**
     * @param $value
     *
     * @return string|false
     */
    public function getCsvValue($value)
    {
        $file = $this->getFile();
        if (!$file) {
            return false;
        }
        return $this->getMappedValue($file, $value);
    }

    /**
     * @return array|bool
     */
    public function getFile()
    {
        $config = Mage::getStoreConfig($this->getFileConfigPath());
        if (!$config) {
            return false;
        }

        $file = $this->getFileBasePath() . $config;
        if (!file_exists($file)) {
            return false;
        }
        $csv = new Varien_File_Csv();

        try {
            $data = $csv->getData($file);
        } catch (Exception $e) {
            Mage::logException($e);
            return false;
        }
        if (empty($data)) {
            return false;
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getFileBasePath()
    {
        return Mage::getBaseDir("media") . DS . "epos" . DS;
    }

    /**
     * @param array $data
     * @param       $value
     *
     * @return bool
     */
    public function getMappedValue(array $data, $value)
    {
        $value = strtolower(trim($value));
        if (!$value) {
            return false;
        }

        foreach ($data as $i => $row) {

            $code = trim($row[0]);
            $code = strtolower($code);
            if ($code === $value) {
                return $row[1];
            }

        }
        return false;
    }
}