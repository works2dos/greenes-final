<?php

$this->startSetup();

$table = $this->getConnection();
$table->addColumn($this->getTable('studioforty9_slider/slide'), 'theme', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => false,
    'length'    => 5,
    'default'   => 'dark',
    'comment'   => 'Theme',
));

$this->endSetup();
