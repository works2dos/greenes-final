<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Category_Levelthree
 */
class Greenes_Epos_Model_Source_Category_LevelthreeSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Category_Levelthree');
    }

    function it_should_be_valid()
    {
        $subColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("subsubcategory");
        $parentColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("subcategory");
        $rootColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("category");
        $sub = "WED";
        $parent = "SAN";
        $root = "LAD";

        $data = array(
            $subColumn => $sub,
            $parentColumn => $parent,
            $rootColumn => $root,
        );
        $this->setRowData($data, $subColumn);
        $this->getValue()->shouldBeInteger();
    }

    function it_should_be_invalid()
    {
        $subColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("subcategory");
        $sub = "WED";

        $data = array(
            $subColumn => $sub,
        );
        $this->setRowData($data);
        $this->getValue()->shouldBe(false);
    }

}
