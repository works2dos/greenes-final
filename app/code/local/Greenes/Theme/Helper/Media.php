<?php

/**
 * Class Greenes_Theme_Helper_Media
 *
 * @extends     Mage_Core_Helper_Abstract
 * @category    Greenes
 * @package     Greenes_Theme
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Theme_Helper_Media extends Mage_Core_Helper_Abstract
{

    /**
     * @return Varien_Db_Adapter_Interface
     */
    public function getConnection()
    {
        return Mage::getModel("core/resource")->getConnection("core_write");
    }

    /**
     * @param $productId
     *
     * @return array
     */
    public function getMediaByProductId($productId)
    {
        $query
            = "
            SELECT m.value_id, attribute_id, value, store_id, label, position, disabled
            FROM catalog_product_entity_media_gallery as m
            INNER JOIN catalog_product_entity_media_gallery_value as v ON m.value_id = v.value_id
            WHERE entity_id = $productId";
        $connection = $this->getConnection();

        return $connection->fetchAll($query);
    }

    /**
     * Insert existing images into the media gallery tables
     *
     *
     * @param array $media
     * @param       $productId
     *
     * @return bool|int
     */
    public function insertMediaGallery(array $media, $productId)
    {

        if (empty($media)) {
            return false;
        }

        /** @var Mage_Catalog_Model_Resource_Product_Attribute_Backend_Media $model */
        $model = Mage::getSingleton("catalog/resource_product_attribute_backend_media");
        $connection = $this->getConnection();
        $values = array();
        foreach ($media as $id => $row) {
            $data = array(
                'attribute_id' => $row['attribute_id'],
                'entity_id'    => $productId,
                'value'        => $row['value']
            );

            $id = $model->insertGallery($data);

            $values[] = array(
                "value_id" => $id,
                "store_id" => $row['store_id'],
                "label"    => $row['label'],
                "position" => $row['position'],
                "disabled" => $row['disabled']
            );
        }

        return $connection->insertMultiple("catalog_product_entity_media_gallery_value", $values);
    }
}