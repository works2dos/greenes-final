<?php

$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();

$attributeSetId = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getDefaultAttributeSetId();
$installer->addAttributeGroup('catalog_category', $attributeSetId, 'Promotions');

$installer->addAttribute('catalog_category', 'is_promoted_on_homepage', array(
    'type'         => 'int',
    'label'        => 'Promote on the Homepage',
    'input'        => 'select',
    'default'      => 0,
    'source'       => 'greenes_promotions/mode_source',
    'sort_order'   => 18,
    'group'        => 'Promotions',
    'required'     => true,
    'visible'      => true,
    'global'       => true,
    'user_defined' => true,
    'filterable'   => false,
    'filterable_in_search' => false,
));

$installer->addAttribute('catalog_category', 'promotion_homepage_notes', array(
    'type'         => 'varchar',
    'label'        => 'Promotion Notes',
    'input'        => 'text',
    'default'      => '',
    'sort_order'   => 19,
    'group'        => 'Promotions',
    'required'     => false,
    'visible'      => true,
    'global'       => true,
    'user_defined' => true,
    'filterable'   => false,
    'filterable_in_search' => false,
));

$installer->addAttribute('catalog_category', 'promotion_homepage_button', array(
    'type'         => 'varchar',
    'label'        => 'Promotion Button Text',
    'input'        => 'text',
    'default'      => 'Shop Now',
    'sort_order'   => 20,
    'group'        => 'Promotions',
    'required'     => false,
    'visible'      => true,
    'global'       => true,
    'user_defined' => true,
    'filterable'   => false,
    'filterable_in_search' => false,
));

$installer->addAttribute('catalog_category', 'promotion_homepage_image', array(
    'type'              => 'varchar',
    'label'             => 'Promotion Image',
    'input'             => 'image',
    'backend'           => 'catalog/category_attribute_backend_image',
    'sort_order'        => 20,
    'group'             => 'Promotions',
    'required'          => false,
    'visible'           => true,
    'global'            => true,
    'user_defined'      => true,
    'visible_on_front'  => true,
    'filterable'        => false,
    'filterable_in_search' => false,
));

$installer->endSetup();
