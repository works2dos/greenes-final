<?php
/**
 * Stores Adminhtml Block: Store Edit Form
 *
 * @category   Studioforty9
 * @package    Studioforty9_Stores
 * @author     Copyright (c) 2014 StudioForty9 <info@studioforty9.com>
 */
class Studioforty9_Stores_Block_Adminhtml_Stores_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'     => 'edit_form',
            'action' => $this->getUrl(
                '*/*/edit', array(
                    '_current' => true,
                    'continue' => 0,
                )
            ),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $form->setUseContainer(true);
        $this->_prepareGeneralFields($form)
            ->_prepareLocationFields($form)
            ->_prepareOpeningHoursFields($form);
        $this->setForm($form);

        return $this;
    }

    /**
     * _addFieldsToFieldset()
     *
     * @param Varien_Data_Form_Element_Fieldset $fieldset
     * @param $fields
     * @return $this
     * @access protected
     */
    protected function _addFieldsToFieldset(Varien_Data_Form_Element_Fieldset $fieldset, $fields)
    {
        $post = new Varien_Object($this->getRequest()->getPost('store'));
        foreach ($fields as $name => $_data) {
            $_data['name'] = "store[$name]";
            $_data['title'] = $_data['label'];
            $_data['value'] = ($post->hasData($name))
                ? $post->getData($name)
                : $this->_getStore()->getData($name);
            $fieldset->addField($name, $_data['input'], $_data);
        }

        return $this;
    }

    /**
     * getSaveAndContinueUrl()
     *
     * @return string
     */
    public function getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
        ));
    }

    /**
     * _getStore()
     *
     * @return null|Studioforty9_Pharmacy_Model_Pharmacy
     * @access protected
     */
    protected function _getStore()
    {
        if (!$this->hasData('store')) {
            $store = Mage::registry('current_store');
            if (!$store instanceof Studioforty9_Stores_Model_Store) {
                $store = Mage::getModel('studioforty9_stores/store');
            }
            $this->setData('store', $store);
        }

        return $this->getData('store');
    }

    /**
     * _prepareGeneralFields()
     *
     * @param  Varien_Data_Form $form
     * @return $this
     */
    protected function _prepareGeneralFields(Varien_Data_Form $form, $fields = array())
    {
        $fieldset = $form->addFieldset('general', array(
            'legend' => $this->__('Store Details'),
            'class' => 'half-width'
        ));

        $fields['name'] = array(
            'label' => $this->__('Store Name'),
            'input' => 'text',
            'required' => true,
        );
        $fields['manager'] = array(
            'label' => $this->__('Manager'),
            'input' => 'text',
            'required' => false,
        );
        $fields['email'] = array(
            'label' => $this->__('Email Address'),
            'input' => 'text',
            'required' => false,
            'class' => 'validate-email'
        );
        $fields['telephone'] = array(
            'label' => $this->__('Telephone'),
            'input' => 'text',
            'required' => true,
            'class' => 'validate-phone'
        );
        /*$fields['store_id'] = array(
            'required' => false,
            'label' => $this->__('Store View'),
            'title' => $this->__('Store View'),
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $fields['store_id']['input'] = 'multiselect';
            $fields['store_id']['values'] = Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true);
        } else {
            $fields['store_id']['input'] = 'hidden';
            $fields['store_id']['value'] = '1';
        }*/

        $this->_addFieldsToFieldset($fieldset, $fields);

        return $this;
    }

    /**
     * _prepareLocationFields()
     *
     * @param  Varien_Data_Form $form
     * @return $this
     */
    public function _prepareLocationFields(Varien_Data_Form $form)
    {
        $configSettings = Mage::getSingleton('cms/wysiwyg_config')->getConfig(array(
            'add_widgets' => false,
            'add_variables' => false,
            'add_images' => false,
            'files_browser_window_url' => $this->getBaseUrl().'admin/cms_wysiwyg_images/index/')
        );
        
        $countries = Mage::getModel('adminhtml/system_config_source_country')->toOptionArray();
        $fieldset = $form->addFieldset('location', array(
            'legend' => $this->__('Location Details'),
            'class' => 'half-width'
        ));                                                                                     
        $this->_addFieldsToFieldset($fieldset, array(
            'street' => array(
                'label' => $this->__('Street'),
                'input' => 'text',
                'required' => true
            ),
            'region' => array(
                'label' => $this->__('Region'),
                'input' => 'text',
                'required' => true
            ),
            'city' => array(
                'label' => $this->__('City'),
                'input' => 'text',
                'required' => true
            ),
            'post_code' => array(
                'label' => $this->__('Post Code'),
                'input' => 'text',
                'required' => false
            ),
            'country' => array(
                'label' => $this->__('Country'),
                'input' => 'select',
                'required' => true,
                'values' => $countries,
            ),
            'latitude' => array(
                'label' => $this->__('Latitude'),
                'input' => 'text',
                'required' => false
            ),
            'longitude' => array(
                'label' => $this->__('Longitude'),
                'input' => 'text',
                'required' => false
            ),
            'content' => array(
                'label' => $this->__('Content'),
                'title' => $this->__('Content'),
                'input' => 'editor',
                'style' => 'width:400px; height:400px;',
                'config' => $configSettings,
                'wysiwyg' => true,
                'required' => false
            )  
        ));

        return $this;
    }

    /**
     * _prepareOpeningHoursFields()
     *
     * @param  Varien_Data_Form $form
     * @return $this
     */
    public function _prepareOpeningHoursFields(Varien_Data_Form $form)
    {
        $fieldset = $form->addFieldset('opening_hours', array(
            'legend' => $this->__('Opening Hours'),
            'class' => 'half-width'
        ));

        $this->_addFieldsToFieldset($fieldset, array(
            'hours' => array(
                'name' => 'hours',
                'input' => 'text',
                'label' => $this->__('Opening Hours'),
                'title' => $this->__('Opening Hours'),
                'required' => false
            )
        ));

        $fieldset->getElements()->searchById('hours')->setRenderer(
            $this->getLayout()->createBlock('studioforty9_stores_adminhtml/renderer_hours')
        );

        return $this;
    }
}
