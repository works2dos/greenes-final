<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

$entityCode = Mage_Catalog_Model_Product::ENTITY;
$this->addAttribute(
    $entityCode,
    "season",
    array(
        'group'                    => 'Greenes',
        'type'                     => 'varchar',
        'input'                    => 'select',
        'label'                    => "Season",
        'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'                  => 1,
        'required'                 => 0,
        'visible_on_front'         => 0,
        'frontend_class'           => '',
        'is_html_allowed_on_front' => 0,
        'is_configurable'          => 1,
        'searchable'               => 0,
        'filterable'               => 1,
        'comparable'               => 1,
        'unique'                   => 0,
        'used_in_product_listing'  => 0,
        'used_for_promo_rules'     => 1,
        'position'                 => 20,
        'option'                   => array(
            'values' => array(
                'Spring/Summer',
                'Continunity',
            )
        ),
    )
);

$this->endSetup();