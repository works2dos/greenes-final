<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Category_Leveltwo
 */
class Greenes_Epos_Model_Source_Category_LeveltwoSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Category_Leveltwo');
    }

    function it_should_be_valid()
    {
        $subColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("subcategory");
        $parentColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("category");
        $sub = "SAN";
        $parent = "LAD";

        $data = array(
            $subColumn => $sub,
            $parentColumn => $parent,
        );
        $this->setRowData($data, $subColumn);
        $this->getValue()->shouldBeInteger();
    }

    function it_should_be_invalid()
    {
        $subColumn = \Mage::helper("greenes_epos/attributes")->getColumnByCode("subcategory");
        $sub = "SAN";

        $data = array(
            $subColumn => $sub,
        );
        $this->setRowData($data);
        $this->getValue()->shouldBe(false);
    }

}
