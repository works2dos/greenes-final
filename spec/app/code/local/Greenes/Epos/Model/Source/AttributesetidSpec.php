<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Attributesetid
 */
class Greenes_Epos_Model_Source_AttributesetidSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Attributesetid');
    }

    function it_should_be_valid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("attribute_set_id");
        $data = array($column => "LAD");
        $this->setRowData($data, $column);
        $this->getValue()->shouldReturn(9);

        $data = array($column => "MEN");
        $this->setRowData($data, $column);
        $this->getValue()->shouldReturn(10);

        $data = array($column => "KID");
        $this->setRowData($data, $column);
        $this->getValue()->shouldReturn(11);
    }

    function it_should_be_be_invalid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("attribute_set_id");
        $data = array($column => "test");
        $this->setRowData($data, $column);
        $this->getValue()->shouldReturn(false);

        $data = array("test" => "MEN");
        $this->setRowData($data, $column);
        $this->getValue()->shouldReturn(false);

        $data = array("test" => "MEN");
        $this->setRowData($data);
        $this->getValue()->shouldReturn(false);
    }
}
