<?php

/**
 * Class Greenes_Epos_Model_Product_Configurable_Create
 *
 * @extends     Mage_Core_Model_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Configurable_Create extends Greenes_Epos_Model_Base
{
    /**
     * Set are as admin and clear the config
     */
    protected function _construct()
    {
        /**
         * Set the current store as admin to create the products
         */
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        /**
         * Clear the config cache
         */
        Mage::app()->getCacheInstance()->cleanType('config');

        return parent::_construct();
    }

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_configurable_";
    }

    /**
     * Creates configurable products
     *
     * @return bool
     */
    public function createConfigurableProducts()
    {
        ini_set("memory_limit", "1024M");

        /**
         * Get Products
         */
        $products = $this->getProducts();
        if (!$products) {
            $this->logWarning("No configurable products found for importing.");
            return true;
        }

        $products = $this->_createProducts($products);
        if (!$products) {
            $this->logError("No configurable products created.");
            return false;
        }

        if (!$this->relateProducts($products)) {
            $this->logWarning("Could not relate products");
        }

        return true;
    }

    /**
     * Groups Products by SKU and Colour
     *
     * @return array|false
     */
    public function getProducts()
    {
        return Mage::getModel("greenes_epos/product_configurable_get")->getProducts();
    }


    /**
     * Creates the configurable product
     * 
     * @param array $products
     *
     * @return false|Mage_Catalog_Model_Product
     */
    protected function _createProducts(array $products)
    {
        $attributes = $this->getAttributes();
        if (!$attributes) {
            $this->logError("No attributes to create configurable product");
            return false;
        }
        
        $createdProducts = array();
        foreach ($products as $sku => $simpleProducts)
        {
            $data = Mage::getModel("greenes_epos/product_configurable_generate")
                ->getProductData($sku, $simpleProducts);

            $configurable = Mage::getModel('greenes_epos/product_create_configurable');
            if (!$configurable->create($attributes, $data)) {
                $this->logError("Could not create configurable product $sku");
                continue;
            }

            $createdProducts[] = $configurable->getProduct();
            $itemIds = array();
            foreach ($simpleProducts as $row) {
                $itemIds[] = $row['epos_id'];
            }
            try {
                $this->deleteItems($itemIds);
            } catch (Exception $e) {
                $this->logError("Could not delete items");
                $this->logError($itemIds);
            }
        }

        if (empty($createdProducts)) {
            $this->logError("Could not create any configurable products");
            return false;
        }

        return $createdProducts;
    }

    /**
     * @return false|array
     */
    public function getAttributes()
    {
        $attributes = Mage::helper("greenes_epos/configurable")->getAttributes();
        if (!$attributes) {
            return false;
        }
        return array_keys($attributes);
    }

    /**
     * @param array $products
     *
     * @return bool
     */
    public function relateProducts(array $products)
    {
        return Mage::getModel("greenes_epos/product_configurable_relate")->relate($products);
    }

}
