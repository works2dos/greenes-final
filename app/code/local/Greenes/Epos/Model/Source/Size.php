<?php

/**
 * Size Source Model
 *
 * Class Greenes_Epos_Model_Source_Size
 *
 * @extends     Greenes_Epos_Model_Source_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Source_Size
    extends Greenes_Epos_Model_Source_Abstract
{
    use Greenes_Epos_Model_Source_Csv_Trait;

    /**
     * @param string $value
     *
     * @return false|string
     */
    public function getValue()
    {
        $value = $this->getColumnValue();
        if (!$value) {
            return false;
        }

        $category = $this->getRawAttributeValue("category");
        if (!$category) {
            return false;
        }

        return $this->getSizeByCategory($value, $category);
    }

    /**
     * @param $value
     * @param $category
     *
     * @return bool|false|string
     */
    public function getSizeByCategory($value, $category)
    {
        switch ($category) {
            case "MEN":
                $this->setFileConfigPath("greenes_epos/simple/mens_sizes_csv");
                break;

            case "LAD":
                $this->setFileConfigPath("greenes_epos/simple/ladies_sizes_csv");
                break;

            case "KID":
                $this->setFileConfigPath("greenes_epos/simple/kids_sizes_csv");
                break;

            case "UNI":
                $this->setFileConfigPath("greenes_epos/simple/unisex_sizes_csv");
                break;

            default:
                return false;
                break;
        }

        return $this->getCsvValue($value);
    }

    /**
     * Config path of the CSV file
     *
     * @return bool
     */
    public function getFileConfigPath()
    {
        return $this->getData("file_config_path");
    }

}