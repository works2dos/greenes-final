<?php

/**
 * Update Stock Levels
 *
 * Class Greenes_Epos_Model_Stock_Update
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Stock_Update extends Greenes_Epos_Model_Base
{

    /**
     * @return string
     */
    public function getLogPrefix()
    {
        return "greenes_stock_";
    }

    /**
     * @return bool
     */
    public function update()
    {
        ini_set("memory_limit", "1024M");

        if (!$this->hasValidCredentials()) {
            $this->log("Does not have valid credentials to import stock levels");
            return false;
        }

        $stockItems = $this->getStockItems();
        if (! is_array($stockItems)) {
            $this->log("No items to update", self::WARNING);
            return true;
        }
        if (empty($stockItems)) {
            $this->log("No items to update", self::WARNING);
            return true;
        }

        $this->_setOutOfStockItems();
        $transaction = $this->getStockTransaction($stockItems);
        try {
            $transaction->save();
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return false;
        }

        $items = array();
        foreach ($stockItems as $row) {
            $items[] = $row["row_id"];
        }

        $this->flushCache();

        return $this->deleteItems($items);
    }

    /**
     * @return bool
     */
    public function hasValidCredentials()
    {
        $stockField = $this->getStockField();
        $skuField = $this->getSkuField();

        if (!$stockField || !$skuField) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getStockItems()
    {
        $productSkus = $this->_getProductSkus();
        $stockField = $this->getStockField();
        $skuField = $this->getSkuField();
        $productSkus = implode('","', $productSkus);
        $productSkus = trim($productSkus, '"');
        $productSkus = '"' . $productSkus . '"';

        $query = "SELECT
            epos.id as row_id,
            epos.$stockField as stock,
            epos.$skuField as sku,
            (SELECT e.entity_id FROM catalog_product_entity as e WHERE e.sku = epos.$skuField LIMIT 1) as product_id
        FROM epos_import AS epos
        WHERE (epos.$skuField IN($productSkus))";

        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
        $connection = $resource->getConnection("core_write");
        return $connection->fetchAll($query);
    }

    /**
     * @return array
     */
    protected function _getProductSkus()
    {
        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        /** @var Magento_Db_Adapter_Pdo_Mysql $connection */
        $connection = $resource->getConnection("core_write");
        $query = "SELECT sku from catalog_product_entity";
        return $connection->fetchCol($query);
    }

    /**
     * @param array $stockItems
     *
     * @return Mage_Core_Model_Resource_Transaction
     */
    public function getStockTransaction(array $stockItems)
    {
        $transaction = Mage::getModel('core/resource_transaction');
        foreach ($stockItems as $id => $row) {

            $stockLevel = $row['stock'];
            $isInStock = $this->isInStock($stockLevel);
            $productId = $row["product_id"];

            /** @var Mage_CatalogInventory_Model_Stock_Item $transaction */
            $stockItem = Mage::getModel('cataloginventory/stock_item')->load($productId, "product_id");
            if ($stockItem) {
                $stockItem->setData('qty', $stockLevel);
                $stockItem->setIsInStock($isInStock);
                $transaction->addObject($stockItem);
            } else {
                $product = Mage::getModel('catalog/product')->load($productId);
                $product->setStockData(
                    array(
                        'qty'                     => $stockLevel,
                        'use_config_manage_stock' => 0,
                        'manage_stock'            => 1,
                        'is_in_stock'             => $isInStock,

                    )
                );
                $transaction->addObject($product);
            }
        }

        return $transaction;
    }

    /**
     * @param $stockLevel
     *
     * @return bool
     */
    public function isInStock($stockLevel)
    {
        if (!$stockLevel) {
            return false;
        }
        $stockLevel = (int)$stockLevel;
        return ($stockLevel === 0) ? false : true;
    }

    /**
     * @return bool
     */
    protected function _setOutOfStockItems()
    {
        return Mage::getModel("greenes_epos/stock_outofstock")->update();
    }

    /**
     * @return bool
     */
    public function flushCache()
    {
        return Mage::app()->getCacheInstance()->flush();
    }

}