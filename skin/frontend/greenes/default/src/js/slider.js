jQuery.noConflict();
;(function($) {
  'use strict';

  /**
   * Switch the banner images depending on window size
   *
   * @param {Object} elements
   * @param {Object} options
   */
  function switchBanners(elements, options) {
    if (!elements.length) return;
    // Loop through the images and replace their src depending on window size
    elements.each(function (idx, image) {
      var imageWidth, winWidth = parseInt($(window).width(), 10);
      var $image = $(image);
      if (winWidth <= options.resize.sm) {
        imageWidth = options.widths.sm;
        $image.attr('src', $image.data(options.data.sm));
      } else if (winWidth >= options.resize.sm && winWidth < options.resize.lg) {
        imageWidth = options.widths.md;
        $image.attr('src', $image.data(options.data.md));
      } else {
        imageWidth = options.widths.lg;
        $image.attr('src', $image.data(options.data.lg));
      }

      if (!$('html').hasClass('csstransforms')) {
        $image.css({left: -(imageWidth - winWidth) / 2});
      }
    });
  }

  // PLUGIN DEFINITION
  $.fn.slider = function (settings) {
    if (!this.length) {
      return this;
    }
    var options = $.extend({
      'widths': {'sm': 770, 'md': 979, 'lg': 1200},
      'images': 'img.banner__slide-photo',
      'resize': {'sm': 770, 'md': 979, 'lg': 1200},
      'data': { 'sm': 'src-sm', 'md': 'src-md', 'lg': 'src-lg' }
    }, settings);

    return this.each(function () {
      var $this = $(this);
      switchBanners($this.find('img.banner__slide-photo'), options);
      $(window).on('resize', function () {
        switchBanners($('img.banner__slide-photo'), options);
      });
      var $carousel = $('#banner-carousel');
      $carousel.hammer().on('swipeleft', function() {
        $(this).carousel('next');
      });
      $carousel.hammer().on('swiperight', function() {
        $(this).carousel('prev');
      });
      $carousel.on('slid.bs.carousel', function (evt) {
        var active = $carousel.find('.item.active');
        var theme = active.data('theme');
        var alt = theme === 'dark' ? 'light' : 'dark';
        var targets = $carousel.find('.carousel-indicators, .carousel-control');
        targets.each(function() {
          var $this = $(this);
          $this.removeClass(alt).addClass(theme);
        });
      })
    });
  };
})(jQuery)
