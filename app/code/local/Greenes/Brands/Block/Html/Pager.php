<?php

/**
 * Class Greenes_Brands_Block_Html_Pager
 *
 * @extends     Greenes_Brands_Block_Pager
 * @category    Greenes
 * @package     Greenes_Brands
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Brands_Block_Html_Pager extends Mage_Page_Block_Html_Pager
{
    public function getPagerUrl($params=array())
    {
        $url = parent::getPagerUrl($params);
        $url = str_replace('index/list/url_key/', '', $url);
        return $url;
    }
}