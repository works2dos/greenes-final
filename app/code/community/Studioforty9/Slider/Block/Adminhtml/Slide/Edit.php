<?php
class Studioforty9_Slider_Block_Adminhtml_Slide_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * The mode tells Magento which folder to use to locate the
     * related form blocks to be displayed within this form container.
     * @var string $_mode
     */
    protected $_mode = 'edit';

    /**
     * The controller tells Magento which controller to use.
     * @var string $_controller
     */
    protected $_controller = 'slide';

    /**
     * The blockGroup tells magento where to find adminhtml blocks.
     * @var string $_blockGroup
     */
    protected $_blockGroup = 'studioforty9_slider_adminhtml';

    /**
     * The constructor will just dynamically set the heading for the
     * type of action we want to make. New or Edit.
     */
    protected function _construct()
    {
        $newOrEdit = $this->getRequest()->getParam('id')
            ? $this->__('Edit') 
            : $this->__('New');
        $this->_headerText =  $newOrEdit . ' ' . $this->__('Slide');
    }
}
