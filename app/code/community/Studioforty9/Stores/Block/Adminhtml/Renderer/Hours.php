<?php

class Studioforty9_Stores_Block_Adminhtml_Renderer_Hours extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element
{
    /**
     * Set the template on object construction.
     */
    protected function _construct()
    {
        $this->setTemplate('studioforty9/stores/hours.phtml');
    }

    /**
     * Get the open/closed values for the given day in the value array.
     *
     * @param array  $value
     * @param int    $day
     * @param string $key
     *
     * @return string
     */
    protected function getValueForDay($value, $day, $key)
    {
        $day = $value[$day];
        if (empty($day)) {
            return '';
        }

        return $day['times'][$key];
    }

    /**
     * Get the open value.
     *
     * @param array $value
     * @param int   $day
     *
     * @return string
     */
    public function getOpenValue($value, $day)
    {
        return $this->getValueForDay($value, $day, 'open');
    }

    /**
     * Get the closed value.
     *
     * @param array $value
     * @param int   $day
     *
     * @return string
     */
    public function getClosedValue($value, $day)
    {
        return $this->getValueForDay($value, $day, 'close');
    }
}
