<?php

namespace spec;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * @mixin \Greenes_Epos_Model_Source_Category
 */
class Greenes_Epos_Model_Source_CategorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        \Mage::app();
        $this->shouldHaveType('Greenes_Epos_Model_Source_Category');
    }

    function it_should_be_valid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("category");
        $value = "LAD";

        $data = array($column => $value);
        $this->setRowData($data, $column);
        $this->getValue()->shouldBeInteger();

    }

    function it_should_be_invalid()
    {
        $column = \Mage::helper("greenes_epos/attributes")->getColumnByCode("category");

        $subCategory = "SAN";
        $data = array($column => $subCategory);
        $this->setRowData($data, $column);
        $this->getValue()->shouldBe(false);

        $data = array($column => "");
        $this->setRowData($data, $column);
        $this->getValue()->shouldBe(false);


        $this->setRowData($data);
        $this->getValue()->shouldBe(false);
    }
}
