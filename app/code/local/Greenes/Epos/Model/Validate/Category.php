<?php

/**
 * Class Greenes_Epos_Model_Validate_Category
 *
 * @extends     Greenes_Epos_Model_Validate_Abstract
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Validate_Category extends Greenes_Epos_Model_Validate_Abstract
{
    /**
     * @param string $value
     *
     * @return bool
     */
    public function validate($value)
    {
        if (strlen($value) < 3) {
            return false;
        }

        return in_array($value, $this->getAllowedCategories());
    }

    /**
     * @return array
     */
    public function getAllowedCategories()
    {
        return array("LAD", "MEN", "KID", "UNI", "ACC");
    }

}