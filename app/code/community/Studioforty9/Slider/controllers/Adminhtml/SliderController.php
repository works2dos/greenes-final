<?php

class Studioforty9_Slider_Adminhtml_SliderController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @var string $_baseUrl
     */
    protected $_baseUrl = 'slider_admin/slider';

    /**
     * @var array $_uploadFields
     */
    protected $_uploadFields = array('image', 'image_tablet', 'image_mobile');

    /**
     * _initAction()
     *
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('studioforty9/slider')
            ->_addBreadcrumb($this->__('Slider'), $this->__('Slide Manager'));

        return $this;
    }

    /**
     * Instantiate our grid container block and add to the page content.
     * When accessing this admin index page we will see a grid of all
     * Slider currently available in our Magento instance, along with
     * a button to add a new one if we wish.
     */
    public function indexAction()
    {
        // instantiate the grid container
        $slideBlock = $this->getLayout()->createBlock('studioforty9_slider_adminhtml/slide');

        // add the grid container as the only item on this page
        $this->loadLayout()->_addContent($slideBlock);

        return $this->renderLayout();
    }

    /**
     * This action handles both viewing and editing of existing Slider.
     *
     * @return $this
     */
    public function editAction()
    {
        /* @var Studioforty9_Slider_Model_Slide $slide */
        $slide = Mage::getModel('studioforty9_slider/slide');

        // Guard against the slide Id not existing
        if ($slideId = $this->getRequest()->getParam('id', false)) {
            $slide->load($slideId);
            if ($slide->getId() == 0) {
                $this->_getSession()->addError(
                    $this->__('The slide you referenced no longer exists.')
                );
                return $this->_redirect('*/*/');
            }
        }

        // process $_POST data if the form was submitted
        if ($this->getRequest()->isPost()) {
            $this->_saveAction($slide, $this->getRequest()->getPost());
        }

        // make the current slide object available to blocks
        Mage::register('current_slide', $slide);
        // instantiate the form container
        $slideEditBlock = $this->getLayout()->createBlock(
            'studioforty9_slider_adminhtml/slide_edit'
        );
        // instantiate the tabs block
        $slideTabBlock = $this->getLayout()->createBlock(
            'studioforty9_slider_adminhtml/slide_edit_tabs'
        );
        // add the form container and tags
        $this->loadLayout()
            ->_addContent($slideEditBlock)
            ->_addLeft($slideTabBlock);

        $this->_setActiveMenu('studioforty9/slider');
        $this->_addBreadcrumb($this->__('Slide Manager'), $this->__('Slide Manager'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        return $this->renderLayout();
    }

    protected function _saveAction($slide, $postData)
    {
        // Unset the image upload fields
        unset($postData['image'], $postData['image_tablet'], $postData['image_mobile'], $postData['form_key']);

        // Check if we have some image files to upload
        if (!empty($_FILES)) {
            try {
                $this->_uploadImages($slide);
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
            }
        }

        try {
            $slide->addData($postData);
            $slide->save();
            $this->_getSession()->addSuccess(
                $this->__('The slide was saved successfully.')
            );
            return $this->_redirect('*/*/', array('id' => $slide->getId()));
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            return $this->_redirect('*/*');
        }
    }

    /**
     * Upload the images from the $_FILES array.
     *
     * @param Studioforty9_Slider_Model_Slide $slide
     * @throws Exception
     */
    protected function _uploadImages($slide)
    {
        foreach ($this->_uploadFields as $image) {
            if (array_key_exists($image, $_FILES) && !empty($_FILES[$image]['name'])) {
                $uploader = new Varien_File_Uploader($image);
                // TODO: Make this a configuration option
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                // TODO: Make this a configuration option
                $uploader->setAllowRenameFiles(false);
                // TODO: Make this a configuration option
                $uploader->setFilesDispersion(false);
                // TODO: Make this a configuration option
                $path = $this->getSliderHelper()->getImagePath($image);
                $uploader->save($path);
                $slide->setData($image, $uploader->getUploadedFileName());
            }
        }
    }

    /**
     * Delete a single slide.
     *
     * @return $this
     */
    public function deleteAction()
    {
        $slide = Mage::getModel('studioforty9_slider/slide');

        if ($slideId = $this->getRequest()->getParam('id', false)) {
            $slide->load($slideId);
        }

        if ($slide->getId() < 1) {
            $this->_getSession()->addError(
                $this->__('The slide you referenced no longer exists.')
            );
            return $this->_redirect('*/*/');
        }

        try {
            $slide->delete();
            $this->_getSession()->addSuccess(
                $this->__('The slide was deleted successfully.')
            );
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        return $this->_redirect('*/*/');
    }

    public function getSliderHelper()
    {
        return Mage::helper('studioforty9_slider');
    }

    /**
     * Without this method the ACL rules configured in adminhtml.xml
     * are ignored. The isAllowed() method will use the ACL rule we
     * have configured in our adminhtml.xml file:
     * - acl
     * - - resources
     * - - - admin
     * - - - - children
     * - - - - - studioforty9_slider
     * - - - - - - children
     * - - - - - - - slider
     *
     * eg. you could add more rules inside slider for edit and delete.
     */
    protected function _isAllowed()
    {
        $actionName = $this->getRequest()->getActionName();
        switch ($actionName) {
            case 'index':
            case 'edit':
            case 'delete':
            default:
                $adminSession = Mage::getSingleton('admin/session');
                $isAllowed = $adminSession->isAllowed('studioforty9_slider/slide');
                break;
        }

        return $isAllowed;
    }

    /**
     * gridAction()
     *
     * Hack to get pagination to work
     */
    public function gridAction()
    {
        $this->loadLayout();
        // instantiate the grid container
        $slideBlock = $this->getLayout()->createBlock('studioforty9_slider_adminhtml/slide_grid');
        // add the grid container as the only item on this page
        $this->getResponse()->setBody($slideBlock->toHtml());
    }
}
