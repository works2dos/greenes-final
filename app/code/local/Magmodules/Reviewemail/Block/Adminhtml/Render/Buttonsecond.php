<?php 
/**
 * Magmodules.eu - http://www.magmodules.eu - info@magmodules.eu
 * =============================================================
 * NOTICE OF LICENSE [Single domain license]
 * This source file is subject to the EULA that is
 * available through the world-wide-web at:
 * http://www.magmodules.eu/license-agreement/
 * =============================================================
 * @category    Magmodules
 * @package     Magmodules_Reviewemail
 * @author      Magmodules <info@magmodules.eu>
 * @copyright   Copyright (c) 2015 (http://www.magmodules.eu)
 * @license     http://www.magmodules.eu/license-agreement/  
 * =============================================================
 */
 
class Magmodules_Reviewemail_Block_Adminhtml_Render_Buttonsecond extends Mage_Adminhtml_Block_System_Config_Form_Field {

	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) 
	{
		$this->setElement($element);
		$order = Mage::getStoreConfig('reviewemail/testing/test_order');
		$email = Mage::getStoreConfig('reviewemail/testing/test_email');
		$message = Mage::helper('reviewemail')->__('Send test email from order %s to %s?', $order, $email);
		$url = $this->getUrl('*/reviewemail/sendSecondTest');

		if($order && $message) {
			$html = $this->getLayout()->createBlock('adminhtml/widget_button')
						->setType('button')->setClass('scalable')
						->setLabel(Mage::helper('reviewemail')->__('Send testemail') . ' (1)')
						->setOnClick("confirmSetLocation('{$message}', '{$url}')")->toHtml();
		} else {
			$html = $this->getLayout()->createBlock('adminhtml/widget_button')
						->setType('button')->setClass('scalable')
						->setLabel(Mage::helper('reviewemail')->__('Send testemail') . ' (1)')
						->setDisabled(1)->toHtml();			
		}
		return $html;
	}  
	 
}