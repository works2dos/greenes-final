<?php

class Studioforty9_Stores_Block_Adminhtml_Stores_Edit_Map extends Mage_Adminhtml_Block_Abstract
{
    /**
     * The default pharmacy helper
     *
     * @var Studioforty9_Stores_Helper_Data
     */
    protected $_helper;

    /**
     * _construct()
     */
    protected function _construct()
    {
        $this->_helper = Mage::helper('studioforty9_stores');
    }

    /**
     * getDefaultZoom()
     *
     * @return int
     */
    public function getDefaultZoom()
    {
        return $this->_helper->getDefaultMapZoom('BACKEND');
    }

    /**
     * getDefaultPosition()
     *
     * @return array
     */
    public function getDefaultPosition()
    {
        $lat = $this->_helper->getDefaultMapLatitude('BACKEND');
        $lng = $this->_helper->getDefaultMapLongitude('BACKEND');

        return array($lat, $lng);
    }

    /**
     * _getStore()
     *
     * @return Studioforty9_Stores_Model_Store
     */
    protected function _getStore()
    {
        return Mage::registry('current_store');
    }

    /**
     * getDefaultMarkerPosition()
     *
     * @return array
     */
    public function getDefaultMarkerPosition()
    {
        list($lat, $lon) = $this->getDefaultPosition();

        $latitude  = $this->_getStore()->getLatitude();
        $longitude = $this->_getStore()->getLongitude();

        if (is_null($latitude) || $latitude === '0.00000000') {
            $this->_getStore()->setLatitude($lat);
        }

        if (is_null($longitude) || $this->_getStore()->getLongitude() === '0.00000000') {
            $this->_getStore()->setLongitude($lon);
        }

        return array($this->_getStore()->getLatitude(), $this->_getStore()->getLongitude());
    }

    public function getDefaultMarkerIcon()
    {
        $default = 'https://mts.googleapis.com/vt/icon/name=icons/spotlight/spotlight-poi.png&scale=1';

        $config  = $this->_helper->getDefaultMapMarker('BACKEND');
        if (!empty($config)) {
            return $config;
        }

        return $default;
    }

    /**
     * getJs()
     *
     * @return string
     */
    public function getJs()
    {
        $icon = $this->getDefaultMarkerIcon();
        $zoom = $this->getDefaultZoom();
        list($lat, $lon) = $this->getDefaultMarkerPosition();

        return <<<JS
var geocoder;
var map;
var marker;
function initialize() {
  geocoder = new google.maps.Geocoder();
  var mapOptions = {
    zoom: $zoom,
    center: new google.maps.LatLng($lat, $lon)
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  marker = new google.maps.Marker({
    position: new google.maps.LatLng($lat, $lon),
    map: map,
    draggable: true,
    icon: new google.maps.MarkerImage('$icon')
  });

  google.maps.event.addListener(marker, 'dragend', function(event) {
    var position = marker.getPosition();
    $('latitude').setValue(position.lat());
    $('longitude').setValue(position.lng());
  });
}

function codeAddress()
{
    var address = document.getElementById('map-lookup-query').value;
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);
          marker.setPosition(results[0].geometry.location);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });

}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' + 'callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;
JS;
    }

    /**
     * _toHtml()
     *
     * @return string
     */
    protected function _toHtml()
    {
        return sprintf(
              '<div class="map-canvas-container">'
              . '<div id="map-canvas"></div>'
              . '<div id="map-lookup">'
                . '<input type="text" id="map-lookup-query" value="%s" placeholder="Search..." />'
                . '<button onclick="return codeAddress()" type="button" id="map-lookup-button">'
                  . '<span><span>Search</span></span>'
                . '</button>'
              . '</div>'
            . '</div>'
            . '<script type="text/javascript">%s</script>',
            $this->_getStore()->getAddress(),
            $this->getJs()
        );
    }
}
