var Locator = Class.create({
  icon: 'https://mts.googleapis.com/vt/icon/name=icons/spotlight/spotlight-poi.png&scale=1',
  settings: {
    icon: '',
    zoom: 16,
    center: { lat: 53.3367655, lng: -6.2489062 },
    buildInfoWindow: function() {}
  },
  initialize: function(el, stores, options) {
    this.element = $(el);
    this.stores = stores;
    this.settings = Object.extend(this.settings, options);
    this.markers = [];
    this.windows = [];
  },
  initGoogle: function() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' + 'callback=locator.render';
    document.body.appendChild(script);
  },
  initMap: function() {
    var self = this;
    return new google.maps.Map(self.element, {
      zoom: parseInt(self.settings.zoom, 10),
      center: new google.maps.LatLng(
        parseFloat(self.settings.center.lat),
        parseFloat(self.settings.center.lng)
      )
    });
  },
  buildMarkers: function() {
    var self = this;

    self.stores.each(function(store) {
      var id = store[self.settings.identifier];
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(store.latitude, store.longitude),
        map: self.map,
        draggable: false,
        icon: new google.maps.MarkerImage(self.getMarkerIcon()),
        __id: id
      });
      var info = new google.maps.InfoWindow({
        content: self.settings.buildInfoWindow.apply(self, [store])
      });
      google.maps.event.addListener(self.map, 'click', function() {
        console.log(self.map.getCenter());
      });
      google.maps.event.addListener(marker, 'click', function(event) {
        self.windows.each(function(win) {
          win.close();
        });
        info.open(self.map, marker);
        self.map.panTo(marker.getPosition());
      });
      self.markers[id] = marker;
      self.windows[id] = info;
    });

    return this.markers;
  },
  setActiveMarker: function(markerId, element) {
    var self = this, marker = self.markers[markerId];
    google.maps.event.trigger(marker, 'click');
    $$('#store-locations .active').each(function(item) {
      item.removeClassName('active');
    });
    element.addClassName('active');
    return false;
  },
  getMarkerIcon: function() {
    var icon = this.icon;

    if (this.settings.icon !== '') {
      icon = this.settings.icon;
    }

    return icon;
  },
  render: function() {
    this.map = this.initMap();
    this.markers = this.buildMarkers();
  }
});
