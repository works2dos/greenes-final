<?php

/**
 * Class Greenes_Epos_Model_Product_Simple_Get
 *
 * @extends     Greenes_Epos_Model_Base
 * @category    Greenes
 * @package     Greenes_Epos
 * @author      2016 StudioForty9 (info@studioforty9.com)
 */
class Greenes_Epos_Model_Product_Simple_Get extends Greenes_Epos_Model_Base
{
    /**
     * Groups Products by SKU and Colour
     *
     * @return array|false
     */
    public function getProducts()
    {
        $collection = Mage::getSingleton('greenes_epos/import')
            ->getCollection()
            ->addFieldToSelect("*")
            ->addFieldToFilter("Group", array("in" => array("MEN", "LAD", "KID", "UNI")))
            ->setOrder("id", "ASC");

        if (!$collection->count()) {
            return false;
        }

        $data = array();
        /** @var Greenes_Epos_Model_Import $item */
        foreach ($collection as $item) {
            $key = $this->getConfigurableSku($item->getData());
            $data[$key][] = $item->getData();
        }

        return $data;
    }

    /**
     * Get the configurable sku
     *
     * @param array $data
     *
     * @return false|mixed
     */
    public function getConfigurableSku(array $data)
    {
        $code = $this->getConfigurableSkuCode();
        if (!$code) {
            $this->logError("Configurable attribute not found");
            return false;
        }

        $sourceModel = $this->getConfigurableSourceModel();
        if (!$sourceModel) {
            $this->logError("Configurable source model not found");
            return false;
        }

        $sourceModel->setRowData($data, $code);
        return $sourceModel->getValue();
    }

    /**
     * @return false|Greenes_Epos_Model_Source_Abstract
     */
    public function getConfigurableSourceModel()
    {
        $model = Mage::getStoreConfig("greenes_epos/configurable/sku_source_model");
        if (!$model) {
            return false;
        }
        return Mage::getSingleton($model);
    }

    /**
     * @return string
     */
    public function getConfigurableSkuCode()
    {
        return Mage::helper("greenes_epos/configurable")->getColumnByCode("sku");
    }
}
