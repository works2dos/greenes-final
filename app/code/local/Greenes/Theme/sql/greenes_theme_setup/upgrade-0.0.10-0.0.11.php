<?php
/* @var $this Mage_Eav_Model_Entity_Setup */
$this->startSetup();

$filename = Mage::getBaseDir('var') . DS . 'import/update_categories.csv';

if (! file_exists($filename)) {
    $this->endSetup();
    return;
}

$csv = new Varien_File_Csv();
$csvData = $csv->getData($filename);

Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
foreach ($csvData as $i => $row) {

    $sku = (string)$row[0];
    $categoryId = $row[1];

    /** @var Mage_Catalog_Model_Product $product */
    $product = Mage::getModel("catalog/product")->loadByAttribute("sku", $sku);
    $name = $product->getName();
    $categoryIds = $product->getCategoryIds();

    if (! array_search($categoryId, $categoryIds)) {
        $categoryIds[] = $categoryId;
        $product->setCategoryIds($categoryIds);
        try {
            $product->save();
            Mage::log($sku . " - " . $product->getName(), null, 'category_updated.log', true);
        } catch(Exception $e) {
            Mage::log($sku, null, 'failed.log', true);
            Mage::log($e->getMessage(), null, 'failed_exception.log', true);
        }
    }


}

$this->endSetup();